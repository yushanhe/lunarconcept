﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows.Media;
using System.Windows;
using SHomeWorkshop.LunarConcept.Widgets;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// 创建时间：2012年8月15日
    /// 创建者：　杨震宇
    /// 
    /// 主要用途：用于在主界面左侧页面视图中显示一个页面占位项。
    /// </summary>
    public class PageViewItem : ListBoxItem
    {
        public PageViewItem()
        {
            this.Margin =
                this.BorderThickness = borderMargin;
            this.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;

            this.ToolTip = "[双击]或[选定后按回车]跳转到对应页面";

            this.KeyDown += new System.Windows.Input.KeyEventHandler(PageViewItem_KeyDown);
            this.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(PageViewItem_MouseDoubleClick);
        }

        void PageViewItem_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            JumpToPageEditor();
        }

        void PageViewItem_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                JumpToPageEditor();
            }
        }

        private void JumpToPageEditor()
        {
            if (this.MasterManager == null)
            {
                LunarMessage.Warning("　　未找到页面管理器。");

                return;
            }

            PageEditor pe = MasterManager.GetPageEditor(this.MasterPageID);
            if (pe == null)
            {
                LunarMessage.Warning("　　未找到指定页面，可能已经被删除。");
                return;
            }

            pe.SelectOnlySelf();
            MasterManager.TryToDisplayPageEditor(pe);
        }

        public string Text { get; set; }

        public EditorManager MasterManager { get; set; }

        public string MasterPageID { get; set; }

        private StackPanel sPanel = new StackPanel()
        {
            Orientation = Orientation.Horizontal,
            MinHeight = 40,
        };

        private TextBlock titleBlock = new TextBlock()
        {
            Margin = borderMargin,
            VerticalAlignment = VerticalAlignment.Center,
            Foreground = Brushes.DeepPink,
            Width = 120,
            FontSize = 14,
            FontWeight = FontWeights.Bold,
        };

        private Rectangle leftRectangle = new Rectangle()
        {
            Width = 20,
            VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
            StrokeThickness = 0,
        };

        private Rectangle rightRectangle = new Rectangle()
        {
            Width = 20,
            VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
            StrokeThickness = 0,
        };

        private static Thickness borderMargin = new Thickness(4);

        private static Thickness borderPadding = new Thickness(2);

        private static Thickness borderThickness = new Thickness(10);

        private Border mainborder = new Border()
        {
            BorderThickness = borderThickness,
            Margin = borderMargin,
            Padding = borderPadding,
            BorderBrush = Brushes.DarkBlue,
        };

        public void RefreshBackground()
        {
            this.sPanel.Background = MasterManager.DocumentBackground;
        }

        public void BuildItem()
        {
            if (MasterManager == null)
            {
                this.Content = new TextBlock() { Text = "未找到页面管理器" };
                return;
            }

            if (string.IsNullOrEmpty(MasterPageID))
            {
                this.Content = new TextBlock() { Text = "未找到所在页面" };
                return;
            }

            PageEditor pe = MasterManager.GetPageEditor(MasterPageID);
            if (pe == null)
            {
                this.Content = new TextBlock() { Text = "未找到所在页面" };
                return;
            }

            //double fontSize = MasterManager.DefaultFontSize;


            leftRectangle.Fill = pe.PageLeftSideBackColor;

            if (sPanel.Children.Contains(leftRectangle) == false)
            {
                sPanel.Children.Add(leftRectangle);
            }

            string pageTitle = pe.PageTitle;
            if (string.IsNullOrEmpty(pageTitle))
            {
                int index = MasterManager.Children.IndexOf(pe);
                titleBlock.Text = string.Format("第 {0} 页", index + 1);
                titleBlock.Foreground = Brushes.Blue;
                titleBlock.FontWeight = FontWeights.Normal;
            }
            else
            {
                titleBlock.Text = pageTitle;
                titleBlock.Foreground = Brushes.Red;
                titleBlock.FontWeight = FontWeights.Normal;
            }

            if (sPanel.Children.Contains(titleBlock) == false)
            {
                sPanel.Children.Add(titleBlock);
            }

            rightRectangle.Fill = pe.PageRightSideBackColor;

            if (sPanel.Children.Contains(rightRectangle) == false)
            {
                sPanel.Children.Add(rightRectangle);
            }

            if (this.mainborder.Child != this.sPanel)
            {
                this.mainborder.Child = this.sPanel;
            }

            this.sPanel.Background = MasterManager.DocumentBackground;

            if (this.Content != this.mainborder)
            {
                this.Content = this.mainborder;
            }
        }
    }
}
