﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// 创建时间：2011年12月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：表示“撤销与重做”列表中的一次“修改”。一次“修改”可包含多个“动作（Action）”。
    /// </summary>
    /// <typeparam name="TAction">传入表示某次修改中某个“动作（Action）”的相关数据。</typeparam>
    /// <typeparam name="TData">表示与此次修改（而非此次修改中某个“动作”）相关的信息数据。</typeparam>
    public class ModifingItem<TAction, TInfo> : IEnumerable<TAction>
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]表示一个“修改项（ModifingItem）”。
        /// ——一个修改项包括多个“动作（Action）”。
        /// </summary>
        public ModifingItem(TInfo modifingInfo)
        {
            this.modifingInfo = modifingInfo;
        }

        #endregion

        #region 索引器与迭代器===============================================================================================

        /// <summary>
        /// 索引器。取actionList中的某个Action。
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public TAction this[int index]
        {
            get { return actionList[index]; }
        }

        #endregion

        #region 字段与属性===================================================================================================

        /// <summary>
        /// [只读]返回ActionList中的Action的数目。
        /// </summary>
        public int ActionCount
        {
            get
            {
                if (actionList == null) return 0;
                return actionList.Count;
            }
        }

        /// <summary>
        /// 这是动作（Action）类实例的集合。
        /// ——通常不止一个成员，一般来说，一次“修改”由多个“动作”组成。
        /// </summary>
        private List<TAction> actionList = new List<TAction>();

        private TInfo modifingInfo;
        /// <summary>
        /// [只读]与此次修改（而非此次修改中某个“动作（Action）”相关的信息。
        /// </summary>
        public TInfo ModifingInfo
        {
            get { return modifingInfo; }
        }

        private bool needRefreshAutoNumberStrings = false;
        /// <summary>
        /// 是否需要刷新自动编号。默认为false。
        /// </summary>
        public bool NeedRefreshAutoNumberStrings
        {
            get { return needRefreshAutoNumberStrings; }
            set { needRefreshAutoNumberStrings = value; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 将一个动作插入到“修改项（ModifingItem）”的“动作列表（ActionList）”中。
        /// ——只是封装了actionList.Add()方法。
        /// </summary>
        /// <param name="action">Action实例，表示一次修改中的一个“动作”。</param>
        public void AddAction(TAction action)
        {
            if (action == null) return;

            this.actionList.Add(action);
        }

        /// <summary>
        /// 将一个动作插入到“修改项（ModifingItem）”的“动作列表（ActionList）”中的指定索引位置处。
        /// 
        /// 异常：
        ///     ArgumentOutOfRangeException
        /// </summary>
        /// <param name="action">Action实例，表示一次修改中的一个“动作”。</param>
        public void InsertAction(int index, TAction action)
        {
            if (action == null) return;

            this.actionList.Insert(index, action);
        }

        #endregion

        #region IEnumerable<Action> 成员

        public IEnumerator<TAction> GetEnumerator()
        {
            for (int i = 0; i < actionList.Count; i++)
            {
                yield return actionList[i];
            }
        }

        #endregion

        #region IEnumerable 成员

        IEnumerator IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < actionList.Count; i++)
            {
                yield return actionList[i];
            }
        }

        #endregion

    }
}
