﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// CopySelectedAreaAsImageWindow.xaml 的交互逻辑
    /// </summary>
    public partial class CopySelectedAreaAsImageWindow : Window
    {
        public CopySelectedAreaAsImageWindow()
        {
            InitializeComponent();
        }

        private void BtnCopyAsImage_Click(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap rtb;
            rtb = new RenderTargetBitmap((int)MainCanvas.ActualWidth, (int)MainCanvas.ActualHeight, 96, 96, PixelFormats.Pbgra32);//只在屏幕上显示的图片分辨率可以低些，96DPI就够了。
            rtb.Render(MainCanvas);

            try
            {
                Clipboard.SetImage(rtb);
            }
            catch (Exception ex)
            {
                LunarMessage.Warning(ex.Message);
            }
            finally
            {
                this.Close();
            }
        }
    }
}
