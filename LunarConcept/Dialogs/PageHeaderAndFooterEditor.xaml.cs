﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept.Dialogs
{
    /// <summary>
    /// PageHeaderAndFooterEditor.xaml 的交互逻辑
    /// </summary>
    public partial class PageHeaderAndFooterEditor : Window
    {
        public PageHeaderAndFooterEditor(Window owner)
        {
            InitializeComponent();

            if (owner != null)
            {
                this.Owner = owner;
            }

            tbxPageHeader.Focus();
            tbxPageHeader.SelectAll();

            tbxPageHeader.GotFocus += new RoutedEventHandler(tbxPageHeader_GotFocus);
            tbxPageFooter.GotFocus += new RoutedEventHandler(tbxPageFooter_GotFocus);
        }

        void tbxPageHeader_GotFocus(object sender, RoutedEventArgs e)
        {
            tbxPageHeader.SelectAll();
        }

        void tbxPageFooter_GotFocus(object sender, RoutedEventArgs e)
        {
            tbxPageFooter.SelectAll();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    {
                        this.DialogResult = false; this.Close(); return;
                    }
                case Key.Enter:
                    {
                        this.DialogResult = true; this.Close(); return;
                    }
            }
        }
    }
}
