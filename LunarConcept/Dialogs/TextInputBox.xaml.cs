﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept.Dialogs
{
    /// <summary>
    /// TextInputBox.xaml 的交互逻辑
    /// </summary>
    public partial class TextInputBox : Window
    {
        public TextInputBox(Window owner, string promptText, string title, string initilizedText)
        {
            InitializeComponent();

            this.Owner = owner;

            TbPrompt.Text = promptText;
            this.Title = (title == null ? string.Empty : title);
            this.TbxInput.Text = (initilizedText == null ? string.Empty : initilizedText);
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.TbxInput.SelectAll();
            this.TbxInput.Focus(); 
            Globals.SwitchInputMethod(true);
        }
    }
}
