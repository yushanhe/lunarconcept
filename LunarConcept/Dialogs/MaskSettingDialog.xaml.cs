﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept.Dialogs
{
    /// <summary>
    /// MaskSettingDialog.xaml 的交互逻辑
    /// </summary>
    public partial class MaskSettingDialog : Window
    {
        public MaskSettingDialog(Window owner)
        {
            InitializeComponent();

            this.Owner = owner;
        }

        public Enums.MaskType Mask
        {
            get
            {
                if (RBtnFillHLine.IsChecked == true) return Enums.MaskType.FillHLine;
                if (RBtnFillHVLine.IsChecked == true) return Enums.MaskType.FillHVLine;
                if (RBtnFillLeftBias.IsChecked == true) return Enums.MaskType.FillLeftBias;
                if (RBtnFillRightBias.IsChecked == true) return Enums.MaskType.FillRightBias;
                if (RBtnFillVLine.IsChecked == true) return Enums.MaskType.FillVLine;

                if (RBtnHLine.IsChecked == true) return Enums.MaskType.HLine;
                if (RBtnHVLine.IsChecked == true) return Enums.MaskType.HVLine;
                if (RBtnLeftBias.IsChecked == true) return Enums.MaskType.LeftBias;
                if (RBtnRightBias.IsChecked == true) return Enums.MaskType.RightBias;
                if (RBtnVLine.IsChecked == true) return Enums.MaskType.VLine;

                if (RBtnBlank.IsChecked == true) return Enums.MaskType.Blank;

                return Enums.MaskType.None;
            }
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
