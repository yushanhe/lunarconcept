﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept.Comparers
{
    class RectangleWidgetMarkTextComparer : IComparer<Widgets.RectangleWidget>
    {
        public int Compare(RectangleWidget x, RectangleWidget y)
        {
            if(int.TryParse(x.CommentText,out int xIndex) && int.TryParse(y.CommentText,out int yIndex))
            {
                return xIndex.CompareTo(yIndex);
            }

            return x.MarkText.CompareTo(y.MarkText);
        }
    }
}
