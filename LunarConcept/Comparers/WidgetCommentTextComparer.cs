﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept.Comparers
{
    class WidgetCommentTextComparer : IComparer<Widgets.Widget>
    {
        public int Compare(Widget x, Widget y)
        {
            if (int.TryParse(x.CommentText, out int xIndex) && int.TryParse(y.CommentText, out int yIndex))
            {
                return xIndex.CompareTo(yIndex);
            }

            return x.CommentText.CompareTo(y.CommentText);
        }
    }
}
