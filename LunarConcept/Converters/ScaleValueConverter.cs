﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Converters
{
    /// <summary>
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    public class ScaleValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return 1;

            double old = (double)value;

            return GetNewScaleValue(old);
        }

        public static double GetNewScaleValue(double old)
        {
            if (old == 0) return 1;
            else if (old < 0)
            {
                return 1 / (1 - old);
            }
            else
            {
                return old + 1;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            double old = (double)value;
            return RestoreScaleValue(old);
        }

        public static double RestoreScaleValue(double old)
        {
            if (old == 1) return 0;
            else if (old < 1)
            {
                return 1 - 1 / old;
            }
            else
            {
                return old - 1;
            }
        }

        #endregion
    }
}
