﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Converters
{
    /// <summary>
    /// 创建时间：2011年12月28日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：以文本形式显示（百分比的）缩放比例所需要的转换器。
    ///           ——因此要乘以１００.
    /// </summary>
    [ValueConversion(typeof(double), typeof(string))]
    public class ScaleTextConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return "100%";

            double old = (double)value;
            double newValue;

            if (old == 0) newValue = 1;
            else if (old < 0)
            {
                newValue = 1 + old * (0.1);
            }
            else
            {
                newValue = old / 2 + 1;
            }

            StringBuilder sb = new StringBuilder();
            sb.Append(((double)newValue * 100).ToString("f1"));
            sb.Append("%");

            return sb.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion
    }
}
