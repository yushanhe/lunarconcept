﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept.Converters
{
    /// <summary>
    /// 创建时间：2012年1月17日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：界面使用的表示控件状态的Brush转换器。
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Brush))]
    public class EnableBrushConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool enable = (bool)value;

            return (enable == true) ? Brushes.Black : Brushes.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return true;
        }

        #endregion
    }
}
