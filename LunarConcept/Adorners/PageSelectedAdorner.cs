﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年2月21日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用以表示PageEditor被选定状态的装饰器。
    /// </summary>
    public class PageSelectedAdorner : Adorner
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入PageEditor的BaseCanvas。</param>
        /// <param name="masterPageEditor">使用此装饰器的页面编辑器。</param>
        public PageSelectedAdorner(UIElement adornedElement, PageEditor masterPageEditor)
            : base(adornedElement)
        {
            this.masterPageEditor = masterPageEditor;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private PageEditor masterPageEditor;
        /// <summary>
        /// [只读]使用此装饰器的页面。
        /// </summary>
        public PageEditor MasterPageEditor
        {
            get { return masterPageEditor; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 绘制装饰器。
        /// </summary>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Visibility != Visibility.Visible) return;

            if (this.masterPageEditor == null || this.masterPageEditor == null) return;

            if (this.masterPageEditor.IsSelected == false) return;

            Rect adornedElementRect = new Rect(this.AdornedElement.RenderSize);

            Rect outRect = new Rect(adornedElementRect.Left - 10, adornedElementRect.Top - 10,
                adornedElementRect.Width + 20, adornedElementRect.Height + 20);

            if (masterPageEditor.IsMainSelected)
            {
                drawingContext.DrawRectangle(null, new Pen(Brushes.Orange, 10), outRect);
            }
            else if (masterPageEditor.IsSelected)
            {
                drawingContext.DrawRectangle(null, new Pen(Brushes.LightBlue, 10), outRect);
            }
        }

        #endregion
    }
}
