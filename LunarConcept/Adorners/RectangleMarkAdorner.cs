﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using System;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年4月9日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：RectangleWidget使用的标记装饰器。一般用来显示“ＡＢＣＤ”等答案字符。
    /// 
    /// </summary>
    public class RectangleMarkAdorner : Adorner
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入RectangleWidget的Canvas。</param>
        public RectangleMarkAdorner(UIElement adornedElement, Widgets.RectangleWidget rectangleWidget)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            //this.SnapsToDevicePixels = true;
            //this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            this.masterRectangleWidget = rectangleWidget;

            this.Cursor = Cursors.Hand;
            this.MouseEnter += new MouseEventHandler(MaskAdorner_MouseEnter);
            this.MouseLeave += new MouseEventHandler(MaskAdorner_MouseLeave);
        }

        void MaskAdorner_MouseLeave(object sender, MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void MaskAdorner_MouseEnter(object sender, MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        private Widgets.RectangleWidget masterRectangleWidget;

        public Widgets.RectangleWidget MasterRectangleWidget
        {
            get { return masterRectangleWidget; }
            set { masterRectangleWidget = value; }
        }

        public Rect MasterRect { get; set; }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (masterRectangleWidget == null) return;
            if (masterRectangleWidget.MarkText == null || masterRectangleWidget.MarkText.Length <= 0) return;

            if (this.masterRectangleWidget.MasterEditor == null) return;
            if (this.masterRectangleWidget.MasterEditor.MasterManager == null) return;
            if (this.masterRectangleWidget.MasterEditor.MasterManager.MasterWindow == null) return;

            //Rect rect = this.masterRectangleWidget.RealRect;

            Rect rect = this.MasterRect;

            double defFontSize = this.masterRectangleWidget.MasterEditor.MasterManager.DefaultFontSize;

            Typeface typeface;

            if (defFontSize > 15)
            {
                typeface = new Typeface(
                            font_YaHei,
                            FontStyles.Normal,
                            FontWeights.Bold,
                            FontStretches.Normal
                        );
            }
            else
            {
                typeface = new Typeface(
                            font_SimSun,
                            FontStyles.Normal,
                            FontWeights.Normal,
                            FontStretches.Normal
                        );
            }

            //要考虑线宽。
            double lineWidth = this.masterRectangleWidget.WidgetLineWidth;

            if (masterRectangleWidget.IsMarkTextCollapsed)
            {
                //折叠时，中间画个实心矩形就是了。
                //lineWidth*4是为文本与边框留点空白。
                if (defFontSize * 3 + lineWidth * 4 > rect.Width)
                {
                    return;//横向空间不够。
                }

                double halfFontSize = defFontSize / 2;

                Rect drawRect = new Rect(rect.Right - defFontSize * 3 - lineWidth * 4,
                        (int)(rect.Bottom - halfFontSize - lineWidth * 2.5),//多*2是为文本与边框留点空白。
                        (defFontSize + lineWidth) * 2,
                        (int)(defFontSize + lineWidth * 4));//*4是为文本与边框留点空白。

                var backBrush = masterRectangleWidget.WidgetBackColor;

                if (backBrush == Brushes.Transparent && Globals.MainWindow.EditorManager.DocumentBackground != Brushes.Transparent)
                {
                    backBrush = Globals.MainWindow.EditorManager.DocumentBackground;
                }

                drawingContext.DrawRectangle(backBrush,
                    new Pen(masterRectangleWidget.WidgetLineColor, masterRectangleWidget.WidgetLineWidth),
                    drawRect);//, halfFontSize + lineWidth, halfFontSize + lineWidth);

                drawingContext.DrawRectangle(masterRectangleWidget.WidgetForeColor, null,
                    //new Pen(masterRectangleWidget.WidgetLineColor, masterRectangleWidget.WidgetLineWidth),
                    new Rect(drawRect.Left + (drawRect.Width - halfFontSize) / 2,
                        drawRect.Top + (drawRect.Height - halfFontSize) / 2,
                        halfFontSize, halfFontSize));
            }
            else
            {
                //Brush renderBrush = this.masterRectangleWidget.MasterEditor.MasterManager.WidgetSelectionAdornerBrush;
                Brush renderBrush = this.masterRectangleWidget.WidgetForeColor;

                TextFormattingMode tfm = TextOptions.GetTextFormattingMode(
                    this.masterRectangleWidget.MasterEditor.MasterWindow.MainScrollViewer);

                //TextFormattingMode tfm = TextFormattingMode.Ideal;

                FormattedText formattedText =
                        new FormattedText(
                            this.masterRectangleWidget.MarkText,
                            new System.Globalization.CultureInfo("zh-CN"),
                            System.Windows.FlowDirection.LeftToRight,
                            typeface, defFontSize, renderBrush, null, tfm
                        );

                //lineWidth*4是为文本与边框留点空白。
                if (formattedText.WidthIncludingTrailingWhitespace + defFontSize * 2 + lineWidth * 4 > rect.Width)
                {
                    return;//横向空间不够。
                }

                double halfFontSize = defFontSize / 2;

                Rect drawRect = new Rect(rect.Right - defFontSize * 2 - formattedText.WidthIncludingTrailingWhitespace - lineWidth * 4,
                        (int)(rect.Bottom - formattedText.Height / 2 - lineWidth * 2.5),//多*2是为文本与边框留点空白。
                        formattedText.WidthIncludingTrailingWhitespace + defFontSize + lineWidth * 2,
                        (int)(formattedText.Height + lineWidth * 4));//*4是为文本与边框留点空白。

                //drawingContext.DrawRoundedRectangle(masterRectangleWidget.WidgetBackColor,
                //    new Pen(masterRectangleWidget.WidgetLineColor, masterRectangleWidget.WidgetLineWidth),
                //    drawRect, halfFontSize + lineWidth, halfFontSize + lineWidth);

                var backBrush = masterRectangleWidget.WidgetBackColor;

                if(backBrush == Brushes.Transparent && Globals.MainWindow.EditorManager.DocumentBackground != Brushes.Transparent)
                {
                    backBrush = Globals.MainWindow.EditorManager.DocumentBackground;
                }

                drawingContext.DrawRectangle(backBrush,
                    new Pen(masterRectangleWidget.WidgetLineColor, masterRectangleWidget.WidgetLineWidth),
                    drawRect);

                drawingContext.DrawText(formattedText,
                        new Point(drawRect.Left + (drawRect.Width - formattedText.WidthIncludingTrailingWhitespace) / 2,
                            drawRect.Top + (drawRect.Height - formattedText.Height) / 2)
                    );
            }
            //drawingContext.DrawText(formattedText,
            //        new Point(rect.BottomLeft.X - formattedText.WidthIncludingTrailingWhitespace - 10,
            //            rect.Top + rect.Height / 2 - formattedText.Height / 2)
            //    );
        }

        static RectangleMarkAdorner()
        {
            //customFont = new FontFamily(new Uri("pack://application:,,,/"), "./Resources/#SubtleSansRegular");
            font_SimSun = new FontFamily("SimSun");
            font_YaHei = new FontFamily("Microsoft YaHei");
        }

        //private static FontFamily customFont;

        private static FontFamily font_YaHei;
        private static FontFamily font_SimSun;
    }
}