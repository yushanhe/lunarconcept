﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2013年8月10日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：为主界面MainScrollViewer添加的装饰器，用于在扩展显示时表示可演示的区域。
    /// </summary>
    public class PresentationAdorner : Adorner
    {

        /// <summary>
        /// 用在主界面的靶标（表示插入点）。
        /// </summary>
        /// <param name="adornedElement">应传入备注文本块。</param>
        /// <param name="win">应传入主窗口。</param>
        public PresentationAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            this.ToolTip = "跨页演示区域";
        }

        protected override void OnRender(DrawingContext drawingContext)
        {

            ScrollViewer scrollViewer = this.AdornedElement as ScrollViewer;
            if (scrollViewer == null) return;

            System.Windows.Forms.Screen[] screens;
            screens = System.Windows.Forms.Screen.AllScreens; //get all the screen width and heights 
            if (screens.Length < 2) return;

            Rect rect = new Rect(new Size(scrollViewer.ViewportWidth, scrollViewer.ViewportHeight));

            if (rect.Width <= screens[1].WorkingArea.Width + 4) return;
            if (rect.Height <= screens[1].WorkingArea.Height + 4) return;

            Brush backbrush = (Brush)FindResource("PresentationAdornerBackgroundImageBrush");
            Pen penMain = new Pen(backbrush, 4);

            Pen penOther = new Pen(Brushes.Green, 0);

            Rect boxMainRect = new Rect(rect.Left + (rect.Width - screens[1].WorkingArea.Width) / 2 - 1,
                    rect.Top + (rect.Height - screens[1].WorkingArea.Height) / 2 - 1,
                    screens[1].WorkingArea.Width + 4, screens[1].WorkingArea.Height + 4);

            //左侧
            drawingContext.DrawRectangle(backbrush, penOther,
                new Rect(boxMainRect.Left, boxMainRect.Top + boxMainRect.Height / 2 - 40, 8, 80));

            //上侧
            drawingContext.DrawRectangle(backbrush, penOther,
                new Rect(boxMainRect.Left + boxMainRect.Width / 2 - 40, boxMainRect.Top, 80, 8));

            //右侧
            drawingContext.DrawRectangle(backbrush, penOther,
                new Rect(boxMainRect.Right - 8, boxMainRect.Top + boxMainRect.Height / 2 - 40, 8, 80));

            //下侧
            drawingContext.DrawRectangle(backbrush, penOther,
                new Rect(boxMainRect.Left + boxMainRect.Width / 2 - 40, boxMainRect.Bottom - 8, 80, 8));

            drawingContext.DrawRectangle(null, penMain, boxMainRect);
        }
    }
}