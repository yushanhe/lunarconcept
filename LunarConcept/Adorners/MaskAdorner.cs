﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年2月16日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：ContentWidget的罩子。如果其它公式也支持的话，一来并无多大意义，
    ///           二来出现多层遮罩可能看不清。
    /// </summary>
    public class MaskAdorner : Adorner
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">ContentWidget。</param>
        public MaskAdorner(Widgets.ContentWidget adornedElement)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            //this.SnapsToDevicePixels = true;
            //this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);

            this.masterContentWidget = adornedElement;

            this.Cursor = Cursors.Hand;
            this.MouseEnter += new MouseEventHandler(MaskAdorner_MouseEnter);
            this.MouseLeave += new MouseEventHandler(MaskAdorner_MouseLeave);
        }

        void MaskAdorner_MouseLeave(object sender, MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void MaskAdorner_MouseEnter(object sender, MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        private Widgets.ContentWidget masterContentWidget;

        /// <summary>
        /// 使用本遮罩的公式。应该是个文本块。
        /// </summary>
        public Widgets.ContentWidget MasterContentWidget
        {
            get { return masterContentWidget; }
            set { masterContentWidget = value; }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (masterContentWidget == null) return;

            TextFormattingMode tfm = (TextFormattingMode)this.GetValue(TextOptions.TextFormattingModeProperty);
            if (tfm == TextFormattingMode.Display)
            {
                this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            }
            else
            {
                this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            }

            Rect rect = new Rect(this.AdornedElement.RenderSize);

            if (this.IsMouseOver)
            {
                //鼠标停留时，只画个透明矩形。
                drawingContext.DrawRectangle(Brushes.Transparent, transparentPen, rect);
                return;
            }

            Brush renderBrush = masterContentWidget.WidgetForeColor;
            Pen renderPen = new Pen(renderBrush, 1);

            switch (masterContentWidget.Mask)
            {
                case MaskType.Blank:
                    {
                        Thickness margin = masterContentWidget.MainBorder.BorderThickness;

                        double newWidth = rect.Width - margin.Left - margin.Right;
                        double newHeight = rect.Height - margin.Top - margin.Bottom;

                        rect = new Rect(rect.X + margin.Left, rect.Y + margin.Top,
                            (newWidth > 0 ? newWidth : 0), (newHeight > 0 ? newHeight : 0));

                        drawingContext.DrawRectangle(Brushes.White, transparentPen, rect);
                        break;
                    }
                case MaskType.LeftBias:
                    {
                        drawingContext.DrawRectangle(Brushes.Transparent, transparentPen, rect);
                        DrawLeftBias(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.FillLeftBias:
                    {
                        drawingContext.DrawRectangle(masterContentWidget.WidgetBackColor, transparentPen, rect);
                        DrawLeftBias(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.RightBias:
                    {
                        drawingContext.DrawRectangle(Brushes.Transparent, transparentPen, rect);
                        DrawRightBias(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.FillRightBias:
                    {
                        drawingContext.DrawRectangle(masterContentWidget.WidgetBackColor, transparentPen, rect);
                        DrawRightBias(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.HLine:
                    {
                        drawingContext.DrawRectangle(Brushes.Transparent, transparentPen, rect);
                        DrawHLine(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.FillHLine:
                    {
                        drawingContext.DrawRectangle(masterContentWidget.WidgetBackColor, transparentPen, rect);
                        DrawHLine(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.VLine:
                    {
                        drawingContext.DrawRectangle(Brushes.Transparent, transparentPen, rect);
                        DrawVLine(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.FillVLine:
                    {
                        drawingContext.DrawRectangle(masterContentWidget.WidgetBackColor, transparentPen, rect);
                        DrawVLine(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.HVLine:
                    {
                        drawingContext.DrawRectangle(Brushes.Transparent, transparentPen, rect);
                        DrawHLine(drawingContext, rect, renderPen);
                        DrawVLine(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.FillHVLine:
                    {
                        drawingContext.DrawRectangle(masterContentWidget.WidgetBackColor, transparentPen, rect);
                        DrawHLine(drawingContext, rect, renderPen);
                        DrawVLine(drawingContext, rect, renderPen);
                        break;
                    }
                case MaskType.None:
                    {
                        return;
                    }
            }
        }

        static MaskAdorner()
        {
            transparentPen = new Pen(Brushes.Transparent, 1);
        }

        private static Pen transparentPen;

        private static Rect DrawRightBias(DrawingContext drawingContext, Rect rect, Pen renderPen)
        {
            double tx, bx;
            bx = 4;

            while (bx < rect.Width + rect.Height)
            {
                tx = bx - rect.Height;

                Point startPt;
                if (tx < rect.Left)
                {
                    startPt = new Point(rect.Left, rect.Top + rect.Left - tx);
                }
                else if (tx < rect.Right && tx >= rect.Left)
                {
                    startPt = new Point(tx, rect.Top);
                }
                else
                {
                    continue;
                }

                Point endPt;
                if (bx < rect.Right)
                {
                    endPt = new Point(bx, rect.Bottom);
                }
                else if (bx >= rect.Right && bx <= rect.Right + rect.Height)
                {
                    endPt = new Point(rect.Right, rect.Bottom - (bx - rect.Right));
                }
                else
                {
                    continue;
                }

                drawingContext.DrawLine(renderPen, startPt, endPt);

                bx += 4;
            }
            return rect;
        }

        private static Rect DrawLeftBias(DrawingContext drawingContext, Rect rect, Pen renderPen)
        {
            double tx, bx;
            tx = 4;

            while (tx < rect.Width + rect.Height)
            {
                bx = tx - rect.Height;

                Point startPt;
                if (tx < rect.Right)
                {
                    startPt = new Point(tx, 0);
                }
                else if (tx < rect.Right + rect.Height)
                {
                    startPt = new Point(rect.Right, rect.Top + tx - rect.Right);
                }
                else
                {
                    continue;
                }

                Point endPt;
                if (bx < rect.Left)
                {
                    endPt = new Point(rect.Left, rect.Bottom - (rect.Left - bx));
                }
                else if (bx >= rect.Left && bx <= rect.Right)
                {
                    endPt = new Point(bx, rect.Bottom);
                }
                else
                {
                    continue;
                }

                drawingContext.DrawLine(renderPen, startPt, endPt);

                tx += 4;
            }
            return rect;
        }

        private static Rect DrawHLine(DrawingContext drawingContext, Rect rect, Pen renderPen)
        {
            double y = rect.Top;

            while (y < rect.Bottom)
            {
                y += 4;
                Point startPt = new Point(rect.Left, y);
                Point endPt = new Point(rect.Right, y);
                drawingContext.DrawLine(renderPen, startPt, endPt);
            }
            return rect;
        }

        private static Rect DrawVLine(DrawingContext drawingContext, Rect rect, Pen renderPen)
        {
            double x = rect.Left;

            while (x < rect.Right)
            {
                x += 4;

                Point startPt = new Point(x, rect.Top);
                Point endPt = new Point(x, rect.Bottom);
                drawingContext.DrawLine(renderPen, startPt, endPt);
            }
            return rect;
        }
    }
}