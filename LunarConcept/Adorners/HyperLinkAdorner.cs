﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年7月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于为部件添加一个标签用来打开超链接。
    /// </summary>
    public class HyperLinkAdorner : Adorner
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement"></param>
        public HyperLinkAdorner(UIElement adornedElement, Widgets.Widget masterWidget)
            : base(adornedElement)
        {
            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            //this.SnapsToDevicePixels = true;
            //this.SetValue(TextOptions.TextFormattingModeProperty, TextFormattingMode.Display);
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            this.masterWidget = masterWidget;

            this.Cursor = Cursors.Arrow;
            //this.MouseEnter += new MouseEventHandler(MaskAdorner_MouseEnter);
            //this.MouseLeave += new MouseEventHandler(MaskAdorner_MouseLeave);
            this.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(HyperLinkAdorner_PreviewMouseLeftButtonDown);
        }

        void HyperLinkAdorner_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.masterWidget != null && this.masterWidget.MasterEditor != null)
            {
                this.masterWidget.MasterEditor.MouseInfo.DraggingType =
                    PageDraggingType.MoveWidgets;//避免移动插入点。
            }
        }

        //void MaskAdorner_MouseLeave(object sender, MouseEventArgs e)
        //{
        //    this.InvalidateVisual();
        //}

        //void MaskAdorner_MouseEnter(object sender, MouseEventArgs e)
        //{
        //    this.InvalidateVisual();
        //}

        private Widgets.Widget masterWidget;

        public Widgets.Widget MasterWidget
        {
            get { return masterWidget; }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            Rect rect;

            Widgets.ArrowLineWidget alw = masterWidget as Widgets.ArrowLineWidget;
            if (alw != null)
            {
                rect = alw.OuterRect;
            }
            else
            {
                Widgets.ContentWidget cw = masterWidget as Widgets.ContentWidget;
                if (cw != null ||
                    masterWidget is Widgets.EllipseWidget ||
                    masterWidget is Widgets.RectangleWidget)
                {
                    rect = new Rect(this.AdornedElement.RenderSize);
                }
                else
                {
                    rect = masterWidget.OuterRect;//ShapeWidget
                }
            }

            Pen pen = new Pen(Brushes.DarkBlue, 0) { DashStyle = DashStyles.Solid };

            Brush bsh = TryFindResource("HyperLinkBrush") as VisualBrush;

            if (bsh != null)
            {
                drawingContext.DrawRectangle(bsh, pen, new Rect(rect.Right - 16, rect.Bottom + 18, 18, 18));
            }

            //Pen pen2 = new Pen(Brushes.DarkBlue, 2);

            //drawingContext.DrawRectangle(Brushes.LightBlue, pen,
            //    new Rect(rect.Right - 18, rect.Bottom + 18, 19, 16));

            //drawingContext.DrawRectangle(Brushes.Transparent, pen2,
            //    new Rect(rect.Right - 16, rect.Bottom + 20, 5, 11));

            //drawingContext.DrawRectangle(Brushes.Transparent, pen2,
            //    new Rect(rect.Right - 7, rect.Bottom + 20, 5, 11));

            //drawingContext.DrawRectangle(Brushes.Black, pen2,
            //    new Rect(rect.Right - 13, rect.Bottom + 23, 7, 4));



        }
    }
}