﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2011年12月29日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用以表示Widget被选定状态的装饰器。
    /// </summary>
    public class WidgetSelectedAdorner : Adorner
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入Widget的主子元素。</param>
        /// <param name="masterWidget">使用此部件装饰器的部件。</param>
        public WidgetSelectedAdorner(UIElement adornedElement, Widget masterWidget)
            : base(adornedElement)
        {
            this.masterWidget = masterWidget;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private Widget masterWidget;
        /// <summary>
        /// [只读]使用此装饰器的部件。
        /// </summary>
        public Widget MasterWidget
        {
            get { return masterWidget; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 绘制装饰器。
        /// </summary>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Visibility != Visibility.Visible) return;

            if (this.masterWidget == null || this.masterWidget == null) return;

            if (this.masterWidget.IsSelected == false) return;

            Brush defaultSelectedAdornerBrush = null;

            if (this.masterWidget.MasterEditor != null && this.masterWidget.MasterEditor.MasterManager != null)
            {
                defaultSelectedAdornerBrush = this.masterWidget.MasterEditor.MasterManager.WidgetSelectionAdornerBrush;
            }

            Rect adornedElementRect = new Rect(this.AdornedElement.RenderSize);

            #region 先画线。
            Pen linePen = null;

            linePen = new Pen((defaultSelectedAdornerBrush == null) ? Brushes.Blue : defaultSelectedAdornerBrush, 1);
            //linePen = this.masterTitle.Editor.SubPanelSelectionAdornerPen;
            linePen.DashStyle = DashStyles.DashDot;


            linePen.LineJoin = PenLineJoin.Round;
            linePen.StartLineCap = PenLineCap.Round;
            linePen.EndLineCap = PenLineCap.Round;

            Point newTopLeft = new Point(adornedElementRect.TopLeft.X - 2, adornedElementRect.TopLeft.Y - 2);
            Point ptTmp = new Point(adornedElementRect.BottomRight.X + 3, adornedElementRect.BottomRight.Y + 3);
            Rect lineRect = new Rect(newTopLeft, ptTmp);

            //使用这个办法其实比画四条线更好。但要注意，第一个参数必须为：null而不能是：Brshes.Transparent。
            //否则画出来的矩形不是空心的。
            //由于某些情况下，部件需要嵌套，一旦矩形不是空心，则选定外层的部件后，就无法用鼠标选定内嵌的部件了。
            drawingContext.DrawRectangle(null, linePen, lineRect);
            #endregion

            #region 再画点。
            //SolidColorBrush renderBrush;
            Brush renderBrush;
            Pen renderPen;

            renderPen = new Pen((defaultSelectedAdornerBrush == null) ? Brushes.Blue : defaultSelectedAdornerBrush, 1);
            //renderPen = this.masterTitle.Editor.ExSelectionAdornerPen;

            if (masterWidget.IsMainSelected)
            {
                renderBrush = (defaultSelectedAdornerBrush == null) ? Brushes.Blue : defaultSelectedAdornerBrush;
                linePen.DashStyle = DashStyles.Dash;
            }
            else
            {
                renderBrush = Brushes.White;
                linePen.DashStyle = DashStyles.Dot;
            }
            //renderBrush = this.masterTitle.Editor.ExSelectionAdornerBrush;

            if (masterWidget.IsLocked)
            {
                //画一把小锁图标
                drawingContext.DrawRectangle(Brushes.White, renderPen,
                    new Rect(adornedElementRect.Left - 5, adornedElementRect.Top - 5, 6, 6));
                drawingContext.DrawRectangle(renderBrush/*Brushes.Black*/, renderPen,
                    new Rect(adornedElementRect.Left - 7, adornedElementRect.Top - 1, 10, 4));
            }
            else
            {
                drawingContext.DrawRectangle(renderBrush/*Brushes.Black*/, renderPen,
                    new Rect(adornedElementRect.Left - 4, adornedElementRect.Top - 4, 4, 4));//左上
            }

            drawingContext.DrawRectangle(renderBrush/*Brushes.Black*/, renderPen,
                new Rect(adornedElementRect.Right + 1, adornedElementRect.Top - 4, 4, 4));//右上
            drawingContext.DrawRectangle(renderBrush/*Brushes.Black*/, renderPen,
                new Rect(adornedElementRect.Left - 4, adornedElementRect.Bottom + 1, 4, 4));//左下
            drawingContext.DrawRectangle(renderBrush/*Brushes.Black*/, renderPen,
                new Rect(adornedElementRect.Right + 1, adornedElementRect.Bottom + 1, 4, 4));//右下

            #endregion
        }

        #endregion
    }
}
