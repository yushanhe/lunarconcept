﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    public class LineCtrlAdorner : Adorner
    {
        /// <summary>
        /// 用于线型部件的控制点。
        /// </summary>
        /// <param name="adornedElement">应传入备注文本块。</param>
        /// <param name="win">应传入主窗口。</param>
        public LineCtrlAdorner(UIElement adornedElement, Widgets.LineWidget lineWidget, Brush brush)
            : base(adornedElement)
        {
            this.lineWidget = lineWidget;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = false;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Unspecified);
            this.mainSelectedBrush = brush;
            this.Opacity = 0.75;

            //this.Cursor = System.Windows.Input.Cursors.SizeAll;

            this.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(LineCtrlAdorner_MouseLeftButtonDown);
        }

        void LineCtrlAdorner_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnDoubleClicked(sender, e);
            }
        }

        public event EventHandler<System.Windows.Input.MouseButtonEventArgs> DoubleClicked;

        protected void OnDoubleClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DoubleClicked != null)
            {
                DoubleClicked(sender, e);
            }
        }

        private Brush mainSelectedBrush;
        /// <summary>
        /// 控制点背景色。
        /// </summary>
        public Brush MainSelectedBrush
        {
            get { return mainSelectedBrush; }
            set { mainSelectedBrush = value; }
        }

        private Point centerPoint = new Point(5, 5);

        public Point CenterPoint
        {
            get { return centerPoint; }
            set
            {
                centerPoint = value;
                this.InvalidateVisual();
            }
        }

        public enum ControlerType { Ellipse, Rectangle }

        private ControlerType form = ControlerType.Ellipse;

        public ControlerType Form
        {
            get { return form; }
            set
            {
                form = value;
                this.InvalidateVisual();
            }
        }

        private Widgets.LineWidget lineWidget = null;

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (lineWidget == null) return;

            //Rect rect = new Rect(AdornedElement.RenderSize);

            Pen renderPen;
            if (Globals.MainWindow.EditorManager != null)
            {
                renderPen = new Pen(Globals.MainWindow.EditorManager.WidgetSelectionAdornerBrush, 1);
            }
            else
            {
                renderPen = new Pen(Brushes.Black, 1);
            }

            if (lineWidget.IsSelected == false) return;

            if (lineWidget.IsMainSelected)
            {
                switch (form)
                {
                    case ControlerType.Ellipse:
                        {
                            if (lineWidget.IsLocked)
                            {
                                drawingContext.DrawRectangle(Brushes.White, renderPen,
                                    new Rect(centerPoint.X - 3, centerPoint.Y - 5, 6, 6));
                                drawingContext.DrawRectangle(mainSelectedBrush, renderPen,
                                    new Rect(centerPoint.X - 5, centerPoint.Y, 10, 5));
                            }
                            else
                            {
                                drawingContext.DrawEllipse(mainSelectedBrush, renderPen, centerPoint, 5, 5);
                            }
                            break;
                        }
                    case ControlerType.Rectangle:
                        {
                            drawingContext.DrawRectangle(mainSelectedBrush, renderPen,
                                new Rect(centerPoint.X - 5, centerPoint.Y - 5, 10, 10));
                            break;
                        }
                }
            }
            else
            {
                switch (form)
                {
                    case ControlerType.Ellipse:
                        {
                            if (lineWidget.IsLocked)
                            {
                                drawingContext.DrawRectangle(Brushes.White, renderPen,
                                    new Rect(centerPoint.X - 3, centerPoint.Y - 5, 6, 5));
                                drawingContext.DrawRectangle(Brushes.Transparent, renderPen,
                                    new Rect(centerPoint.X - 5, centerPoint.Y, 10, 5));
                            }
                            else
                            {
                                drawingContext.DrawEllipse(Brushes.Transparent, renderPen, centerPoint, 5, 5);
                            }
                            break;
                        }
                    case ControlerType.Rectangle:
                        {
                            drawingContext.DrawRectangle(Brushes.Transparent, renderPen,
                                   new Rect(centerPoint.X - 5, centerPoint.Y - 5, 10, 10));
                            break;
                        }
                }
            }
        }
    }
}