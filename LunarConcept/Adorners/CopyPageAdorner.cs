﻿using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2018年02月17日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用来复制新页面的装饰器——省去切换Ribbon选项卡。
    /// </summary>
    public class CopyPageAdorner : Adorner
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="adornedElement">应传入PageEditor的BaseCanvas。</param>
        /// <param name="masterPageEditor">使用此装饰器的页面编辑器。</param>
        public CopyPageAdorner(UIElement adornedElement, PageEditor masterPageEditor)
            : base(adornedElement)
        {
            this.masterPageEditor = masterPageEditor;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = true;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private PageEditor masterPageEditor;
        /// <summary>
        /// [只读]使用此装饰器的页面。
        /// </summary>
        public PageEditor MasterPageEditor
        {
            get { return masterPageEditor; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 绘制装饰器。
        /// </summary>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Visibility != Visibility.Visible) return;

            if (this.masterPageEditor.MasterManager == null || this.masterPageEditor == null) return;

            if (this.masterPageEditor.IsMainSelected == false) return;

            Rect adornedElementRect = new Rect(this.AdornedElement.RenderSize);

            Rect outRect;

            var blackPen = new Pen(Brushes.Black, 1);
            switch (this.masterPageEditor.MasterManager.Direction)
            {
                case System.Windows.Controls.Orientation.Horizontal:
                    {
                        outRect = new Rect(adornedElementRect.Right + 1, adornedElementRect.Bottom - 19, 20, 20);
                        drawingContext.DrawRectangle(Brushes.SkyBlue, new Pen(Brushes.Blue, 1), outRect);

                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 2, outRect.Top + 2, 12, 12));
                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 6, outRect.Top + 6, 12, 12));

                        break;
                    }
                default:
                    {
                        outRect = new Rect(adornedElementRect.Right - 19, adornedElementRect.Bottom + 1, 20, 20);
                        drawingContext.DrawRectangle(Brushes.SkyBlue, new Pen(Brushes.Blue, 1), outRect);

                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 2, outRect.Top + 2, 12, 12));
                        //drawingContext.DrawRectangle(Brushes.White, blackPen, new Rect(outRect.Left + 6, outRect.Top + 6, 12, 12));

                        break;
                    }
            }


            var ptStart = new Point(outRect.Left + 14, outRect.Top + 6);

            var geo = new PathGeometry();
            var figure = new PathFigure();
            figure.StartPoint = ptStart;

            var segment = new PolyLineSegment();
            segment.Points.Add(ptStart);
            segment.Points.Add(new Point(outRect.Left + 14, outRect.Top + 2));
            segment.Points.Add(new Point(outRect.Left + 2, outRect.Top + 2));
            segment.Points.Add(new Point(outRect.Left + 2, outRect.Top + 14));
            segment.Points.Add(new Point(outRect.Left + 6, outRect.Top + 14));
            segment.Points.Add(new Point(outRect.Left + 6, outRect.Top + 6));
            segment.Points.Add(new Point(outRect.Left + 18, outRect.Top + 6));
            segment.Points.Add(new Point(outRect.Left + 18, outRect.Top + 18));
            segment.Points.Add(new Point(outRect.Left + 6, outRect.Top + 18));
            segment.Points.Add(new Point(outRect.Left + 6, outRect.Top + 6));
            figure.Segments.Add(segment);

            figure.IsClosed = true;
            figure.IsFilled = true;
            geo.Figures.Add(figure);

            drawingContext.DrawGeometry(Brushes.White, blackPen, geo);
        }

        #endregion
    }
}
