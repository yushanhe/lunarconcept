﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Adorners
{
    /// <summary>
    /// 创建时间：2012年2月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：为ConetneWidget提供一个“链接”装饰器，用以跳转到指定的某个页面。
    /// </summary>
    public class WidgetLinkerAdorner : Adorner
    {
        /// <summary>
        /// 用于内容部件的控制点。
        /// </summary>
        public WidgetLinkerAdorner(UIElement adornedElement, Widgets.ContentWidget masterWidget)
            : base(adornedElement)
        {
            this.masterWidget = masterWidget;

            //终于解决了选定装饰器中线条宽度为2而不是1像素的问题！！！
            this.SnapsToDevicePixels = false;
            this.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);
            this.Opacity = 0.75;

            this.MouseLeftButtonDown +=
                new System.Windows.Input.MouseButtonEventHandler(LineCtrlAdorner_MouseLeftButtonDown);
            this.MouseEnter += new System.Windows.Input.MouseEventHandler(WidgetCenterAdorner_MouseEnter);
            this.MouseLeave += new System.Windows.Input.MouseEventHandler(WidgetCenterAdorner_MouseLeave);

            this.ToolTip = "右击指定链接目标页，\r\n左击跳转到目标页";
        }

        void WidgetCenterAdorner_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void WidgetCenterAdorner_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            this.InvalidateVisual();
        }

        void LineCtrlAdorner_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnDoubleClicked(sender, e);
            }
        }

        public event EventHandler<System.Windows.Input.MouseButtonEventArgs> DoubleClicked;

        protected void OnDoubleClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DoubleClicked != null)
            {
                DoubleClicked(sender, e);
            }
        }

        private Widgets.ContentWidget masterWidget = null;

        private static Pen renderPen;

        static WidgetLinkerAdorner()
        {
            renderPen = new Pen(Brushes.Green, 1);
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (masterWidget == null) return;

            Rect srcRect = new Rect(AdornedElement.RenderSize);
            Rect rect = new Rect(srcRect.Left + 1, srcRect.Top + 1, 10, 11);

            PathGeometry geometry = new PathGeometry();
            PathFigure pfArrow = new PathFigure();
            pfArrow.StartPoint = new Point(rect.Left + 3, rect.Top + 1);
            PolyLineSegment psArrow = new PolyLineSegment() { IsSmoothJoin = true, IsStroked = true };
            psArrow.Points = new PointCollection()
            {
                new Point(rect.Left + 7, rect.Top + 5),
                new Point(rect.Left + 3, rect.Top + 9),
            };
            pfArrow.Segments.Add(psArrow);
            geometry.Figures.Add(pfArrow);

            //如果链接到的页存在，则总是显示。
            bool haveLinkedPage = false;
            if (masterWidget.LinkedPageEditorId == null) haveLinkedPage = false;
            if (masterWidget.MasterEditor == null) haveLinkedPage = false;
            if (masterWidget.MasterEditor.MasterManager == null) haveLinkedPage = false;
            if (masterWidget.MasterEditor.MasterManager.GetPageEditor(masterWidget.LinkedPageEditorId) == null)
            {
                haveLinkedPage = false;
            }
            else haveLinkedPage = true;

            if (IsMouseOver || haveLinkedPage)// || masterWidget.IsMainSelected)//这个会导致当前文本块中的文本看不清楚。
            {
                drawingContext.DrawRectangle(Brushes.Green, renderPen, rect);
                drawingContext.DrawGeometry(Brushes.White, renderPen, geometry);
            }
            else
            {
                drawingContext.DrawRectangle(Brushes.Transparent, new Pen(Brushes.Transparent, 1), rect);
            }
        }
    }
}