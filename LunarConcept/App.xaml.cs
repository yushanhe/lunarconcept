﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Deployment.Application;
using System.Linq;
using System.Windows;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// App.xaml的交互逻辑代码。
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// .net 4.0 中，wpf 对剪贴板的操作有缺陷——当剪贴板被其它程序（如WPS、迅雷等）监视时，wpf 向剪贴板写入
        /// 文本时会弹出异常。
        /// 微软在 .net 4.5 中已解决这一问题。此程序如果将来要支持 .net 4.0（以便在 WinXP下使用）
        /// 则应加上下面的事件处理器来反复尝试向剪贴板写入。
        /// 
        /// 另，有人测试使用 Clipboard.SetData() 方法代替 Clipboard.SetText() 方法在 .net 4.0 下也可正常工作。
        /// </summary>
        //void Application_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        //{
        //    var comException = e.Exception as System.Runtime.InteropServices.COMException;
        //    if (comException != null && comException.ErrorCode == -2147221040)///OpenClipboard HRESULT:0x800401D0 (CLIPBRD_E_CANT_OPEN))
        //        e.Handled = true;
        //}

        /// <summary>
        /// 取命令行参数中的第一个（只支持打开一个文档），写入Globals.CmdParameterString属性。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // 取命令行参数。
            if (e.Args.Length > 0)
            {
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    string queryString = ApplicationDeployment.CurrentDeployment.ActivationUri.Query;
                    Globals.CmdParameterString = queryString;
                }
                else
                {
                    Globals.CmdParameterString = e.Args[0];
                }
            }
        }

        /// <summary>
        /// 此事件用以在“文档被修改且未保存的情况下，直接关闭Windows时让用户有机会保存文件”。
        /// </summary>
        private void Application_SessionEnding(object sender, SessionEndingCancelEventArgs e)
        {
            if (this.MainWindow != null)
            {
                MainWindow win = this.MainWindow as MainWindow;
                if (win != null)
                {
                    if (win.CloseDocument() != true) e.Cancel = true;
                    //2012年9月27日，前几天修改这段代码，但未起作用，不知道是否是因为微软的补丁造成的。
                }
            }
        }
    }
}
