﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年9月14日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于矩形部件，定义矩形部件“自动折行”关闭时，文本区域在矩形的哪个位置。
    /// </summary>
    public enum TextAreaAlignment
    {
        Center,         //默认
        
        Left,           //四正
        Top,
        Right,
        Bottom,

        TopLeft,        //四隅
        TopRight,
        BottomLeft,
        BottomRight,
    };
}
