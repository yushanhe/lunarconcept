﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年8月12日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于曲线部件。定义曲线是否按思维导图线方式绘制。
    /// </summary>
    public enum BezierLineForms
    {
        False = 0,                    //正常的曲线（为兼容而保留）
        BezierLine = 0,               //正常的曲线

        True = 1,                     //思维导图线（为兼容而保留）
        MindMapLinkLine = 1,          //思维导图线

        //TODO: 以后可以再添加
    };
}
