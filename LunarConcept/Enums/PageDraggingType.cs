﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年1月1日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：MousePointInfo类使用。PageEditor在进行鼠标操作时需要以此判断如何结束操作。
    /// </summary>
    public enum PageDraggingType
    {
        InsertBezierLine,
        InsertBracket,
        InsertEllipse,
        InsertPolyLine,
        InsertStraitLine,
        InsertRectangle,
        InsertRhomb,
        InsertTriangle,

        InsertBezierLineRelation,           //曲线型关系线（连接线）。
        InsertPolyLineRelation,             //折线型关系线（连接线）。
        InsertStraitLineRelation,           //默认添加直线型连接线（关系线）。

        MoveLineWidgetControler,
        MovePages,
        MoveWidgets,

        None,

        SelectWidgets,
        JumpToLinkedPageEditor,
    }
}
