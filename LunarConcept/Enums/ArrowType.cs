﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年1月4日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于线型部件，定义箭头方式。但对圆、矩形没有意义。
    /// </summary>
    public enum ArrowType { Start, End, All, None };
}
