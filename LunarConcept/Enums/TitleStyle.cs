﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年4月12日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于矩型部件，表示矩形部件中文本的标题等级。
    /// 　　　　　在通过某些途径添加矩形块时，会按这个枚举指定的标题等级进行格式化（以使此矩形看起来象个标题）。
    /// </summary>
    public enum TitleStyle
    {
        MainTitle = 0,              //主标题。                      横坐标区间：[EditArea.Left+4,EditArea.Right-4]  ★后五个尾端点横坐标相同。这个则更宽点。

        T1 = 1,                     //一、级标题                    横坐标区间：[EditArea.Left+ 2*EditorManager.DefaultFontSize,EditArea.Right-2*EditorManager.DefaultFontSize]  
        T2 = 2,                     //　（二）级标题                横坐标区间：[EditArea.Left+ 4*EditorManager.DefaultFontSize,EditArea.Right-2*EditorManager.DefaultFontSize]
        T3 = 3,                     //　　　３.级标题               横坐标区间：[EditArea.Left+ 6*EditorManager.DefaultFontSize,EditArea.Right-2*EditorManager.DefaultFontSize]  
        T4 = 4,                     //　　　　（４）级标题          横坐标区间：[EditArea.Left+ 8*EditorManager.DefaultFontSize,EditArea.Right-2*EditorManager.DefaultFontSize]      
        T5 = 5,                     //　　　　　　　缩进正文        横坐标区间：[EditArea.Left+10*EditorManager.DefaultFontSize,EditArea.Right-2*EditorManager.DefaultFontSize]

        Normal = 6,                 //无缩进正文　　　　　　　　　　横坐标区间：同顶级标题（主标题）[EditArea.Left+4,EditArea.Right-4]
    };
}
