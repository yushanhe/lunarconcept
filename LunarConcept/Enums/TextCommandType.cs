﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：自定义命令类（以“_TC”开头的类），需要在元数据上设置“命令别名”和“命令类别”。
    ///           此特性用以在元数据上设置“命令类别”。
    /// </summary>
    public enum TextCommandType
    {
        Appearance,         //外观操作
        Move,               //移动操作
        Alignment,          //对齐操作
        Insert,             //插入操作
        EditText,           //编辑文本操作
        Select,             //选定操作
        OnlyObserve,        //仅观察，不造成任何修改
    }
}
