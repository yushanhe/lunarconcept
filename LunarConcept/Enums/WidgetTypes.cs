﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年6月1日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于区别部件类型以便switch，免去使用反射的痛苦。
    /// </summary>
    public enum WidgetTypes
    {
        None,
        BezierLine,
        Bracket,
        Ellipse,
        Group,
        PictureBox,
        PolyLine,
        Rectangle,
        Rhomb,
        StraitLine,
        TextArea,
        Triangle,
        //TODO: 添加任何新部件类型，均需要在此处添加相应类型。
    };
}
