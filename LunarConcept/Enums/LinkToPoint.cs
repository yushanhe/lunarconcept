﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年12月30日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于直线作连接线时指定某端点连接到目标部件哪个位置。
    /// </summary>
    public enum LinkToPoint
    {
        Center,
        TopLeft,
        Top,
        TopRight,
        Left,
        Right,
        BottomLeft,
        Bottom,
        BottomRight,
        /// <summary>
        /// 用于制作时间轴。如果起点在指向部件的四正区域，则垂直对应边；如果起点位于四隅区域，则指向对应角端点。
        /// </summary>
        Border,
    };
}
