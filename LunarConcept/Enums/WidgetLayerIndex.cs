﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    public enum WidgetLayerIndex
    {
        Background = 0,
        ShapeWidget = 1,
        ContentWidget = 2,
        ArrowLineWidget = 3,
    }
}
