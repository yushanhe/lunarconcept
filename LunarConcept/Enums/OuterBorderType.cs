﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年9月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于内容部件，定义外框类型。
    /// </summary>
    public enum OuterBorderType
    {
        Rect,                       //矩形（★默认值）
        RoundRect,                  //圆角矩形
        Ellipse,                    //椭圆形

        Rhomb,                      //菱形
        LeftParallelogram,          //左右边向左下倾斜平行四边形
        RightParallelogram,         //左右边向右下倾斜平行四边形
        
        SixSideShape,               //六边形
        EightSidedShape,            //八边形
        
        Cross,                      //缺角矩形（十字形）

        FillArrowLeft,              //平底箭头（向左）
        FillArrowRight,             //平底箭头（向右）
        FillArrowTop,               //平底箭头（向上）
        FillArrowBottom,            //平底箭头（向下）

        ArrowLeft,                  //燕尾箭头（向左）
        ArrowRight,                 //燕尾箭头（向右）
        ArrowTop,                   //燕尾箭头（向上）
        ArrowBottom,                //燕尾箭头（向下）

    };
}
