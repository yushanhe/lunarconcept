﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年7月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于折线部件。定义折线是否按鱼骨图方式绘制。
    /// </summary>
    public enum PolyLineForms
    {
        PolyLine,                   //正常的折线
        //MainFishBone,               //鱼骨图主骨，不支持
        BigFishBone,                //鱼骨图大骨
        MediumFishBone,             //鱼骨图中骨
        //SmallFishBone,              //鱼骨图小骨，不支持

        CLine,                      //C形线（匚形线）
        LLine,                      //L形线（拐形线|∟形线）
    };
}
