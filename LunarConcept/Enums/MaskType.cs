﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// 创建时间：2012年2月16日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：规定ContentWidget的遮罩的形态。
    ///           MaskAdorner类需要用来决定如何绘制遮罩。
    /// </summary>
    public enum MaskType
    {
        None,               //无遮罩
        Blank,              //实底遮罩
        HLine,              //横线
        VLine,              //纵线
        HVLine,             //网格线
        LeftBias,           //左斜线
        RightBias,          //右斜线
        FillHLine,          //填充背景横线
        FillVLine,          //填充背景纵线
        FillHVLine,         //填充背景网格线
        FillLeftBias,       //填充背景左斜线
        FillRightBias,      //填充背景右斜线
    }
}
