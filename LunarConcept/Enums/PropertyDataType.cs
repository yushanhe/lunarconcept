﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept.Enums
{
    /// <summary>
    /// Widget类及其派生类中的各个需要由SetProperty()方法设置的属性的数据类型。
    /// 
    /// SetProperty()方法只处理这几种数据类型。
    /// </summary>
    public enum PropertyDateType
    {
        ArrowType,                      //箭头类型
        Bool,                           //逻辑真假
        Brush,                          //Brushes类中定义的SolidColor
        DashType,                       //点划线类型
        Double,                         //双精度浮点型
        Dock,                           //停靠位置（上、下、左、右）
        LayerIndex,                     //部件层次
        MaskType,                       //遮罩类型
        Orientation,                    //指向
        OutXmlText,                     //Xml文本
        ParagraphSetInnerXmlText,       //富文本内容
        Point,                          //点
        String,                         //文本
        Thickness,                      //厚度
        TitleStyle,                     //标题层级
        WidgetForm,                     //部件形态（如菱形有多种形态）
        PolyLineForm,                   //折线形态
        BezierLineForms,                //曲线形态
        BracketLineForm,                //括弧形态
        TextAreaAlignment,              //矩形部件在“自动折行”关闭时文本区域（不是区域内文本段落）的对齐状态
        OuterBorderType,                //内容部件的外框类型
        TriangleForm,                   //三角形形态
        LinkToPoint,                    //直线的端点连接到目标部件哪个连接位置
    }
}
