﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls.Ribbon;
using SHomeWorkshop.LunarConcept.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年1月2日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：向当前插入点位置插入一个新的PictureBox部件。
    /// </summary>
    public static class InsertPictureBoxCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态][构造方法]
        /// 
        /// ——此方法会初始化并向WPF系统注册一个RoutedUICommand。
        /// </summary>
        static InsertPictureBoxCommand()//类型构造器
        {
            routedUICmd = new RoutedUICommand(
                "InsertPictureBoxCommand",
                "InsertPictureBoxCommand",
                typeof(InsertPictureBoxCommand),//创建RoutedUICommand对象
                null);//本程序考虑支持“命令模式”因此，这些命令完全没有必要直接支持快捷键。

            //routedUICmd.InputGestures.Add(new KeyGesture(Key.P, ModifierKeys.Control, "Ctrl + P"));
            //2012年6月24日。此快捷键被指定为“打印选定页面”。
            //在主界面的ScrollViewer中，将单键P指定为此命令的快捷键。

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 判断命令是否可以执行。
        /// ——由于可以直接调用Execute()方法，因此，即使被禁用，也不是不能执行相关功能！！！
        /// </summary>
        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //PageEditor pe = manager.GetMainSelectedPageEditor();

            //e.CanExecute = (pe != null && pe.XmlData != null);
            e.CanExecute = true;
            return;
        }

        /// <summary>
        /// 命令被触发时，会调用本事件处理器方法。
        /// ——本方法实际上是调用ExeCute()这个静态方法来实现特定功能。
        /// </summary>
        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null || pe.XmlData == null)
            {
                return "　　未找到要添加图片框的页面，或此页面的后台数据为空。";
            }

            XmlNode peWidgetSetNode = pe.WidgetSetNode;
            if (peWidgetSetNode == null)
            {
                return "　　未找到目标页面的后台WidgetSet节点。";
            }

            XmlNode newPictureBox = peWidgetSetNode.AppendXmlAsChild(Properties.Resources.PictureBoxXml);

            PictureBox pb = new PictureBox(pe);
            pb.XmlData = newPictureBox;
            pb.NewID();
            pb.Location = pe.BaseInsertPoint;

            if (manager.FormatBrush != null)
            {
                pb.WidgetBackColor = manager.FormatBrush.DefaultWidgetStyle.WidgetBackColor;
                pb.WidgetForeColor = manager.FormatBrush.DefaultWidgetStyle.WidgetForeColor;
                pb.WidgetLineColor = manager.FormatBrush.DefaultWidgetStyle.WidgetLineColor;
                pb.WidgetLineWidth = manager.FormatBrush.DefaultWidgetStyle.WidgetLineWidth;
                pb.WidgetOpacity = manager.FormatBrush.DefaultWidgetStyle.WidgetOpacity;
                pb.WidgetPadding = manager.FormatBrush.DefaultWidgetStyle.WidgetPadding;
                pb.IsShadowVisible = manager.FormatBrush.DefaultWidgetStyle.IsShadowVisible;
                if (manager.FormatBrush.OnlyFormatOnceTime)
                {
                    manager.FormatBrush = null;
                }
            }

            //按默认值设置属性
            //pb.WidgetOpacity = manager.PictureBoxStyle.WidgetOpacity;
            //pb.WidgetForeColor = manager.PictureBoxStyle.WidgetForeColor;
            //pb.WidgetBackColor = manager.PictureBoxStyle.WidgetBackColor;
            //pb.WidgetLineWidth = manager.PictureBoxStyle.WidgetLineWidth;
            //pb.WidgetLineColor = manager.PictureBoxStyle.WidgetLineColor;
            //pb.IsShadowVisible = manager.PictureBoxStyle.IsShadowVisible;

            ModifingInfo info = new ModifingInfo();
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);

            info.ModifingDescription = "添加图片框";

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            Action actAddPictureBox = new Action(ActionType.WidgetAdded, pe.Id, pb.Id, null, pb.XmlData.OuterXml);

            pe.AddWidget(pb);
            pb.RefreshLocation();

            pb.SelectOnlySelf();

            mi.AddAction(actAddPictureBox);

            info.NewMainSelectedWidgetID = pb.Id;

            manager.RegisterModifingItem(mi);

            return string.Empty;
        }

        #endregion
    }
}
