﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls.Ribbon;
using SHomeWorkshop.LunarConcept.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using SHomeWorkshop.LunarConcept.Widgets;
using System.Windows.Controls;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年1月18日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：在当前主编辑器中添加一个折线部件。
    /// </summary>
    public static class InsertPolyLineWidgetCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态][构造方法]
        /// 
        /// ——此方法会初始化并向WPF系统注册一个RoutedUICommand。
        /// </summary>
        static InsertPolyLineWidgetCommand()//类型构造器
        {
            routedUICmd = new RoutedUICommand(
                "InsertPolyLineWidgetCommand",
                "InsertPolyLineWidgetCommand",
                typeof(InsertPolyLineWidgetCommand),//创建RoutedUICommand对象
                null);//本程序考虑支持“命令模式”因此，这些命令完全没有必要直接支持快捷键。

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 判断命令是否可以执行。
        /// ——由于可以直接调用Execute()方法，因此，即使被禁用，也不是不能执行相关功能！！！
        /// </summary>
        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //if (manager.GetMainSelectedPageEditor() == null)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true;
            return;
        }

        /// <summary>
        /// 命令被触发时，会调用本事件处理器方法。
        /// ——本方法实际上是调用ExeCute()这个静态方法来实现特定功能。
        /// </summary>
        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //Execute();//此命令不直接调用。
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(MouseLeftButtonDraggingEventArgs e)
        {
            if (e == null || e.MasterPageEditor == null) return "　　未传入MouseLeftButtonDraggingEventArgs参数，或者未指定页面。";

            EditorManager manager = e.MasterPageEditor.MasterManager;
            if (manager == null) return "　　未找到页面管理器。";

            XmlNode widgetSetNode = e.MasterPageEditor.WidgetSetNode;
            if (widgetSetNode == null)
            {
                return "　　未找到WidgetSet节点，无法添加直线。";
            }

            XmlNode newNode = widgetSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
            Widgets.PolyLineWidget plw = new Widgets.PolyLineWidget(e.MasterPageEditor);
            plw.XmlData = newNode;
            plw.NewID();
            plw.StartPoint = e.MasterPageEditor.MouseInfo.LeftButtonPreviewPoint;
            plw.EndPoint = e.MasterPageEditor.MouseInfo.LeftButtonPoint;
            plw.CenterCPPoint = new Point(plw.StartPoint.X + (plw.EndPoint.X - plw.StartPoint.X) / 2,
                plw.StartPoint.Y + (plw.EndPoint.Y - plw.StartPoint.Y) / 2);

            if (manager.FormatBrush != null)
            {
                plw.WidgetBackColor = manager.FormatBrush.DefaultWidgetStyle.WidgetBackColor;
                plw.WidgetForeColor = manager.FormatBrush.DefaultWidgetStyle.WidgetForeColor;
                plw.WidgetLineColor = manager.FormatBrush.DefaultWidgetStyle.WidgetLineColor;
                plw.WidgetLineWidth = manager.FormatBrush.DefaultWidgetStyle.WidgetLineWidth;
                plw.LineDash = manager.FormatBrush.DefaultWidgetStyle.LineDash;
                plw.WidgetOpacity = manager.FormatBrush.DefaultWidgetStyle.WidgetOpacity;
                plw.WidgetPadding = manager.FormatBrush.DefaultWidgetStyle.WidgetPadding;
                plw.IsShadowVisible = manager.FormatBrush.DefaultWidgetStyle.IsShadowVisible;
                if (manager.FormatBrush.OnlyFormatOnceTime)
                {
                    manager.FormatBrush = null;
                }
            }

            //按默认值设置属性
            //plw.WidgetOpacity = manager.PolyLineStyle.WidgetOpacity;
            //plw.Arrows = manager.PolyLineStyle.Arrows;
            //plw.WidgetForeColor = manager.PolyLineStyle.WidgetForeColor;
            //plw.WidgetBackColor = manager.PolyLineStyle.WidgetBackColor;
            //plw.WidgetLineWidth = manager.PolyLineStyle.WidgetLineWidth;
            //plw.LineDash = manager.PolyLineStyle.LineDash;
            //plw.IsShadowVisible = manager.PolyLineStyle.IsShadowVisible;

            if (Math.Abs(plw.EndPoint.X - plw.StartPoint.X) > Math.Abs(plw.EndPoint.Y - plw.StartPoint.Y))
            {
                plw.Direction = Orientation.Horizontal;
            }
            else
            {
                plw.Direction = Orientation.Vertical;
            }

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "添加折线" };
            e.MasterPageEditor.MasterManager.GetSelectedPageEditorStatus(info);
            e.MasterPageEditor.MasterManager.GetSelectedWidgetStatus_Old(info);
            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            Action actionAddPolyLine = new Action(ActionType.WidgetAdded,
                e.MasterPageEditor.Id, plw.Id, null, plw.XmlData.OuterXml);

            e.MasterPageEditor.AddWidget(plw);

            plw.SelectOnlySelf();

            info.NewMainSelectedWidgetID = plw.Id;
            mi.AddAction(actionAddPolyLine);

            if (e.MasterPageEditor.MasterManager != null)
            {
                e.MasterPageEditor.MasterManager.RegisterModifingItem(mi);
            }

            return string.Empty;
        }

        #endregion
    }
}
