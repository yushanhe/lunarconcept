﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年7月27日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置折线形部件的“线形态”。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class SetPolyLineFormCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static SetPolyLineFormCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "SetPolyLineFormCommand",
                "SetPolyLineFormCommand",
                typeof(SetPolyLineFormCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            //if (selectedWidgets.Count <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //int polyLineCount = 0;
            //foreach (Widgets.Widget w in selectedWidgets)
            //{
            //    if (w is Widgets.PolyLineWidget) polyLineCount++;
            //}

            //if (polyLineCount <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            //Execute();//这个命令不是直接调用。
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(Enums.PolyLineForms lineForm)
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor masterEditor = manager.GetMainSelectedPageEditor();
            if (masterEditor == null) return "　　未找到活动页面。";

            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();
            if (selectedWidgets.Count <= 0) return "　　未选定任何部件。";

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "设置折线部件形态";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            masterEditor.MasterManager.GetSelectedPageEditorStatus(info);
            masterEditor.MasterManager.GetSelectedWidgetStatus_Old(info);
            masterEditor.MasterManager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            int polyLineCount = 0;


            int unLinkedPolyLineCount = 0;

            if (lineForm == PolyLineForms.PolyLine)
            {
                foreach (Widgets.Widget w in selectedWidgets)
                {
                    Widgets.PolyLineWidget pw = w as Widgets.PolyLineWidget;
                    if (pw != null )
                    {
                        if (pw.LineForm != lineForm)
                        {
                            Action act = new Action(masterEditor.Id, pw.Id, pw.GetType().Name, XmlTags.LineFormTag,
                                pw.LineForm.ToString(), lineForm.ToString());

                            pw.LineForm = lineForm;

                            mi.AddAction(act);
                        }
                        polyLineCount++;
                    }
                }
            }
            else
            {
                foreach (Widgets.Widget w in selectedWidgets)
                {
                    Widgets.PolyLineWidget pw = w as Widgets.PolyLineWidget;
                    if (pw != null)
                    {
                        if (pw.LineForm != lineForm)
                        {
                            if (pw.IsLinked == false)
                            {
                                unLinkedPolyLineCount++;
                            }

                            Action act = new Action(masterEditor.Id, pw.Id, pw.GetType().Name, XmlTags.LineFormTag,
                                pw.LineForm.ToString(), lineForm.ToString());

                            pw.LineForm = lineForm;

                            mi.AddAction(act);
                        }
                        polyLineCount++;
                    }
                }
            }

            if (polyLineCount <= 0) return "　　没有选定任何折线形部件。";

            switch (lineForm)
            {
                case PolyLineForms.BigFishBone:
                case PolyLineForms.MediumFishBone:
                    {
                        if (unLinkedPolyLineCount > 0)
                        {
                            LunarMessage.Warning(string.Format("　　{0} 条折线并非连接线，虽然设置了“鱼骨线”形态，但不会起作用！", unLinkedPolyLineCount));
                        }
                        break;
                    }
                case PolyLineForms.CLine:
                    {
                        if (unLinkedPolyLineCount > 0)
                        {
                            LunarMessage.Warning(string.Format("　　{0} 条折线并非连接线，虽然设置了“匚形线”形态，但不会起作用！", unLinkedPolyLineCount));
                        }
                        break;
                    }
            }

            manager.RegisterModifingItem(mi);
            return string.Empty;
        }

        #endregion
    }
}
