﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年2月3日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置实现IWidgetPadding接口的部件（ContentWidget和ArrowLineWidget）的内间距。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class SetWidgetPaddingWidthCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static SetWidgetPaddingWidthCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "SetWidgetPaddingWidthCommand",
                "SetWidgetPaddingWidthCommand",
                typeof(SetWidgetPaddingWidthCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。

            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //List<Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            //if (selectedWidgets.Count <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //int contentWidgetsCount = 0;
            //foreach (Widget w in selectedWidgets)
            //{
            //    if (w is Widgets.ContentWidget) contentWidgetsCount++;
            //}

            //if (contentWidgetsCount <= 0)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            //Execute();//这个命令不是直接调用。
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(Thickness newThick)
        {
            if (newThick.Left < 0 || newThick.Right < 0 || newThick.Top < 0 || newThick.Bottom < 0) return "　　指定的内间距任意一边不能小于0。";

            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();
            if (selectedWidgets.Count <= 0) return "　　未选定任何部件。";

            int paddingWidgetCount = 0;
            foreach (Widget w in selectedWidgets)
            {
                if (w is Widgets.Interfaces.IWidgetPadding) paddingWidgetCount++;
            }

            if (paddingWidgetCount <= 0)
            {
                return "　　选定的部件中没有内容部件或线条型部件，只有这两类部件才支持设置内间距。";
            }

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "设置内间距";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            var pages = manager.GetAllPageEditors();
            foreach (var page in pages)
            {
                var selWidgets = page.GetSelectedWidgetsList();
                foreach (Widgets.Widget w in selWidgets)
                {
                    Action actSetPaddingWidth;

                    //注意：此处不能改为ICanBeLinkedWidget，因为只有ContentWidget和线条型部件才支持“内间距”这个属性。
                    Widgets.ContentWidget cw = w as Widgets.ContentWidget;
                    if (cw != null)
                    {
                        //ContentWidget内间距改变，如果被链接，则应考虑保持中心点位置。
                        if (cw.IsLinked)
                        {
                            Point oldTopLeft = cw.TopLeft;
                            Point oldBottomRight = cw.BottomRight;//备用

                            actSetPaddingWidth = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.WidgetPaddingTag,
                           Globals.ThickConverter.ConvertToString(cw.WidgetPadding), Globals.ThickConverter.ConvertToString(newThick));
                            cw.WidgetPadding = newThick;
                            mi.AddAction(actSetPaddingWidth);

                            //保持中心点
                            Point oldCenter = new Point(oldTopLeft.X + (oldBottomRight.X - oldTopLeft.X) / 2,
                                oldTopLeft.Y + (oldBottomRight.Y - oldTopLeft.Y) / 2);
                            cw.InvalidateArrange(); cw.UpdateLayout();

                            Point newTopLeft = cw.TopLeft;
                            Point newBottomRight = cw.BottomRight;
                            Point newLocation = new Point(oldCenter.X - (newBottomRight.X - newTopLeft.X) / 2,
                                oldCenter.Y - (newBottomRight.Y - newTopLeft.Y) / 2);

                            Action actNewLocation = new Action(cw.MasterEditor.Id, cw.Id, cw.GetType().Name, XmlTags.LocationTag,
                                cw.Location.ToString(), newLocation.ToString());
                            cw.Location = newLocation;
                            mi.AddAction(actNewLocation);
                        }
                        else
                        {
                            actSetPaddingWidth = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.WidgetPaddingTag,
                            Globals.ThickConverter.ConvertToString(cw.WidgetPadding), Globals.ThickConverter.ConvertToString(newThick));
                            cw.WidgetPadding = newThick;
                            mi.AddAction(actSetPaddingWidth);
                        }

                        cw.InvalidateArrange(); cw.UpdateLayout();

                        var linkedLines = cw.GetLinkedLines();
                        foreach (var line in linkedLines)
                        {
                            page.RefreshLinkedLines(mi, line);
                        }
                        continue;
                    }
                    else
                    {
                        Widgets.Interfaces.IWidgetPadding paddingWidget = w as Widgets.Interfaces.IWidgetPadding;
                        if (paddingWidget == null) continue;

                        actSetPaddingWidth = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.WidgetPaddingTag,
                        Globals.ThickConverter.ConvertToString(paddingWidget.WidgetPadding), Globals.ThickConverter.ConvertToString(newThick));
                        paddingWidget.WidgetPadding = newThick;
                        mi.AddAction(actSetPaddingWidth);

                        var clw = w as ICanBeLinkedWidget;
                        var linkedLines = clw.GetLinkedLines();
                        foreach (var line in linkedLines)
                        {
                            page.RefreshLinkedLines(mi, line);
                        }
                    }
                }
            }

            manager.RegisterModifingItem(mi);
            return string.Empty;
        }

        #endregion
    }
}
