﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年4月9日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置矩形部件的“IconXamlText”——对应的命令从右键菜单实现，名为“粘贴Xaml图标”。
    ///           这个属性用于极大地扩充矩形的功能，现在它几乎可以支持任何自定义的图标。配合Syncfusion Metro Studio 2这个软件使用，效果极好。
    /// 　　　　　
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class SetRectangleIconXamlTextCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static SetRectangleIconXamlTextCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "SetRectangleIconXamlTextCommand",
                "SetRectangleIconXamlTextCommand",
                typeof(SetRectangleIconXamlTextCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //List<Widgets.Widget> selWidgets = mainPe.GetSelectedWidgetsList();
            //if (selWidgets.Count <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //int rectanglesCount = 0;

            //foreach (Widgets.Widget w in selWidgets)
            //{
            //    if (w is Widgets.RectangleWidget) rectanglesCount++;
            //}

            //if (rectanglesCount <= 0)
            //{
            //    e.CanExecute = false; return;//至少选定一个内容部件。只有内容部件才支持设置备注文本。
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            LunarMessage.Warning(Execute(e.Parameter as string));
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(string newIconXamlText = "")
        {
            if (Globals.MainWindow == null) return "　　意外错误，未找到Globals.MainWindow";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到活动页面。";

            List<Widgets.Widget> selWidgets = mainPe.GetSelectedWidgetsList();

            if (selWidgets.Count <= 0) return "　　未选定任何矩形部件。";

            int rectanglesCount = 0;
            foreach (Widgets.Widget w in selWidgets)
            {
                if (w is Widgets.RectangleWidget) rectanglesCount++;
            }
            if (rectanglesCount <= 0) return "　　未选定任何矩形部件。";

            if (newIconXamlText == null || newIconXamlText.Length <= 0) return "传入了空文本。";

            //if (newMarkText.Length > 0)
            //{
            //    char fstChar = newMarkText[0];
            //    if (((fstChar >= 'a' && fstChar <= 'z') || (fstChar >= 'A' && fstChar <= 'Z')) == false)
            //        return "参数必须是英文字母或空字符串。";
            //}

            //if (newMarkText.Length > 1)
            //{
            //MessageBox.Show("　　矩形部件的“标记文本”只支持一个字符，且英文必定显示为大写字母。其它字符会被忽略。",
            //    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Information);
            //newMarkText = newMarkText.Substring(0, 1);
            //}

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "Xaml图标粘贴到矩形中";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);
            try
            {
                foreach (Widgets.Widget w in selWidgets)
                {
                    Widgets.RectangleWidget rw = w as Widgets.RectangleWidget;
                    if (rw == null) continue;

                    Action actSetIconXamlText = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.IconXamlTextTag,
                        rw.IconXamlText, newIconXamlText);
                    rw.IconXamlText = newIconXamlText;
                    mi.AddAction(actSetIconXamlText);
                }

                manager.RegisterModifingItem(mi);
                return string.Empty;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        #endregion
    }
}
