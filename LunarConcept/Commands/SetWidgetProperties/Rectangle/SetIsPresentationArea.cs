﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2016年1月5日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置矩形部件切换“是否演示区域”属性。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class SetIsPresentationAreaCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static SetIsPresentationAreaCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "SetIsPresentationAreaCommand",
                "SetIsPresentationAreaCommand",
                typeof(SetIsPresentationAreaCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            //if (selectedWidgets.Count <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //int rectangleCount = 0;
            //foreach (Widgets.Widget w in selectedWidgets)
            //{
            //    if (w is Widgets.RectangleWidget) rectangleCount++;
            //}

            //if (rectangleCount <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            //Execute();//这个命令不是直接调用。
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(bool isPresentation)
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor masterEditor = manager.GetMainSelectedPageEditor();
            if (masterEditor == null) return "　　未找到活动页面。";

            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();
            if (selectedWidgets.Count <= 0) return "　　未选定任何部件。";

            var mainSelWidget = masterEditor.GetMainSelectedWidget();
            var mainSelRectangle = mainSelWidget as Widgets.RectangleWidget;

            if (mainSelWidget == null || mainSelRectangle == null) return "　　请选择一个矩形部件。";

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "切换矩形部件“是否演示区域”属性";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            masterEditor.MasterManager.GetSelectedPageEditorStatus(info);
            masterEditor.MasterManager.GetSelectedWidgetStatus_Old(info);
            masterEditor.MasterManager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            int rectangleCount = 0;

            foreach (Widgets.Widget w in selectedWidgets)
            {
                Widgets.RectangleWidget rw = w as Widgets.RectangleWidget;
                if (rw != null)
                {
                    Action act = new Action(masterEditor.Id, rw.Id, rw.GetType().Name, XmlTags.IsPresentationAreaTag,
                        rw.IsPresentationArea.ToString(), isPresentation.ToString());
                    rw.IsPresentationArea = isPresentation;

                    mi.AddAction(act);

                    rectangleCount++;
                }
            }

            if (rectangleCount <= 0) return "　　没有选定任何矩形部件。";//这个文本千万别改。

            manager.RegisterModifingItem(mi);

            if (isPresentation)
            {
                #region 设置矩形演示区的MarkText，这将决定它的演示顺序

                string oldMarkText = string.Empty;

                oldMarkText = mainSelRectangle.MarkText;

                Dialogs.TextInputBox tiBox = new Dialogs.TextInputBox(Globals.MainWindow,
                    "　　请在下框中输入矩形的新标记文本。\r\n\r\n　　★注意：如果此矩形被定义为【演示区域】，则这里标记文本将决定演示的顺序。",
                    Globals.AppName, oldMarkText);

                if (tiBox.ShowDialog() == true)
                {
                    string result = Commands.TextCommands.RunTextCommand.Run("mk_" + tiBox.TbxInput.Text);
                    if (result != string.Empty)
                    {
                        MessageBox.Show("　　设置矩形标记文本出错。异常信息如下：\r\n" + result, Globals.AppName,
                             MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                #endregion
            }

            return string.Empty;
        }

        #endregion
    }
}
