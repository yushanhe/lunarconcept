﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年1月22日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：选定部件中心纵坐标分散对齐。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class AlignWidgetsDistributeVerticalCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static AlignWidgetsDistributeVerticalCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "AlignWidgetsDistributeVerticalCommand",
                "AlignWidgetsDistributeVerticalCommand",
                typeof(AlignWidgetsDistributeVerticalCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }

            //PageEditor pageEditor = manager.GetMainSelectedPageEditor();
            //if (pageEditor == null) { e.CanExecute = false; return; }

            //List<Widgets.Widget> selectedWidgets = pageEditor.GetSelectedWidgetsList(true);
            //if (selectedWidgets.Count < 3) { e.CanExecute = false; return; }

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor pageEditor = manager.GetMainSelectedPageEditor();
            if (pageEditor == null) return "　　未找到活动页面。";

            List<Widgets.Widget> selectedWidgets = pageEditor.GetSelectedWidgetsList(true);
            if (selectedWidgets.Count < 3) return "　　选定的部件数目小于３个，【分散对齐】无意义。";

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "部件垂直分散对齐" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            selectedWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = selectedWidgets[0];
            Widgets.Widget endWidget = selectedWidgets[selectedWidgets.Count - 1];

            Point startTopLeft = startWidget.TopLeft;
            Point endBottomRight = endWidget.BottomRight;

            int paddingCount = selectedWidgets.Count - 1;//间距数目为部件数目－１.

            double wholeHeight = endBottomRight.Y - startTopLeft.Y;
            double padding;

            double sumOfWidgetsHeights = 0;
            for (int i = 0; i < selectedWidgets.Count; i++)//首尾两个不需要重排，但需要计算尺寸。
            {
                sumOfWidgetsHeights += (selectedWidgets[i].BottomRight.Y - selectedWidgets[i].TopLeft.Y);
            }

            padding = ((wholeHeight - sumOfWidgetsHeights) / paddingCount);

            List<ILinkableLine> linkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            Rect startRect = new Rect(startWidget.TopLeft, startWidget.BottomRight);
            double offset = startTopLeft.Y + startRect.Height + padding;

            for (int i = 1; i < selectedWidgets.Count - 1; i++)//首尾两个不需要重排。
            {
                Widgets.Widget w = selectedWidgets[i];
                w.MoveTopSiderTo(mi, offset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);
                offset = w.TopLeft.Y + rect.Height + padding;
            }

            //刷新挂接的直线的位置
            pageEditor.RefreshLinkedLines(mi, linkedLines);
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();

            return string.Empty;
        }

        #endregion
    }
}
