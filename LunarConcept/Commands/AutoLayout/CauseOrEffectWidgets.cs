﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using WinForm = System.Windows.Forms;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Widgets;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年7月25日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：按因果关系图样式排列实体部件（（指ShapeWidget和ContentWidget,选定的），并在实体部件之间绘制连接线。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class CauseOrEffectWidgetsCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static CauseOrEffectWidgetsCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "CauseOrEffectWidgetsCommand",
                "CauseOrEffectWidgetsCommand",
                typeof(CauseOrEffectWidgetsCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。

            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //两个条件：⑴有活动部件；⑵除活动部件外，必须还有两个及以上被选定的部件。

            //if (Globals.MainWindow == null) { e.CanExecute = false; return; }

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null) { e.CanExecute = false; return; }

            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null) { e.CanExecute = false; return; }

            //List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();
            //if (selectedWidgets.Count < 3) { e.CanExecute = false; return; }//至少三个部件。

            //Widgets.Widget mainSelWidget = null;
            //int notLineWidgetsCount = 0;
            //foreach (Widgets.Widget w in selectedWidgets)
            //{
            //    Widgets.ArrowLineWidget alw = w as Widgets.ArrowLineWidget;
            //    if (alw != null) continue;

            //    if (w.IsMainSelected) mainSelWidget = w;
            //    notLineWidgetsCount++;
            //}

            //if (mainSelWidget == null || notLineWidgetsCount < 3)
            //{
            //    e.CanExecute = false;
            //    return;
            //}

            e.CanExecute = true;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。
            LunarMessage.Warning(Execute(e.Parameter as string));
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(string cmdParameter)
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未选定任何页面。";

            List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();
            if (selectedWidgets.Count < 3) return "　　至少选定三个实体部件。";

            Widgets.Widget mainSelWidget = null;
            List<Widgets.Widget> otherNotLineWidgets = new List<Widgets.Widget>();

            foreach (Widgets.Widget w in selectedWidgets)
            {
                Widgets.ArrowLineWidget alw = w as Widgets.ArrowLineWidget;
                if (alw != null) continue;
                //真正的线型部件不参与。而Shape虽然也是线型部件，但有实体区域，因此此处不算在线型部件。

                if (w.IsMainSelected)
                {
                    mainSelWidget = w;
                }
                else
                {
                    otherNotLineWidgets.Add(w);
                }
            }

            if (mainSelWidget == null || otherNotLineWidgets.Count < 2)//总数不能小于３（包括一个活动内容部件）
                return "　　没有找到活动部件，或选定的实体部件总数目小于３个。";

            string direction = "right";
            if (cmdParameter != null)
            {
                direction = cmdParameter.ToLower();
            }

            switch (direction)
            {
                case "leftcause":
                    {
                        return CauseOrEffectLeft(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Cause);
                    }
                case "lefteffect":
                    {
                        return CauseOrEffectLeft(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Effect);
                    }
                case "rightcause":
                    {
                        return CauseOrEffectRight(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Cause);
                    }
                case "righteffect":
                    {
                        return CauseOrEffectRight(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Effect);
                    }
                case "topcause":
                    {
                        return CauseOrEffectTop(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Cause);
                    }
                case "topeffect":
                    {
                        return CauseOrEffectTop(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Effect);
                    }
                case "bottomcause":
                    {
                        return CauseOrEffectBottom(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Cause);
                    }
                case "bottomeffect":
                    {
                        return CauseOrEffectBottom(manager, mainPe, mainSelWidget, selectedWidgets, otherNotLineWidgets, CauseOrEffect.Effect);
                    }
            }

            return "　　指定的方向不是上下左右任意一种。";
        }

        /// <summary>
        /// 用来决定箭头位置。Cause表示箭头会聚到MainSelecedWidget。而Effect则从MainSelectedWidget发散。
        /// </summary>
        private enum CauseOrEffect { Cause, Effect }

        private static string CauseOrEffectBottom(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets,
            CauseOrEffect causeOrEffect)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "下向因果关系图" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double sumWidthOfOthers = 0;
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumWidthOfOthers += w.RealRect.Width;
            }

            double padding = manager.DefaultFontSize;

            sumWidthOfOthers += padding * (otherNotLineWidgets.Count - 1);

            double baseOffset = mainSelWidget.TopLeft.X + mainSelWidget.RealRect.Width / 2 - sumWidthOfOthers / 2;

            double startOffset = baseOffset;
            //double otherNewTop = mainSelWidget.RealRect.Bottom + 80;

            Rect mainRect = mainSelWidget.RealRect;

            Point arcCenter = new Point(mainRect.X + mainRect.Width / 2, mainRect.Bottom + 10);
            double arcRadiu = sumWidthOfOthers / 2;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveLeftSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);

                //计算w的上中心点坐标。
                double xOfTopCenter = startOffset + rect.Width / 2;
                double yOfTopCenter = Math.Sqrt((arcRadiu * arcRadiu) - (xOfTopCenter - arcCenter.X) * (xOfTopCenter - arcCenter.X)) + arcCenter.Y;

                if (w.TopLeft.Y != yOfTopCenter)
                {
                    w.MoveTopSiderTo(mi, yOfTopCenter);
                }

                //计算下一个部件的横坐标偏移量。
                startOffset = w.TopLeft.X + rect.Width + padding;
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.End;

                if (causeOrEffect == CauseOrEffect.Cause)
                {
                    pw.StartMasterId = w.Id;
                    pw.EndMasterId = mainSelWidget.Id;
                }
                else
                {
                    pw.StartMasterId = mainSelWidget.Id;
                    pw.EndMasterId = w.Id;
                }

                pw.Direction = System.Windows.Controls.Orientation.Horizontal;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string CauseOrEffectTop(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets,
            CauseOrEffect causeOrEffect)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "上向因果关系图" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterXCompareClass());//按横坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double sumWidthOfOthers = 0;
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumWidthOfOthers += w.RealRect.Width;
            }

            double padding = manager.DefaultFontSize;

            sumWidthOfOthers += padding * (otherNotLineWidgets.Count - 1);

            double baseOffset = mainSelWidget.TopLeft.X + mainSelWidget.RealRect.Width / 2 - sumWidthOfOthers / 2;

            double startOffset = baseOffset;
            //double otherNewBottom = mainSelWidget.RealRect.Top - 80;

            Rect mainRect = mainSelWidget.OuterRect;

            Point arcCenter = new Point(mainRect.X + mainRect.Width / 2, mainRect.Top - 10);
            double arcRadiu = sumWidthOfOthers / 2;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveLeftSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);

                //计算w的底中心点坐标
                double xOfBottomCenter = startOffset + rect.Width / 2;
                double yOfBottomCenter = arcCenter.Y - Math.Sqrt((arcRadiu * arcRadiu) - (xOfBottomCenter - arcCenter.X) * (xOfBottomCenter - arcCenter.X));

                if (w.BottomRight.Y != yOfBottomCenter)
                {
                    w.MoveBottomSiderTo(mi, yOfBottomCenter);
                }

                //计算下一个部件的横坐标偏移量。
                startOffset = w.TopLeft.X + rect.Width + padding;
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.End;

                if (causeOrEffect == CauseOrEffect.Cause)
                {
                    pw.StartMasterId = w.Id;
                    pw.EndMasterId = mainSelWidget.Id;
                }
                else
                {
                    pw.StartMasterId = mainSelWidget.Id;
                    pw.EndMasterId = w.Id;
                }

                pw.Direction = System.Windows.Controls.Orientation.Horizontal;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        private static string CauseOrEffectRight(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets,
            CauseOrEffect causeOrEffect)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "右向因果关系图" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double sumHeightOfOthers = 0;
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumHeightOfOthers += w.RealRect.Height;
            }

            double padding = manager.DefaultFontSize;

            sumHeightOfOthers += padding * (otherNotLineWidgets.Count - 1);

            double baseOffset = mainSelWidget.TopLeft.Y + mainSelWidget.RealRect.Height / 2 - sumHeightOfOthers / 2;

            double startOffset = baseOffset;
            //double otherNewLeft = mainSelWidget.RealRect.Right + 80;

            //按一个虚拟的圆弧来排列：
            //这个圆的垂直径方程是：x=MainSelWidget.RealRect.Right+10
            //(y>= MainSelWidget.RightCenter.y- otherWidgetsNeedHeight/2)
            //(y<= MainSelWidget.RightCenter.y+ otherWidgetsNeedHeight/2)

            //otherWidgetsNeedSize的计算方法：
            //由各部件高度之和，加上（各部件高度之和/(部件数目-1））——这个是间隙的高度。

            Rect mainRect = mainSelWidget.RealRect;

            Point arcCenter = new Point(mainRect.Right + 10, mainRect.Y + mainRect.Height / 2);
            double arcRadiu = sumHeightOfOthers / 2;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveTopSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);

                //计算w的左中心点坐标。
                double yOfLeftCenter = startOffset + rect.Height / 2;
                double xOfLeftCenter = Math.Sqrt((arcRadiu * arcRadiu) - (yOfLeftCenter - arcCenter.Y) * (yOfLeftCenter - arcCenter.Y)) + arcCenter.X;

                if (w.TopLeft.X != xOfLeftCenter)
                {
                    w.MoveLeftSiderTo(mi, xOfLeftCenter);
                }

                //计算下一个部件的纵坐标偏移量。
                startOffset = w.TopLeft.Y + rect.Height + padding;
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.End;

                if (causeOrEffect == CauseOrEffect.Cause)//按因果颠倒箭头
                {
                    pw.StartMasterId = w.Id;
                    pw.EndMasterId = mainSelWidget.Id;
                }
                else
                {
                    pw.StartMasterId = mainSelWidget.Id;
                    pw.EndMasterId = w.Id;
                }

                pw.Direction = System.Windows.Controls.Orientation.Vertical;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        /// <summary>
        /// 指向MainSelWidgets和otherNotLineWidgets中某个部件的连接线才会被删除。
        /// </summary>
        private static void DeleteLinkedLines(PageEditor pe, ModifingItem<Action, ModifingInfo> mi,
            List<ILinkableLine> linkedLines, Widget mainSelWidget, List<Widget> otherNotLineWidgets)
        {
            if (pe == null || mi == null || linkedLines == null ||
                linkedLines.Count == 0 || mainSelWidget == null ||
                otherNotLineWidgets == null || otherNotLineWidgets.Count <= 0) return;

            foreach (ILinkableLine linkedLine in linkedLines)
            {
                if (linkedLine.StartMasterId != mainSelWidget.Id &&
                    linkedLine.EndMasterId != mainSelWidget.Id) continue;

                bool needDelete = false;

                if (linkedLine.StartMasterId == mainSelWidget.Id)
                {
                    //看EndMasterWidget是否在otherNotLineWidgets列表中
                    foreach (Widget w in otherNotLineWidgets)
                    {
                        if (w.Id == linkedLine.EndMasterId)
                        {
                            needDelete = true;
                            break;//需要删除
                        }
                    }
                }
                else if (linkedLine.EndMasterId == mainSelWidget.Id)
                {
                    //看StartMasterWidget是否在otherNotLineWidgets列表中
                    foreach (Widget w in otherNotLineWidgets)
                    {
                        if (w.Id == linkedLine.StartMasterId)
                        {
                            needDelete = true;
                            break;//需要删除
                        }
                    }
                }

                if (needDelete == false) continue;

                Action actDel = new Action(ActionType.WidgetDeleted, pe.Id, linkedLine.Id, linkedLine.XmlData.OuterXml, null);
                if (linkedLine.XmlData.ParentNode != null)
                {
                    linkedLine.XmlData.ParentNode.RemoveChild(linkedLine.XmlData);
                }

                UIElement ue = linkedLine as UIElement;
                if (ue != null)
                {
                    if (pe.Children.Contains(ue))
                    {
                        pe.Children.Remove(ue);
                    }
                }
                mi.AddAction(actDel);
            }
        }

        private static string CauseOrEffectLeft(EditorManager manager, PageEditor pageEditor,
            Widgets.Widget mainSelWidget,
            List<Widgets.Widget> selectedWidgets,
            List<Widgets.Widget> otherNotLineWidgets,
            CauseOrEffect causeOrEffect)
        {
            if (manager == null) return "　　未提供页面管理器。";
            if (pageEditor == null) return "　　未提供页面编辑器。";
            if (mainSelWidget == null) return "　　活动部件必须存在。";
            if (otherNotLineWidgets == null || otherNotLineWidgets.Count < 2) return "　　至少选定三个部件。";

            //活动部件位置不动，尝试垂直分散对齐选定的其它“实体（内容、形状）部件”。

            List<ILinkableLine> oldLinkedLines = pageEditor.GetLinkedLines(selectedWidgets);

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "左向因果关系图" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            //manager.GetSelectedWidgetStatus_New(info);//这个要移动到末尾

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            otherNotLineWidgets.Sort(new Tools.WidgetCenterYCompareClass());//按纵坐标排序。

            Widgets.Widget startWidget = otherNotLineWidgets[0];
            Widgets.Widget endWidget = otherNotLineWidgets[otherNotLineWidgets.Count - 1];

            double sumHeightOfOthers = 0;
            foreach (Widgets.Widget w in otherNotLineWidgets)
            {
                sumHeightOfOthers += w.RealRect.Height;
            }

            double padding = manager.DefaultFontSize;

            sumHeightOfOthers += padding * (otherNotLineWidgets.Count - 1);

            double baseOffset = mainSelWidget.TopLeft.Y + mainSelWidget.RealRect.Height / 2 - sumHeightOfOthers / 2;

            double startOffset = baseOffset;
            //double otherNewRight = mainSelWidget.RealRect.Left - 80;

            //按一个虚拟的圆弧来排列：
            //这个圆的垂直径方程是：x=MainSelWidget.RealRect.Right+10
            //(y>= MainSelWidget.LeftCenter.y- otherWidgetsNeedHeight/2)
            //(y<= MainSelWidget.LeftCenter.y+ otherWidgetsNeedHeight/2)

            //otherWidgetsNeedSize的计算方法：
            //由各部件高度之和，加上（各部件高度之和/(部件数目-1））——这个是间隙的高度。

            Rect mainRect = mainSelWidget.OuterRect;

            Point arcCenter = new Point(mainRect.Left - 10, mainRect.Y + mainRect.Height / 2);
            double arcRadiu = sumHeightOfOthers / 2;

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];
                w.MoveTopSiderTo(mi, startOffset);

                Rect rect = new Rect(w.TopLeft, w.BottomRight);

                //计算w的右中心点坐标
                double yOfRightCenter = startOffset + rect.Height / 2;
                double xOfRightCenter = arcCenter.X - Math.Sqrt((arcRadiu * arcRadiu) - (yOfRightCenter - arcCenter.Y) * (yOfRightCenter - arcCenter.Y));

                if (w.BottomRight.X != xOfRightCenter)
                {
                    w.MoveRightSiderTo(mi, xOfRightCenter);
                }

                //计算下一个部件的纵坐标偏移量。
                startOffset = w.TopLeft.Y + rect.Height + padding;
            }

            XmlNode pageWidgetsSetNode = pageEditor.WidgetSetNode;

            List<ILinkableLine> newLinkedLines = new List<ILinkableLine>();

            for (int i = 0; i < otherNotLineWidgets.Count; i++)
            {
                Widgets.Widget w = otherNotLineWidgets[i];

                XmlNode newNode = pageWidgetsSetNode.AppendXmlAsChild(Properties.Resources.PolyLineXml);
                PolyLineWidget pw = new PolyLineWidget(pageEditor);
                pw.XmlData = newNode;
                pw.NewID();
                pw.Arrows = Enums.ArrowType.End;

                if (causeOrEffect == CauseOrEffect.Cause)//按因果颠倒箭头
                {
                    pw.StartMasterId = w.Id;
                    pw.EndMasterId = mainSelWidget.Id;
                }
                else
                {
                    pw.StartMasterId = mainSelWidget.Id;
                    pw.EndMasterId = w.Id;
                }

                pw.Direction = System.Windows.Controls.Orientation.Vertical;

                pageEditor.Children.Add(pw);

                Action actInsert = new Action(ActionType.WidgetAdded, pageEditor.Id, pw.Id, null, pw.XmlData.OuterXml);
                mi.AddAction(actInsert);

                newLinkedLines.Add(pw);
            }

            //刷新挂接线的位置
            pageEditor.RefreshLinkedLines(mi, oldLinkedLines);
            pageEditor.RefreshLinkedLines(mi, newLinkedLines);

            DeleteLinkedLines(pageEditor, mi, oldLinkedLines, mainSelWidget, otherNotLineWidgets);
            manager.GetSelectedWidgetStatus_New(info);//最后再刷新选定状态。
            manager.RegisterModifingItem(mi);
            manager.RefreshAutoNumberStrings();
            return string.Empty;
        }

        #endregion
    }
}
