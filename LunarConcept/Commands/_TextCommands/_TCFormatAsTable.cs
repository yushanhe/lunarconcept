﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年3月25日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/fat/ft/tab/table/formatastable/bg/ewb/表格/二维表/表格格式化/格式化表格/按表格格式化/按表格样式格式化/",
        Enums.TextCommandType.Alignment, "尝试将当前选定的部件（通常应是矩形）按表格样式格式化")]
    public class _TCFormatAsTable
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCFormatAsTable");
                return string.Empty;
            }

            if (parameters == null || parameters.Trim().Length <= 0)
            {
                return Commands.FoamatAsTableCommand.Execute();
            }

            string parameterText = parameters.Trim();

            char[] splitter = new char[1];
            splitter[0] = ' ';//半角空格

            string[] texts = parameters.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            if (texts.Length == 0) return Commands.FoamatAsTableCommand.Execute();
            else if (texts.Length == 1)
            {
                try
                {
                    double padding = double.Parse(texts[0]);

                    if (padding < 0 || padding > 36) return "　　间距必须在[0,36]这个区间内！";

                    return Commands.FoamatAsTableCommand.Execute(padding, true);
                }
                catch (Exception ex)
                {
                    return "参数不正确。" + ex.Message;
                }
            }
            else if (texts.Length == 2)
            {
                try
                {
                    double padding = double.Parse(texts[0]);

                    if (padding < 0 || padding > 36) return "　　间距必须在[0,36]这个区间内！";

                    switch (texts[1])
                    {
                        case "f":
                        case "j":
                        case "unfix":
                        case "u":
                        case "unfixsize":
                        case "假":
                        case "不自适应尺寸":
                        case "不调整尺寸":
                            {
                                return Commands.FoamatAsTableCommand.Execute(padding, false);
                            }
                        default:
                            {
                                return Commands.FoamatAsTableCommand.Execute(padding, true);
                            }
                    }
                }
                catch (Exception ex)
                {
                    return "参数不正确。" + ex.Message;
                }
            }
            else
            {
                return "　　提供的参数数目不正确！";
            }
        }
    }
}
