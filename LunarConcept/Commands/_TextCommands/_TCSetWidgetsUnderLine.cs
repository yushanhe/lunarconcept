﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月17日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/ul/xhx/uline/underline/下划线/", Enums.TextCommandType.Appearance, "强行设置选定部件字体下划线")]
    public class _TCSetWidgetsUnderLine
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetWidgetsUnderLine");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";


            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            if (selectedWidgets.Count <= 0) return "　　未选定部件。";

            int titleCount = 0;
            {
                foreach (Widgets.Widget w in selectedWidgets)
                {
                    if (w.TitleLevel != Enums.TitleStyle.Normal) titleCount++;
                }
            }

            if (titleCount > 0)
            {
                MessageBox.Show(string.Format("　　选定的 {0} 个部件中，有 {1} 个是标题，此功能只对非标题部件有效！", selectedWidgets.Count, titleCount),
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            bool newUnderLine = true;
            if (parameters != null)
            {
                switch (parameters.Trim())
                {
                    case ""://空命令表示有有下划线。
                    case "k":
                    case "t":
                    case "true":
                    case "e":
                    case "enable":
                    case "有效":
                    case "启用":
                    case "真":
                    case "开":
                    case "开启":
                        {
                            newUnderLine = true; break;
                        }
                    case "n":
                    case "g":
                    case "f":
                    case "false":
                    //case "u":
                    case "unable":
                    case "无效":
                    case "取消":
                    case "不启用":
                    case "禁用":
                    case "假":
                    case "关":
                    case "关闭":
                    case "正常":
                        {
                            newUnderLine = false; break;
                        }
                }
            }

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "设置选定部件字体下划线" };
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            XmlDocument tmpDoc = new XmlDocument();

            foreach (Widgets.Widget w in selectedWidgets)
            {
                if (w.TitleLevel != Enums.TitleStyle.Normal) continue;

                XmlNode paragraphSetNode = w.ParagraphSetNode;
                string oldXml = paragraphSetNode.InnerXml;

                //<ParagraphSet <Paragraph <Text 

                XmlNodeList paragraphNodes = paragraphSetNode.SelectNodes(XmlTags.ParagraphTag);
                foreach (XmlNode paragraphNode in paragraphNodes)
                {
                    XmlNodeList textNodes = paragraphNode.SelectNodes(XmlTags.TextTag);
                    foreach (XmlNode textNode in textNodes)
                    {
                        textNode.SetAttribute(XmlTags.UnderLineTag, newUnderLine.ToString());
                    }
                }

                Action act = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.XmlDataInnerXml,
                    oldXml, paragraphSetNode.InnerXml);
                w.Build();//已更改，直接调用。
                mi.AddAction(act);
            }

            manager.RegisterModifingItem(mi);

            return string.Empty;
        }
    }
}
