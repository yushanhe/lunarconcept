﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月17日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/bjyy/yy/shadow/widgetshadow/阴影/部件阴影/", Enums.TextCommandType.Appearance, "设置选定部件的阴影")]
    public class _TCSetWidgetsShadow
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetWidgetsShadow");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";


            List<Widgets.Widget> selectedWidgets = manager.GetSelectedWidgetsList();

            if (selectedWidgets.Count <= 0) return "　　未选定部件。";

            FontStyle newFontStyle = FontStyles.Italic;
            if (parameters != null)
            {
                switch (parameters.Trim())
                {
                    case "":
                    case "k":
                    case "t":
                    case "true":
                    case "e":
                    case "enable":
                    case "有效":
                    case "启用":
                    case "真":
                    case "开":
                    case "开启":
                        {
                            return Commands.SetWidgetShadowOnCommand.Execute();
                        }
                    case "n":
                    case "g":
                    case "f":
                    case "false":
                    case "u":
                    case "unable":
                    case "无效":
                    case "取消":
                    case "不启用":
                    case "禁用":
                    case "假":
                    case "关":
                    case "关闭":
                    case "正常":
                        {
                            return Commands.SetWidgetShadowOffCommand.Execute();
                        }
                    default:
                        {
                            return "　　命令参数不正确。";
                        }
                }
            }
            else return Commands.SetWidgetShadowOnCommand.Execute();//不要参数就是打开。
        }
    }
}
