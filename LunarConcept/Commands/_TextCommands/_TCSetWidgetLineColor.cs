﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Tools;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月16日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/bks/xs/linecolor/bordercolor/borderground/线色/边线色/边框色/", Enums.TextCommandType.Appearance, "设置部件边框色")]
    public class _TCSetWidgetLineColor
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetWidgetLineColor");
                return string.Empty;
            }

            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到选定页面。";

            List<Widgets.Widget> selectedWidgets = mainPe.GetSelectedWidgetsList();
            if (selectedWidgets.Count <= 0) return "　　未选定部件。";

            Brush newBrush = BrushManager.GetBrushByEnglishName(parameters.Trim().ToLower());
            if (newBrush == null)
            {
                newBrush = BrushManager.GetBrushByChineseName(parameters.Trim().ToLower());
            }

            if (newBrush == null)
            {
                parameters = parameters.ToLower();
                switch (parameters)
                {
                    case "r":
                    case "random":
                    case "randomcolor":
                    case "sj":
                    case "sjs":
                    case "随机":
                    case "随机色":
                        {
                            return Commands.SetWidgetLineColorCommand.Execute(null, true);
                        }
                    default:
                        {
                            return "　　没有这种色彩：" + parameters;//按中文名称也找不到。
                        }
                }
            }

            Commands.SetWidgetLineColorCommand.Execute(newBrush);

            return string.Empty;
        }
    }
}
