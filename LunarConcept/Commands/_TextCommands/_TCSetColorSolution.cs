﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年2月17日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/ps/wdps/doccolor/doccolors/colorsolution/配色/文档配色/", Enums.TextCommandType.Appearance, "文档配色")]
    public class _TCSetColorSolution
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCSetColorSolution");
                return string.Empty;
            }

            string pText = parameters.Trim();
            switch (pText)
            {
                case "hdbz"://黑底白字
                case "bzhd":
                case "黑底白字":
                case "白字黑底":
                    {
                        List<WidgetStyle> styleList = new List<WidgetStyle>();

                        //文本块
                        WidgetStyle s1 = WidgetStyle.FromWidgetClassName(typeof(Widgets.TextArea).Name);
                        s1.WidgetOpacity = 1.0;
                        s1.WidgetBackColor = Brushes.Transparent;
                        s1.WidgetLineColor = Brushes.White;
                        s1.WidgetLineWidth = 1;
                        s1.WidgetForeColor = Brushes.White;
                        s1.WidgetPadding = new Thickness(2);
                        s1.IsShadowVisible = false;
                        styleList.Add(s1);

                        //图片框
                        WidgetStyle s2 = WidgetStyle.FromWidgetClassName(typeof(Widgets.PictureBox).Name);
                        s2.WidgetOpacity = 1.0;
                        s2.WidgetBackColor = Brushes.Transparent;
                        s2.WidgetLineColor = Brushes.White;
                        s2.WidgetLineWidth = 0;
                        s2.WidgetForeColor = Brushes.White;
                        s2.WidgetPadding = new Thickness(0);
                        s2.IsShadowVisible = false;
                        styleList.Add(s2);

                        //组
                        WidgetStyle s3 = WidgetStyle.FromWidgetClassName(typeof(Widgets.GroupWidget).Name);
                        s3.WidgetOpacity = 1.0;
                        s3.WidgetBackColor = Brushes.Transparent;
                        s3.WidgetLineColor = Brushes.White;
                        s3.WidgetLineWidth = 0;
                        s3.WidgetForeColor = Brushes.White;
                        s3.WidgetPadding = new Thickness(0);//这个不是10，而是0。这是为了便于计算坐标。
                        s3.IsShadowVisible = false;
                        styleList.Add(s3);

                        //直线
                        WidgetStyle s4 = WidgetStyle.FromWidgetClassName(typeof(Widgets.StraitLineWidget).Name);
                        s4.WidgetOpacity = 1.0;
                        s4.Arrows = Enums.ArrowType.End;
                        s4.WidgetBackColor = Brushes.Black;
                        s4.WidgetLineColor = Brushes.White;
                        s4.WidgetLineWidth = 2;
                        s4.LineDash = LineDashType.DashType.Solid;
                        s4.WidgetForeColor = Brushes.White;
                        s4.IsShadowVisible = false;
                        styleList.Add(s4);

                        //曲线
                        WidgetStyle s5 = WidgetStyle.FromWidgetClassName(typeof(Widgets.BezierLineWidget).Name);
                        s5.WidgetOpacity = 1.0;
                        s5.Arrows = Enums.ArrowType.End;
                        s5.WidgetBackColor = Brushes.Black;
                        s5.WidgetLineColor = Brushes.White;
                        s5.WidgetLineWidth = 2;
                        s5.LineDash = LineDashType.DashType.Solid;
                        s5.WidgetForeColor = Brushes.White;
                        s5.IsShadowVisible = false;
                        styleList.Add(s5);

                        //折线
                        WidgetStyle s6 = WidgetStyle.FromWidgetClassName(typeof(Widgets.PolyLineWidget).Name);
                        s6.WidgetOpacity = 1.0;
                        s6.Arrows = Enums.ArrowType.End;
                        s6.WidgetBackColor = Brushes.Black;
                        s6.WidgetLineColor = Brushes.White;
                        s6.WidgetLineWidth = 2;
                        s6.LineDash = LineDashType.DashType.Solid;
                        s6.WidgetForeColor = Brushes.White;
                        s6.IsShadowVisible = false;
                        styleList.Add(s6);

                        //括弧
                        WidgetStyle s7 = WidgetStyle.FromWidgetClassName(typeof(Widgets.BracketWidget).Name);
                        s7.WidgetOpacity = 1.0;
                        s7.Arrows = Enums.ArrowType.None;
                        s7.WidgetBackColor = Brushes.Transparent;//不在线上，不需要黑背景
                        s7.WidgetLineColor = Brushes.White;
                        s7.WidgetLineWidth = 2;
                        s7.LineDash = LineDashType.DashType.Solid;
                        s7.WidgetForeColor = Brushes.White;
                        s7.IsShadowVisible = false;
                        styleList.Add(s7);

                        //圆
                        WidgetStyle s8 = WidgetStyle.FromWidgetClassName(typeof(Widgets.EllipseWidget).Name);
                        s8.WidgetOpacity = 1.0;
                        s8.WidgetBackColor = Brushes.MediumSeaGreen;
                        s8.WidgetLineWidth = 1;
                        s8.WidgetLineColor = Brushes.MediumSeaGreen;
                        s8.LineDash = LineDashType.DashType.Solid;
                        s8.WidgetForeColor = Brushes.White;
                        s8.IsShadowVisible = false;
                        styleList.Add(s8);

                        //矩形
                        WidgetStyle s9 = WidgetStyle.FromWidgetClassName(typeof(Widgets.RectangleWidget).Name);
                        s9.WidgetOpacity = 1.0;
                        s9.WidgetBackColor = Brushes.DarkGoldenrod;
                        s9.WidgetLineWidth = 1;
                        s9.WidgetLineColor = Brushes.DarkGoldenrod;
                        s9.LineDash = LineDashType.DashType.Solid;
                        s9.WidgetForeColor = Brushes.White;
                        s9.IsShadowVisible = false;
                        styleList.Add(s9);

                        //菱形
                        WidgetStyle s10 = WidgetStyle.FromWidgetClassName(typeof(Widgets.RhombWidget).Name);
                        s10.WidgetOpacity = 1.0;
                        s10.WidgetBackColor = Brushes.LightSeaGreen;
                        s10.WidgetLineWidth = 1;
                        s10.WidgetLineColor = Brushes.LightSeaGreen;
                        s10.LineDash = LineDashType.DashType.Solid;
                        s10.WidgetForeColor = Brushes.White;
                        s10.IsShadowVisible = false;
                        styleList.Add(s10);

                        //三角形
                        WidgetStyle s11 = WidgetStyle.FromWidgetClassName(typeof(Widgets.TriangleWidget).Name);
                        s11.WidgetOpacity = 1.0;
                        s11.WidgetBackColor = Brushes.DarkBlue;
                        s11.WidgetLineWidth = 2;
                        s11.WidgetLineColor = Brushes.DarkBlue;
                        s11.LineDash = LineDashType.DashType.Solid;
                        s11.WidgetForeColor = Brushes.White;
                        s11.IsShadowVisible = false;
                        styleList.Add(s11);

                        return SetColorSolution(Brushes.Black, Brushes.GreenYellow,
                            Brushes.Cyan, Brushes.Pink, Brushes.GreenYellow, styleList);
                    }
                case "bdhz"://白底黑字
                case "hzbd":
                case "白底黑字":
                case "黑字白底":
                    {
                        if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";
                        EditorManager manager = Globals.MainWindow.EditorManager;
                        if (manager == null) return "　　未找到页面管理器。";

                        List<WidgetStyle> styleList = new List<WidgetStyle>();

                        //文本块
                        WidgetStyle s1 = manager.GetInitilizedWidgetStyle(typeof(Widgets.TextArea).Name);
                        WidgetStyle s2 = manager.GetInitilizedWidgetStyle(typeof(Widgets.PictureBox).Name);
                        WidgetStyle s3 = manager.GetInitilizedWidgetStyle(typeof(Widgets.GroupWidget).Name);

                        WidgetStyle s4 = manager.GetInitilizedWidgetStyle(typeof(Widgets.StraitLineWidget).Name);
                        WidgetStyle s5 = manager.GetInitilizedWidgetStyle(typeof(Widgets.BezierLineWidget).Name);
                        WidgetStyle s6 = manager.GetInitilizedWidgetStyle(typeof(Widgets.PolyLineWidget).Name);
                        WidgetStyle s7 = manager.GetInitilizedWidgetStyle(typeof(Widgets.BracketWidget).Name);

                        WidgetStyle s8 = manager.GetInitilizedWidgetStyle(typeof(Widgets.EllipseWidget).Name);
                        WidgetStyle s9 = manager.GetInitilizedWidgetStyle(typeof(Widgets.RectangleWidget).Name);
                        WidgetStyle s10 = manager.GetInitilizedWidgetStyle(typeof(Widgets.RhombWidget).Name);
                        WidgetStyle s11 = manager.GetInitilizedWidgetStyle(typeof(Widgets.TriangleWidget).Name);

                        styleList.Add(s1); styleList.Add(s2); styleList.Add(s3); styleList.Add(s4);
                        styleList.Add(s5); styleList.Add(s6); styleList.Add(s7); styleList.Add(s8);
                        styleList.Add(s9); styleList.Add(s10); styleList.Add(s11);

                        return SetColorSolution(Brushes.White, Brushes.Blue,
                            Brushes.Red, Brushes.Yellow, Brushes.Blue, styleList);
                    }
            }

            return "　　无此命令。";
        }

        private static string SetColorSolution(Brush background, Brush selectBoxColor,
            Brush startCtrlColor = null, Brush centerCPCtrlColor = null, Brush endCtrlColor = null,
            List<WidgetStyle> styleList = null)
        {
            if (Globals.MainWindow == null) return "　　未找到主窗口。";

            LunarConcept.Controls.EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到主窗口的页面管理器。";

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "文档配色";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            Brush newBackground = background;
            Brush newSelboxBrush = selectBoxColor;

            //写入文档背景
            TileMode newTileMode = TileMode.None;
            if (newTileMode != manager.DefaultBackTileMode)
            {
                Action actTileMode = new Action(ActionType.TileMode,
                    manager.DefaultBackTileMode.ToString(), newTileMode.ToString());

                manager.DefaultBackTileMode = newTileMode;

                mi.AddAction(actTileMode);
            }

            string newBrushTex = BrushManager.ParseToText(newBackground);
            Action actBackground = new Action(ActionType.DefaultBackgroundChanged,
                    manager.DocumentBackgroundText, newBrushTex);

            manager.DocumentBackgroundText = newBrushTex;
            mi.AddAction(actBackground);

            //写入“部件选定框色”。
            if (newSelboxBrush != manager.WidgetSelectionAdornerBrush)
            {
                Action actSelColor = new Action(ActionType.WidgetSelectionAdornerBrushChanged,
                    BrushManager.GetName(manager.WidgetSelectionAdornerBrush),
                    BrushManager.GetName(newSelboxBrush));

                manager.WidgetSelectionAdornerBrush = newSelboxBrush;
                mi.AddAction(actSelColor);
            }

            //写入“首控制点色”。
            if (startCtrlColor != null && startCtrlColor != manager.WidgetStartControlerBrush)
            {
                Action actStartCtrlColor = new Action(ActionType.WidgetStartCtrlBrushChanged,
                    BrushManager.GetName(manager.WidgetStartControlerBrush),
                    BrushManager.GetName(startCtrlColor));

                manager.WidgetStartControlerBrush = startCtrlColor;
                mi.AddAction(actStartCtrlColor);
            }

            //写入“尾控制点色”。
            if (centerCPCtrlColor != null && centerCPCtrlColor != manager.WidgetCenterControlerBrush)
            {
                Action actCenterCPCtrlColor = new Action(ActionType.WidgetCenterCtrlBrushChanged,
                    BrushManager.GetName(manager.WidgetCenterControlerBrush),
                    BrushManager.GetName(centerCPCtrlColor));

                manager.WidgetCenterControlerBrush = centerCPCtrlColor;
                mi.AddAction(actCenterCPCtrlColor);
            }

            //写入“尾控制点色”。
            if (endCtrlColor != null && endCtrlColor != manager.WidgetEndControlerBrush)
            {
                Action actEndCtrlColor = new Action(ActionType.WidgetEndCtrlBrushChanged,
                    BrushManager.GetName(manager.WidgetEndControlerBrush),
                    BrushManager.GetName(endCtrlColor));

                manager.WidgetEndControlerBrush = endCtrlColor;
                mi.AddAction(actEndCtrlColor);
            }

            if (styleList != null && styleList.Count > 0)
            {
                foreach (WidgetStyle style in styleList)
                {
                    WidgetStyle curStyle = manager.GetDefaultWidgetStyle(style.WidgetClassName);
                    if (style != null && curStyle != null)
                    {
                        Action actStyle = new Action(ActionType.SetWidgetStyle,
                            curStyle.ToString(), style.ToString());
                        manager.SetWidgetStyle(style);
                        mi.AddAction(actStyle);
                    }
                }
            }

            manager.RegisterModifingItem(mi);

            manager.MasterWindow.RefreshOutlineView(true);

            return string.Empty;
        }
    }
}
