﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands.TextCommands
{
    /// <summary>
    /// 创建时间：2012年3月7日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：提供自定义字符串命令的执行方法。
    ///           条件：⑴类名必须以“_TC”开头，否则会无法调用。
    ///                   类名即内置命令文本，用户输入的命令文本将被转换（通过映射表）成内置命令，
    ///                   根据内置命令字符串（即类名），找到类，并调用必须实现的下列方法：
    ///                 ⑵类必须实现带一个string型参数的:
    ///                   static string Execute(string parameters)方法，
    ///                   此方法必须自行处理parameters中以半角空格分隔的命令参数列表（当然，如果不需要，也可不处理）。
    /// </summary>
    [Tools.TextCommandAttribute("/ls/lb/lst/list/列表/",
            Enums.TextCommandType.Alignment, "八向列表布局")]
    public class _TCListWidgets
    {
        /// <summary>
        /// [静态方法]
        /// </summary>
        /// <param name="parameters">字符串形式的参数列表（参数间用半角空格分隔）。</param>
        /// <returns>顺利执行返回string.Empty，否则返回错误信息字符串。</returns>
        public static string Execute(string parameters)
        {
            if (parameters != null && parameters.Trim() == "?")
            {
                TextCommandHelp.ShowHelp("_TCListWidgets");
                return string.Empty;
            }

            string paText = parameters.Trim().ToLower();

            switch (paText)
            {
                case "lt":
                case "lefttop":
                case "zs":
                case "左上":
                    {
                        return Commands.ListWidgetsCommand.Execute("lefttop");
                    }
                case "lb":
                case "leftbottom":
                case "zx":
                case "左下":
                    {
                        return Commands.ListWidgetsCommand.Execute("leftbottom");
                    }
                case "rs":
                case "righttop":
                case "ys":
                case "右上":
                    {
                        return Commands.ListWidgetsCommand.Execute("righttop");
                    }
                case "rb":
                case "rightbottom":
                case "yx":
                case "右下":
                    {
                        return Commands.ListWidgetsCommand.Execute("rightbottom");
                    }
                case "tl":
                case "sz":
                case "ul":
                case "topleft":
                case "upleft":
                case "上左":
                    {
                        return Commands.ListWidgetsCommand.Execute("topleft");
                    }
                case "tr":
                case "sy":
                case "ur":
                case "topright":
                case "upright":
                case "上右":
                    {
                        return Commands.ListWidgetsCommand.Execute("topright");
                    }
                case "xz":
                case "bl":
                case "dl":
                case "bottomleft":
                case "downleft":
                case "底左":
                case "下左":
                    {
                        return Commands.ListWidgetsCommand.Execute("bottomleft");
                    }
                case ""://默认为下右。
                case "xy":
                case "br":
                case "dr":
                case "bottomright":
                case "downright":
                case "底右":
                case "下右":
                    {
                        return Commands.ListWidgetsCommand.Execute("bottomright");
                    }
            }

            return "　　指定的方位不是八向列表中的任意一种。";
        }
    }
}
