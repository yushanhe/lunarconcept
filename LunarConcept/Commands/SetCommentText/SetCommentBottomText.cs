﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Microsoft.Win32;
using System.Windows;
using System.Xml;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Tools;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年4月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置内容部件下注释文本。
    /// ★★说明：关于自定义命令的实现，可参考“NewDocument”类的备注。
    /// </summary>
    public static class SetCommentBottomTextCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// ★②，修改静态构造方法名。
        /// </summary>
        static SetCommentBottomTextCommand()//类型构造器
        {
            //★③，修改两个字符串参数名。★④以及第三个参数的类型名。
            routedUICmd = new RoutedUICommand(
                "SetCommentBottomTextCommand",
                "SetCommentBottomTextCommand",
                typeof(SetCommentBottomTextCommand),//创建RoutedUICommand对象
                null);

            //如果需要挂接快捷键，请参考下面这段代码：
            //routedUICmd = new RoutedUICommand(
            //    "SaveDocumentCommand",
            //    "SaveDocumentCommand",
            //    typeof(SaveDocumentCommand),//创建RoutedUICommand对象
            //    new InputGestureCollection() 
            //    { 
            //        //★⑤，修改此处三个参数，以便挂接快捷键。
            //        new KeyGesture(Key.S,ModifierKeys.Control,"Ctrl+S")
            //    });

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //★⑧，修改此方法的实现。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;

            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //List<Widgets.Widget> selWidgets = mainPe.GetSelectedWidgetsList();
            //if (selWidgets.Count <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            //int contentWidgetsCount = 0;

            //foreach (Widgets.Widget w in selWidgets)
            //{
            //    if (w is Widgets.ContentWidget) contentWidgetsCount++;
            //}

            //if (contentWidgetsCount <= 0)
            //{
            //    e.CanExecute = false; return;//至少选定一个内容部件。只有内容部件才支持设置备注文本。
            //}

            e.CanExecute = true; return;
        }

        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //★⑦，修改此方法的实现。

            LunarMessage.Warning(Execute(null));

        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        /// <param name="newCommentBottomText">只有为null时才会弹出输入框。</param>
        public static string Execute(string newCommentBottomText = "")
        {
            if (Globals.MainWindow == null) return "　　意外错误，未找到Globals.MainWindow";
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到活动页面。";

            List<Widgets.Widget> selWidgets = mainPe.GetSelectedWidgetsList();

            if (selWidgets.Count <= 0) return "　　未选定任何内容部件。";

            int contentWidgetCount = 0;
            foreach (Widgets.Widget w in selWidgets)
            {
                if (w is Widgets.ContentWidget) contentWidgetCount++;
            }
            if (contentWidgetCount <= 0) return "　　未选定任何内容部件。";

            if (newCommentBottomText == null)
            {
                Dialogs.TextInputBox tibox = new Dialogs.TextInputBox(Globals.MainWindow,
                            "请为【内容部件】输入下备注文本：", Globals.AppName, "");
                if (tibox.ShowDialog() != true) return string.Empty;

                newCommentBottomText = tibox.TbxInput.Text;
            }

            ModifingInfo info = new ModifingInfo();
            info.ModifingDescription = "设置内容部件下备注文本";
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);

            foreach (Widgets.Widget w in selWidgets)
            {
                Widgets.ContentWidget cw = w as Widgets.ContentWidget;
                if (cw == null) continue;

                Action actSetCommentText = new Action(w.MasterEditor.Id, w.Id, w.GetType().Name, XmlTags.CommentBottomTextTag,
                    cw.CommentBottomText, newCommentBottomText);
                cw.CommentBottomText = newCommentBottomText;
                mi.AddAction(actSetCommentText);
            }

            manager.RegisterModifingItem(mi);

            Globals.SwitchInputMethod(false);
            return string.Empty;
        }

        #endregion
    }
}
