﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls.Ribbon;
using SHomeWorkshop.LunarConcept.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2012年3月9日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：设置当前选定的所有部件（除连接线外）的锁定状态为（锁定｜解锁）状态之一。
    /// </summary>
    public static class LockWidgetsCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态][构造方法]
        /// 
        /// ——此方法会初始化并向WPF系统注册一个RoutedUICommand。
        /// </summary>
        static LockWidgetsCommand()//类型构造器
        {
            routedUICmd = new RoutedUICommand(
                "LockWidgetsCommand",
                "LockWidgetsCommand",
                typeof(RedoCommand),//创建RoutedUICommand对象
                null);//本程序考虑支持“命令模式”因此，这些命令完全没有必要直接支持快捷键。

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 判断命令是否可以执行。
        /// ——由于可以直接调用Execute()方法，因此，即使被禁用，也不是不能执行相关功能！！！
        /// </summary>
        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //考虑到性能问题，全部取消此判断。反正执行时会判断。
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //PageEditor mainPe = manager.GetMainSelectedPageEditor();
            //if (mainPe == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //int selWidgetsCount = mainPe.GetSelectedWidgetsCount();
            //if (selWidgetsCount <= 0)
            //{
            //    e.CanExecute = false; return;
            //}

            e.CanExecute = true;
            return;
        }

        /// <summary>
        /// 命令被触发时，会调用本事件处理器方法。
        /// ——本方法实际上是调用ExeCute()这个静态方法来实现特定功能。
        /// </summary>
        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            LunarMessage.Warning(Execute(e.Parameter as string));
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute(string parameter)
        {
            if (parameter == null) return "　　参数不合法。";

            bool isLocked;
            try
            {
                isLocked = bool.Parse(parameter);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPe = manager.GetMainSelectedPageEditor();
            if (mainPe == null) return "　　未找到任何活动页面。";

            List<Widgets.Widget> selWidgets = mainPe.GetSelectedWidgetsList();
            if (selWidgets.Count <= 0) return "　　请选定一个以上部件再执行此操作。";

            int linkedLinesCount = 0;
            foreach (Widgets.Widget w in selWidgets)
            {
                Widgets.StraitLineWidget sw = w as Widgets.StraitLineWidget;
                if (sw == null) continue;

                if (sw.IsLinked) linkedLinesCount++;
            }

            if (linkedLinesCount == selWidgets.Count) return "　　选定的部件全都是连接线，此操作无法继续。";

            ModifingInfo info = new ModifingInfo() { ModifingDescription = "设置部件锁定状态" };
            ModifingItem<Action, ModifingInfo> mi = new ModifingItem<Action, ModifingInfo>(info);
            manager.GetSelectedPageEditorStatus(info);
            manager.GetSelectedWidgetStatus_Old(info);
            manager.GetSelectedWidgetStatus_New(info);

            foreach (Widgets.Widget w in selWidgets)
            {
                if (w.IsLocked == isLocked) continue;

                Widgets.StraitLineWidget sw = w as Widgets.StraitLineWidget;
                if (sw != null && sw.IsLinked) continue;

                Action act = new Action(mainPe.Id, w.Id, w.GetType().Name, XmlTags.IsLockedTag,
                    w.IsLocked.ToString(), isLocked.ToString());
                w.IsLocked = isLocked;
                mi.AddAction(act);
            }

            manager.RegisterModifingItem(mi);
            return string.Empty;
        }

        #endregion
    }
}
