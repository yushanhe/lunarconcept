﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Controls.Ribbon;
using SHomeWorkshop.LunarConcept.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Commands
{
    /// <summary>
    /// 创建时间：2013年8月10日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：选定以StartPoint连接到当前活动部件，以EndPoint连接的另一（些）部件。
    /// </summary>
    public static class SelectLinkedToWidgetsCommand
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态][构造方法]
        /// 
        /// ——此方法会初始化并向WPF系统注册一个RoutedUICommand。
        /// </summary>
        static SelectLinkedToWidgetsCommand()//类型构造器
        {
            routedUICmd = new RoutedUICommand(
                "SelectLinkedToWidgetsCommand",
                "SelectLinkedToWidgetsCommand",
                typeof(SelectLinkedToWidgetsCommand),//创建RoutedUICommand对象
                null);//本程序考虑支持“命令模式”因此，这些命令完全没有必要直接支持快捷键。

            //routedUICmd.InputGestures.Add(new KeyGesture(Key.A, ModifierKeys.Control, "Ctrl + A"));

            cmdBinding.Command = routedUICmd;
            cmdBinding.CanExecute += new CanExecuteRoutedEventHandler(cmdBinding_CanExecute);
            cmdBinding.Executed += new ExecutedRoutedEventHandler(cmdBinding_Executed);
        }

        #endregion

        #region 字段与属性===================================================================================================

        private static CommandBinding cmdBinding = new CommandBinding();
        /// <summary>
        /// 用在主窗口CommandBindings集合中的命令绑定。
        /// 
        /// 它的Command是RoutedUICommand。
        /// ——因此，RoutedUICommand是否可以运行将由CmdBinding的CanExecute事件决定。
        /// ——而且，RoutedUICommand的执行也是通过CmdBinding的Execute事件来进行的。
        /// </summary>
        public static CommandBinding CmdBinding
        {
            get { return cmdBinding; }
        }

        private static RoutedUICommand routedUICmd;
        /// <summary>
        /// [只读静态属性]表示在WPF系统中注册的一个RoutedUICommand。
        /// ——必须和CommandBinding配合才能使用。
        ///     CommandBinding要添加到主窗口的CommandBindings集合中；
        ///     RoutedUICommand则要向WPF系统注册。
        ///     
        /// ★说明：使用静态属性是因为这样在Xaml代码中比较便于绑定。
        /// </summary>
        public static RoutedUICommand RoutedUICmd
        {
            get { return routedUICmd; }
        }

        #endregion

        #region 方法=========================================================================================================

        /// <summary>
        /// 判断命令是否可以执行。
        /// ——由于可以直接调用Execute()方法，因此，即使被禁用，也不是不能执行相关功能！！！
        /// </summary>
        static void cmdBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //if (Globals.MainWindow == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //EditorManager manager = Globals.MainWindow.EditorManager;
            //if (manager == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //PageEditor pe = manager.GetMainSelectedPageEditor();
            //if (pe == null)
            //{
            //    e.CanExecute = false; return;
            //}

            //Widgets.Interfaces.ICanBeLinkedWidget icw = pe.GetMainSelectedWidget() as Widgets.Interfaces.ICanBeLinkedWidget;
            //if (icw == null)
            //{
            //    e.CanExecute = false; return;
            //}

            e.CanExecute = true; return;
        }

        /// <summary>
        /// 命令被触发时，会调用本事件处理器方法。
        /// ——本方法实际上是调用ExeCute()这个静态方法来实现特定功能。
        /// </summary>
        static void cmdBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            LunarMessage.Warning(Execute());
        }

        /// <summary>
        /// [公开静态方法]即使此命令处于禁用状态，也可以通过代码调用此方法来执行特定任务！！！
        /// 
        /// 当被绑定的命令被调用（触发）时，会引发cmdBinding_Executed事件。
        /// 在cmdBinding_Executed事件处理器方法中已添加了调用Execute()方法的代码。
        /// 
        /// ——因此，触发命令，就相当于调用此方法！！！
        /// </summary>
        public static string Execute()
        {
            if (Globals.MainWindow == null) return "　　未找到Globals.MainWindow。";

            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return "　　未找到页面管理器。";

            PageEditor mainPageEditor = manager.GetMainSelectedPageEditor();
            if (mainPageEditor == null) return "　　未找到活动页面。";

            Widgets.Interfaces.ICanBeLinkedWidget icw = mainPageEditor.GetMainSelectedWidget() as Widgets.Interfaces.ICanBeLinkedWidget;
            if (icw == null) return "　　活动部件不是可连接部件。";

            List<Widgets.Interfaces.ILinkableLine> linkedLines = mainPageEditor.GetLinkedToLines(icw);

            if (linkedLines.Count <= 0)
            {
                return "　　没有找到我连接到的部件！";
            }

            manager.ClearWidgetsSelection();

            foreach (UIElement ue in mainPageEditor.MainCanvas.Children)
            {
                Widgets.Interfaces.ICanBeLinkedWidget icl = ue as Widgets.Interfaces.ICanBeLinkedWidget;
                if (icl == null) continue;

                foreach (Widgets.Interfaces.ILinkableLine linkedLine in linkedLines)
                {
                    if (linkedLine.StartMasterId == icl.Id)
                    {
                        icl.IsSelected = true;
                    }
                }
            }

            icw.IsMainSelected = true;

            return string.Empty;
        }

        #endregion
    }
}
