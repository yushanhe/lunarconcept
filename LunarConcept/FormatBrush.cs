﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SHomeWorkshop.LunarConcept
{
    public class FormatBrush
    {
        public WidgetStyle DefaultWidgetStyle { get; set; }

        private bool onlyFormatOnceTime = false;
        /// <summary>
        /// [读写]是否只格式化一次。
        /// </summary>
        public bool OnlyFormatOnceTime
        {
            get { return onlyFormatOnceTime; }
            set { onlyFormatOnceTime = value; }
        }
    }
}
