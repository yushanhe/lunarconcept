﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows.Media;
using System.Windows;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// 创建时间：2012年7月12日
    /// 创建者：　杨震宇
    /// 
    /// 主要用途：用于在主界面左侧大纲视图中显示一个标题文本块的层级。
    /// </summary>
    public class OutLineItem : ListBoxItem
    {
        public OutLineItem()
        {
            this.Background = Brushes.Transparent;
            this.Margin =
                this.BorderThickness = new Thickness(2);

            this.ToolTip = "[双击]或[选定后按回车]跳转到对应部件";
        }

        public EditorManager MasterManager { get; set; }

        public string MasterWidgetID { get; set; }

        public string MasterPageID { get; set; }

        public void BuildItem()
        {
            if (MasterManager == null)
            {
                this.Content = new TextBlock() { Text = "未找到页面管理器" };
                return;
            }

            if (string.IsNullOrEmpty(MasterPageID))
            {
                this.Content = new TextBlock() { Text = "未找到所在页面" };
                return;
            }

            PageEditor pe = MasterManager.GetPageEditor(MasterPageID);
            if (pe == null)
            {
                this.Content = new TextBlock() { Text = "未找到所在页面" };
                return;
            }

            Widgets.Widget w = pe.GetWidget(MasterWidgetID);
            if (w == null)
            {
                this.Content = new TextBlock() { Text = "未找到对应部件" };
                return;
            }

            if (w.TitleLevel == Enums.TitleStyle.Normal)
            {
                this.Content = new TextBlock() { Text = "正文..." };
                return;
            }

            StackPanel textPanel = w.BuildOutLineTextPanel();
            if (textPanel != null)
            {
                this.Content = textPanel;
            }
            else
            {
                this.Content = new TextBlock() { Text = "未能读入大纲" };
            }

            double fontSize = MasterManager.DefaultFontSize;
            //fontSize = TitleManager.GetFontSize(w.TitleLevel);
            Thickness margin;

            switch (w.TitleLevel)
            {
                case Enums.TitleStyle.T1:
                    {
                        margin = new System.Windows.Thickness(fontSize * 2 + 2, 2, 2, 2);
                        break;
                    }
                case Enums.TitleStyle.T2:
                    {
                        margin = new System.Windows.Thickness(fontSize * 4 + 2, 2, 2, 2);
                        break;
                    }
                case Enums.TitleStyle.T3:
                    {
                        margin = new System.Windows.Thickness(fontSize * 6 + 2, 2, 2, 2);
                        break;
                    }
                case Enums.TitleStyle.T4:
                    {
                        margin = new System.Windows.Thickness(fontSize * 8 + 2, 2, 2, 2);
                        break;
                    }
                case Enums.TitleStyle.T5:
                    {
                        margin = new System.Windows.Thickness(fontSize * 10 + 2, 2, 2, 2);
                        break;
                    }
                default:
                    {
                        margin = new Thickness(2); break;
                    }
            }

            this.Margin = margin;
        }
    }
}
