﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SHomeWorkshop.LunarConcept
{
    public class PresentationAreaItem : ListBoxItem
    {
        public PresentationAreaItem(Widgets.RectangleWidget masterRW)
        {
            this.masterRW = masterRW;

            if (this.masterRW != null)
            {
                this.baseImage.Source = new BitmapImage(new Uri("pack://Application:,,,/LunarConcept;component/Images/presentationAreaItem.png"));
            }

            tbMarktext.Text = masterRW.MarkText;
            tbComment.Text = masterRW.CommentText;

            if (string.IsNullOrWhiteSpace(tbComment.Text))
            {
                tbMarktext.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                tbComment.Visibility = System.Windows.Visibility.Collapsed;
            }

            DockPanel.SetDock(baseImage, Dock.Left);
            basePanel.Children.Add(baseImage);

            DockPanel.SetDock(tbMarktext, Dock.Top);
            basePanel.Children.Add(tbMarktext);


            DockPanel.SetDock(tbComment, Dock.Bottom);
            basePanel.Children.Add(tbComment);
            this.MouseDoubleClick += PresentationAreaItem_MouseDoubleClick;
            this.Content = basePanel;
        }

        private void PresentationAreaItem_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.masterRW == null) return;
            Globals.MainWindow.TryToShowWidgetInScrollViewer(this.masterRW);
        }

        private Widgets.RectangleWidget masterRW = null;

        public Widgets.RectangleWidget MasterRW { get { return this.masterRW; } }

        private DockPanel basePanel = new DockPanel() { LastChildFill = true, };
        private Image baseImage = new Image()
        {
            Margin = new System.Windows.Thickness(2),
            Width = 15,
            Height = 15,
            SnapsToDevicePixels = true,
            VerticalAlignment = System.Windows.VerticalAlignment.Center,
        };
        private TextBlock tbMarktext = new TextBlock() { Margin = new System.Windows.Thickness(2) };
        private TextBlock tbComment = new TextBlock()
        {
            Foreground = Brushes.SaddleBrown,
            Margin = new System.Windows.Thickness(2),
            TextWrapping = System.Windows.TextWrapping.Wrap,
        };
    }
}
