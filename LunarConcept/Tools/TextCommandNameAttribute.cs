﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Tools
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建人：杨震宇
    /// 
    /// 主要用途：用在自定义文本命令类上的特性用于给自定义文本命令类提供一个“别名”。
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class TextCommandAttribute : Attribute
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="aliasName">命令别名（英文）。注意：不能带全半角空格、“_”、“/”。</param>
        /// <param name="zh_Alias">命令别名（中文）。注意：不能带全半角空格、“_”、“/”。</param>
        /// <param name="commandType">命令类型。
        /// 此类型必定是\Enums\TextCommandType.cs中的“TextCommandType”枚举定义的几种数据类型之一。
        /// </param>
        /// <param name="description">命令的简单描述。</param>
        public TextCommandAttribute(string aliasNames, TextCommandType commandType, string description)
        {
            this.aliasNames = aliasNames;
            this.commandType = commandType;
            this.description = description;
        }

        private string aliasNames;
        /// <summary>
        /// [只读]命令别名。支持多个命令别名，这些命令别名间以“/”分隔。
        /// </summary>
        public string AliasNames
        {
            get { return aliasNames; }
        }

        private string zh_Alias;
        /// <summary>
        /// [只读]命令的中文别名。
        /// </summary>
        public string Zh_Alias
        {
            get { return zh_Alias; }
            set { zh_Alias = value; }
        }

        private TextCommandType commandType;
        /// <summary>
        /// [只读]命令类型枚举值。
        /// </summary>
        public TextCommandType CommandType
        {
            get { return commandType; }
        }

        private string description;
        /// <summary>
        /// [只读]关于命令功能的简单描述。
        /// </summary>
        public string Description
        {
            get { return description; }
        }
    }
}
