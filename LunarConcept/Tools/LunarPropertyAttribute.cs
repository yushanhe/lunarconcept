﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Enums;

namespace SHomeWorkshop.LunarConcept.Tools
{
    /// <summary>
    /// 创建时间：2011年12月26日
    /// 创建人：杨震宇
    /// 
    /// 主要用途：用在需要由SetProperty()方法设置值的Widget类及其派生类的某些属性上的特性。
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class LunarPropertyAttribute : Attribute
    {
        /// <summary>
        /// [构造方法]
        /// </summary>
        /// <param name="aliasName">要设置特性的Widget类及其派生类的某个属性的名称。</param>
        /// <param name="commandType">
        /// 要设置特性的Widget类及其派生类的某个属性的数据类型。
        /// 此类型必定是\Enums\PropertyDataType.cs中的“PropertyDateType”枚举定义的几种数据类型之一。
        /// </param>
        public LunarPropertyAttribute(string propertyName, PropertyDateType propertyType)
        {
            this.propertyName = propertyName;
            this.propertyType = propertyType;
        }

        private string propertyName;
        /// <summary>
        /// [只读]属性名称。
        /// 
        /// ☆这个属性并非必需——因为“PropertyInfo”中本就有Name。
        /// 
        /// </summary>
        public string PropertyName
        {
            get { return propertyName; }
        }

        private PropertyDateType propertyType;
        /// <summary>
        /// [只读]属性的数据类型枚举值。
        /// </summary>
        public PropertyDateType PropertyType
        {
            get { return propertyType; }
        }
    }
}
