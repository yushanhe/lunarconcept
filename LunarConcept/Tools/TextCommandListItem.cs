﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Tools
{
    /// <summary>
    /// 创建时间：2012年2月6日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：为主界面的“自定义命令列表”提供项目，说明命令。
    /// </summary>
    class TextCommandListItem : ListBoxItem
    {
        public TextCommandListItem(string textCmdClassName, string[] texts)
        {
            this.textCmdClassName = textCmdClassName;

            this.mainBorder.Child = this.mainStackPanel;
            this.Content = this.mainBorder;

            foreach (string s in texts)
            {
                this.mainStackPanel.Children.Add(new TextBlock() { Text = s, TextWrapping = TextWrapping.Wrap });
            }

            this.KeyDown += new System.Windows.Input.KeyEventHandler(TextCommandListItem_KeyDown);
            this.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(TextCommandListItem_MouseDoubleClick);
        }

        void TextCommandListItem_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                //显示帮助文档
                Commands.ShowHelpCommand.ShowHelpPage(textCmdClassName);
                e.Handled = true;
                return;
            }

            switch (e.Key)
            {
                case System.Windows.Input.Key.Up:
                case System.Windows.Input.Key.Down:
                case System.Windows.Input.Key.PageDown:
                case System.Windows.Input.Key.PageUp:
                    {
                        return;
                    }
                default:
                    {
                        e.Handled = true;
                        return;
                    }
            }
        }

        void TextCommandListItem_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //显示帮助文档
            Commands.ShowHelpCommand.ShowHelpPage(textCmdClassName);
        }

        private string textCmdClassName;
        /// <summary>
        /// [只读]命令类的类名。
        /// </summary>
        public string TextCmdClassName
        {
            get { return textCmdClassName; }
        }

        private static Thickness defaultMargin = new Thickness(5);

        private static Thickness defaultBorderThickness = new Thickness(2);

        private static Thickness defaultBorderPadding = new Thickness(5);

        private Border mainBorder = new Border()
        {
            BorderBrush = Brushes.Green,
            BorderThickness = defaultBorderThickness,
            Padding = defaultBorderPadding,
            Margin = defaultMargin,
            HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
        };

        private StackPanel mainStackPanel = new StackPanel();
    }
}
