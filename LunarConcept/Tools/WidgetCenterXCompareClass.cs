﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Tools
{
    /// <summary>
    /// 创建时间：2012年1月22日（除夕）
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：在垂直分散对齐与水平分散对齐时，需要按当前实际中心点位置对选定的部件进行排序。
    /// </summary>
    public class WidgetCenterXCompareClass : IComparer<Widgets.Widget>
    {
        public int Compare(Widgets.Widget x, Widgets.Widget y)
        {
            if (x == y) return 0;

            Point xCenter = x.Center;
            Point yCenter = y.Center;

            if (xCenter.X == yCenter.X) return 0;
            else return xCenter.X > yCenter.X ? 1 : -1;
        }
    }
}
