﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunarSF.SHomeWorkshop.LunarMarkdownEditor
{
    public class MarkdownFileInfo
    {
        public MarkdownFileInfo(string fullPath, string tailPath)
        {
            FullPath = fullPath;
            TailPath = tailPath;
        }

        public string FullPath { get; } = "";

        public string TailPath { get; } = "";
    }

    public class MarkdownFile
    {
        public MarkdownFile(string title, string path)
        {
            this.Title = title;
            this.FullPath = path;
        }
        public string FullPath { get; set; }
        public string Title { get; set; }
    }
}
