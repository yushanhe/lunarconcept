﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml;
using System.Windows.Input;
using System;
using SHomeWorkshop.LunarConcept.Controls;
using SHomeWorkshop.LunarConcept.Adorners;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using SHomeWorkshop.LunarConcept.Enums;
using SHomeWorkshop.LunarConcept.Widgets.Interfaces;
using System.Collections.Generic;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    /// <summary>
    /// 创建时间：2012年1月20日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：三角形部件。
    /// </summary>
    public class TriangleWidget : ShapeWidget, ICanBeLinkedWidget, Widgets.Interfaces.ITextRotate
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态构造方法]
        /// </summary>
        static TriangleWidget()
        {
            dashArray = new DoubleCollection() { 2, 2 };
            contextMenu = (ContextMenu)Globals.MainWindow.MainGrid.FindResource("CMDicTriangleWidget");
        }

        /// <summary>
        /// [构造方法]
        /// </summary>
        public TriangleWidget(PageEditor masterEditor)
            : base(masterEditor)
        {
            this.ContextMenu = contextMenu;

            widgetType = Enums.WidgetTypes.Triangle;
            widgetClassLocalName = Widget.GetWidgetClassLocalName(this.GetType().Name);

            this.mainPath.Data = this.pathGeometry;
            this.pathGeometry.Figures.Add(this.pathFigure);
            this.pathFigure.Segments.Add(this.arcSegmentAtInner);
            this.pathFigure.Segments.Add(this.startLineSegment);
            this.pathFigure.Segments.Add(this.arcSegmentAtOuter);
            this.pathFigure.Segments.Add(this.endLineSegment);

            //mainPolygon.Fill = null;
            //mainPolygon.Stroke = WidgetForeColor;
            mainPath.Fill = null;
            mainPath.Stroke = WidgetForeColor;

            //mainPolygon.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(mainPolygon_PreviewMouseLeftButtonDown);
            //mainPolygon.PreviewMouseRightButtonUp += new MouseButtonEventHandler(mainPolygon_PreviewMouseRightButtonUp);
            mainPath.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(mainPath_PreviewMouseLeftButtonDown);
            mainPath.PreviewMouseRightButtonUp += new MouseButtonEventHandler(mainPath_PreviewMouseRightButtonUp);

            //this.mainCanvas.Children.Add(mainPolygon);
            this.mainCanvas.Children.Add(mainPath);

            //Canvas.SetZIndex(mainPolygon, 0);
            Canvas.SetZIndex(mainPath, 0);

            //centerCPCtrl = new LineCtrlAdorner(this.mainPolygon, this, Brushes.Violet) { Visibility = Visibility.Hidden };
            //startCtrl = new LineCtrlAdorner(this.mainPolygon, this, Brushes.Red) { Visibility = Visibility.Hidden };
            //endCtrl = new LineCtrlAdorner(this.mainPolygon, this, Brushes.Blue) { Visibility = Visibility.Hidden };
            centerCPCtrl = new LineCtrlAdorner(this.mainPath, this, Brushes.Violet) { Visibility = Visibility.Hidden };
            startCtrl = new LineCtrlAdorner(this.mainPath, this, Brushes.Red) { Visibility = Visibility.Hidden };
            endCtrl = new LineCtrlAdorner(this.mainPath, this, Brushes.Blue) { Visibility = Visibility.Hidden };

            startCtrl.ToolTip = endCtrl.ToolTip = CenterCPCtrl.ToolTip = "双击鼠标左键设置备注文本";

            //中点装饰器。
            //this.centerAdorner = new WidgetCenterAdorner(this.mainPolygon, this, Brushes.Black) { ToolTip = "按鼠标左键向外拖动，生成关系节点" };
            this.centerAdorner = new WidgetCenterAdorner(this.mainPath, this, Brushes.Black) { ToolTip = "按鼠标左键向外拖动，生成关系节点" };
            this.centerAdorner.PreviewMouseRightButtonUp += new MouseButtonEventHandler(centerAdorner_PreviewMouseRightButtonUp);
            this.centerAdorner.MouseLeftButtonDown += new MouseButtonEventHandler(centerAdorner_MouseLeftButtonDown);

            //this.commentAdorner = new CommentAdorner(this.mainPolygon, this) { Visibility = System.Windows.Visibility.Collapsed };//默认不显示。
            this.commentAdorner = new CommentAdorner(this.mainPath, this) { Visibility = System.Windows.Visibility.Collapsed };//默认不显示。
            this.commentAdorner.MouseLeftButtonUp += new MouseButtonEventHandler(commentAdorner_MouseLeftButtonUp);

            //this.hyperLinkAdorner = new HyperLinkAdorner(this.mainPolygon, this) { Visibility = Visibility.Collapsed };
            this.hyperLinkAdorner = new HyperLinkAdorner(this.mainPath, this) { Visibility = Visibility.Collapsed };
            this.hyperLinkAdorner.MouseLeftButtonUp += new MouseButtonEventHandler(hyperLinkAdorner_MouseLeftButtonUp);

            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this.mainCanvas);
            if (adornerLayer == null)
            {
                MessageBox.Show("　　未找到Widget的装饰层！", Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                adornerLayer.Add(this.centerCPCtrl);
                adornerLayer.Add(this.startCtrl);
                adornerLayer.Add(this.endCtrl);
                adornerLayer.Add(this.centerAdorner);
                adornerLayer.Add(this.commentAdorner);//这个要在各具体部件类中添加。
                adornerLayer.Add(this.hyperLinkAdorner);//这个要在各具体部件类中添加。
            }

            centerCPCtrl.MouseLeftButtonDown += new MouseButtonEventHandler(centerCPCtrl_MouseLeftButtonDown);
            startCtrl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(startCtrl_MouseLeftButtonDown);
            endCtrl.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(endCtrl_MouseLeftButtonDown);

            //最后添加文本面板。
            this.mainCanvas.Children.Add(this.mainTextPanel);
            Canvas.SetZIndex(this.mainTextPanel, 2);

            this.ControlHandlerDoubleClicked += TriangleWidget_ControlHandlerDoubleClicked;
        }

        private void TriangleWidget_ControlHandlerDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        #endregion


        #region 字段与属性===================================================================================================

        public override Point BottomRight
        {
            get
            {
                double minLeft, minTop, maxRight, maxBottom;

                minLeft = Math.Min(Math.Min(startPoint.X, endPoint.X), centerCPPoint.X);
                maxRight = Math.Max(Math.Max(startPoint.X, endPoint.X), centerCPPoint.X);

                minTop = Math.Min(Math.Min(startPoint.Y, endPoint.Y), centerCPPoint.Y);
                maxBottom = Math.Max(Math.Max(startPoint.Y, endPoint.Y), centerCPPoint.Y);

                return new Point(maxRight, maxBottom);
            }
        }

        protected WidgetCenterAdorner centerAdorner;
        /// <summary>
        /// [只读]用于“拖出自动连接线”的装饰器。一般应在中心位置。
        /// </summary>
        public WidgetCenterAdorner CenterAdorner
        {
            get { return centerAdorner; }
        }

        private LineCtrlAdorner centerCPCtrl;
        /// <summary>
        /// [只读]中控制点（装饰器）。——CP指ControlPoint。
        /// </summary>
        public LineCtrlAdorner CenterCPCtrl
        {
            get { return centerCPCtrl; }
        }

        private Point centerCPPoint = new Point();
        [Tools.LunarProperty("CenterCPPoint", PropertyDateType.Point)]
        public Point CenterCPPoint
        {
            get { return centerCPPoint; }
            set
            {
                centerCPPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.CenterCPPointTag, value.ToString());
                }

                RefreshLocation();
            }
        }

        /// <summary>
        /// 本类通用上下文菜单。
        /// </summary>
        private static ContextMenu contextMenu;

        private ControlDraggingType draggingType = ControlDraggingType.None;

        private LineCtrlAdorner endCtrl;
        /// <summary>
        /// [只读]尾控制点（装饰器）。
        /// </summary>
        public LineCtrlAdorner EndCtrl
        {
            get { return endCtrl; }
        }

        private Point endPoint = new Point();
        /// <summary>
        /// [读写]线的终点坐标。
        /// </summary>
        [Tools.LunarProperty("EndPoint", PropertyDateType.Point)]
        public Point EndPoint
        {
            get { return endPoint; }
            set
            {
                endPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.EndPointTag, value.ToString());
                }

                RefreshLocation();
            }
        }

        /// <summary>
        /// [只读]表示当前部件是否被某个连接线挂接着。
        /// </summary>
        public bool IsLinked
        {
            get
            {
                if (this.masterEditor == null) return false;

                foreach (UIElement ue in this.masterEditor.Children)
                {
                    ILinkableLine linkedLine = ue as ILinkableLine;
                    if (linkedLine == null) continue;

                    if (linkedLine.StartMasterId == this.id || linkedLine.EndMasterId == this.id) return true;
                }

                return false;
            }
        }

        public override bool IsSelected
        {
            get { return base.IsSelected; }
            set
            {
                base.IsSelected = value;

                //显示控制点。
                if (isSelected)
                {
                    startCtrl.Visibility = centerCPCtrl.Visibility =
                        endCtrl.Visibility = Visibility.Visible;
                }
                else
                {
                    startCtrl.Visibility = centerCPCtrl.Visibility =
                        endCtrl.Visibility = Visibility.Hidden;
                }
            }
        }

        //private Polygon mainPolygon = new Polygon() { Cursor = Cursors.Hand };

        //public Polygon MainPolygon
        //{
        //    get { return mainPolygon; }
        //}
        #region 画三角形、饼形、环形等

        private Path mainPath = new Path() { Cursor = Cursors.Hand, StrokeEndLineCap = PenLineCap.Flat, };

        private PathGeometry pathGeometry = new PathGeometry();

        private PathFigure pathFigure = new PathFigure();

        private LineSegment startLineSegment = new LineSegment();

        private LineSegment endLineSegment = new LineSegment();

        /// <summary>
        /// 三角形SE边由弧来实现。
        /// </summary>
        private ArcSegment arcSegmentAtOuter = new ArcSegment();

        private ArcSegment arcSegmentAtInner = new ArcSegment();

        //2019年11月6日。专门用于新增的“花括弧模式”——此花括弧与 Bracket 部件不同——可以支持斜向。
        private BezierSegment startBezierSegment = new BezierSegment();

        private BezierSegment endBezierSegment = new BezierSegment();

        //2019年11月7日。新增T型线。
        private PolyLineSegment polyLineSegment = new PolyLineSegment();

        #endregion

        private Rect movingRect;
        /// <summary>
        /// 部件正在被拖动时的外边框。
        /// </summary>
        public Rect MovingRect
        {
            get
            {
                return movingRect;
            }
        }

        private LineCtrlAdorner startCtrl;

        public LineCtrlAdorner StartCtrl
        {
            get { return startCtrl; }
        }

        private Point startPoint = new Point(0, 0);
        /// <summary>
        /// [读写]线的起点坐标。
        /// </summary>
        [Tools.LunarProperty("StartPoint", PropertyDateType.Point)]
        public Point StartPoint
        {
            get { return startPoint; }
            set
            {
                startPoint = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.StartPointTag, startPoint.ToString());
                }

                RefreshLocation();
            }
        }

        private double textRotateAngle = 0;
        /// <summary>
        /// [读写]文本旋转角度。取值范围：[-180,180]。
        /// </summary>
        [Tools.LunarProperty("TextRotateAngle", PropertyDateType.Double)]
        public double TextRotateAngle
        {
            get { return textRotateAngle; }
            set
            {
                if (value > 180)
                {
                    textRotateAngle = 180;
                }
                else if (value < -180)
                {
                    textRotateAngle = -180;
                }
                else
                {
                    textRotateAngle = value;
                }

                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.TextRotateAngleTag, textRotateAngle.ToString());
                }

                this.RefreshTextRotateAngle();
            }
        }

        private Enums.TriangleForm triangleForm = TriangleForm.Triangle;
        /// <summary>
        /// [读写]三角形形态。
        /// </summary>
        [Tools.LunarProperty("TriangleForm", PropertyDateType.TriangleForm)]
        public Enums.TriangleForm TriangleForm
        {
            get { return triangleForm; }
            set
            {
                triangleForm = value;
                if (this.xmlData != null)
                {
                    this.xmlData.SetAttribute(XmlTags.TriangleFormTag, triangleForm.ToString());
                }
                DrawLine();
                RefreshTextPanelLocatin();
            }
        }

        public override Point TopLeft
        {
            get
            {
                double minLeft, minTop, maxRight, maxBottom;

                minLeft = Math.Min(Math.Min(startPoint.X, endPoint.X), centerCPPoint.X);
                maxRight = Math.Max(Math.Max(startPoint.X, endPoint.X), centerCPPoint.X);

                minTop = Math.Min(Math.Min(startPoint.Y, endPoint.Y), centerCPPoint.Y);
                maxBottom = Math.Max(Math.Max(startPoint.Y, endPoint.Y), centerCPPoint.Y);

                return new Point(minLeft, minTop);
            }
        }

        #endregion


        #region 方法=========================================================================================================

        public override void Build()
        {
            base.Build();

            if (this.xmlData == null) return;

            XmlAttribute attrStartPoint = this.xmlData.GetAttribute(XmlTags.StartPointTag);
            if (attrStartPoint != null)
            {
                this.startPoint = Point.Parse(attrStartPoint.Value);
            }

            XmlAttribute attrCenterCPPoint = this.xmlData.GetAttribute(XmlTags.CenterCPPointTag);
            if (attrCenterCPPoint != null)
            {
                this.centerCPPoint = Point.Parse(attrCenterCPPoint.Value);
            }

            XmlAttribute attrEndPoint = this.xmlData.GetAttribute(XmlTags.EndPointTag);
            if (attrEndPoint != null)
            {
                this.endPoint = Point.Parse(attrEndPoint.Value);
            }

            XmlAttribute attrTextRotateAngle = this.xmlData.GetAttribute(XmlTags.TextRotateAngleTag);
            if (attrTextRotateAngle != null)
            {
                this.textRotateAngle = double.Parse(attrTextRotateAngle.Value);
            }

            XmlAttribute attrTriangleForm = this.xmlData.GetAttribute(XmlTags.TriangleFormTag);
            if (attrTriangleForm != null)
            {
                this.triangleForm = (Enums.TriangleForm)Enum.Parse(typeof(Enums.TriangleForm), attrTriangleForm.Value);
            }

            this.RefreshTextRotateAngle();
            this.RefreshLocation();

            //此类是下面这几个属性的“最终实现类”。这些属性的值都已在基类确定。因此调用,
            this.RefreshWidgetBackColor();
            this.RefreshWidgetLineColor();
            this.RefreshWidgetLineWidth();
            this.RefreshLineDash();
        }

        protected override void BuildStyleProperties()
        {
            base.BuildStyleProperties();

            //没有必须在此类读取的、与Style相关的Xml特性
        }

        public override void BuildWidgetStylePropertiesAndRefresh()
        {
            base.BuildWidgetStylePropertiesAndRefresh();

            //此类是下面这几个属性的“最终实现类”。这些属性的值都已在基类确定。因此调用,
            this.RefreshWidgetBackColor();
            this.RefreshWidgetLineColor();
            this.RefreshWidgetLineWidth();
            this.RefreshLineDash();


            //无意义
            //this.RefreshArrows();
            //this.RefreshWidgetPadding();

            //这几个在Widget类中已经调用。
            //this.RefreshWidgetForeColor();
            //this.RefreshIsShadowVisible();
            //this.RefreshWidgetOpacity();
        }

        /// <summary>
        /// 准备拖出“关系（由两条直线、两个新文本框组成，或由一个文本框、一条直线组成）。
        /// </summary>
        void centerAdorner_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (masterEditor == null) return;
            if (e.ClickCount == 2)
            {
                //防止误拖动部件。
                if (masterEditor != null && masterEditor.MasterManager != null)
                {
                    masterEditor.MouseInfo.DraggingType = masterEditor.MasterManager.MouseDraggingType;
                }

                this.SelectOnlySelf();
                if (Globals.MainWindow.IsPresentatingByPath == false)
                {
                    this.Edit();
                }
                return;
            }

            e.Handled = true;

            if (Globals.MainWindow != null && Globals.MainWindow.EditorManager != null)
            {
                switch (Globals.MainWindow.EditorManager.MouseDraggingType)
                {
                    case PageDraggingType.InsertBezierLine:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertBezierLineRelation;
                            break;
                        }
                    case PageDraggingType.InsertPolyLine:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertPolyLineRelation;
                            break;
                        }
                    default:
                        {
                            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertStraitLineRelation;
                            break;
                        }
                }
            }
            else
            {
                masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.InsertStraitLineRelation;
            }

            //Point pt = e.GetPosition(this.masterEditor);//这个不好，会偏
            //pt = new Point(pt.X - this.masterEditor.BorderThickness.Left,
            //    pt.Y - this.masterEditor.BorderThickness.Top);

            Point pt = this.Center;//直接用中心点更好。
            masterEditor.MouseInfo.LeftButtonPreviewPoint = pt;
            masterEditor.MouseInfo.MainSelectedWidget = this;
        }

        void centerCPCtrl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = centerCPPoint;
            draggingType = ControlDraggingType.CenterCP;
            masterEditor.MouseInfo.DraggingType = PageDraggingType.MoveLineWidgetControler;

            centerCPCtrl.Visibility = System.Windows.Visibility.Hidden;
        }

        /// <summary>
        /// 关联事件。右击中心装饰器时，活动此部件。
        /// </summary>
        void centerAdorner_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.mainPath_PreviewMouseRightButtonUp(sender, e);
        }

        public void DrawLine()
        {
            if (masterEditor == null) return;

            DrawLine(this.startPoint, this.centerCPPoint, this.endPoint);
        }

        /// <summary>
        /// 三角形由三个顶点决定。
        /// </summary>
        /// <param name="startPoint">首端点。</param>
        /// <param name="centerCPPoint">中控制点。</param>
        /// <param name="endPoint">尾端点。</param>
        private void DrawLine(Point startPoint, Point centerCPPoint, Point endPoint)
        {
            Point startBasePt = new Point();
            Point endBasePt = new Point();

            startBasePt.X = Math.Min(Math.Min(startPoint.X, centerCPPoint.X), endPoint.X);
            startBasePt.Y = Math.Min(Math.Min(startPoint.Y, centerCPPoint.Y), endPoint.Y);

            endBasePt.X = Math.Max(Math.Max(startPoint.X, centerCPPoint.X), endPoint.X);
            endBasePt.Y = Math.Max(Math.Max(startPoint.Y, centerCPPoint.Y), endPoint.Y);

            //mainPolygon.Points = new PointCollection()
            //{
            //    startPoint,centerCPPoint,endPoint,startPoint,
            //};

            this.pathFigure.Segments.Clear();
            this.pathFigure.IsClosed = true;
            switch (this.triangleForm)
            {
                case Enums.TriangleForm.Triangle:
                    {
                        #region 三角形绘制代码
                        this.pathFigure.Segments.Add(this.arcSegmentAtInner);
                        this.pathFigure.Segments.Add(this.startLineSegment);
                        this.pathFigure.Segments.Add(this.arcSegmentAtOuter);
                        this.pathFigure.Segments.Add(this.endLineSegment);
                        Size zeroSize = new Size(0, 0);

                        this.pathFigure.StartPoint = centerCPPoint;
                        this.arcSegmentAtInner.Point = centerCPPoint;//此时不显示内弧。
                        this.startLineSegment.Point = startPoint;
                        this.arcSegmentAtOuter.Point = endPoint;
                        this.arcSegmentAtOuter.Size = zeroSize;
                        this.endLineSegment.Point = centerCPPoint;
                        #endregion

                        LocateTextPanel(startPoint, centerCPPoint, endPoint);
                        break;
                    }
                case Enums.TriangleForm.Pie:
                    {
                        Point? c = null;
                        #region 饼形绘制代码
                        this.pathFigure.Segments.Add(this.arcSegmentAtInner);
                        this.pathFigure.Segments.Add(this.startLineSegment);
                        this.pathFigure.Segments.Add(this.arcSegmentAtOuter);
                        this.pathFigure.Segments.Add(this.endLineSegment);
                        //饼形，总是取“StartPoint到CenterPoint”与“EndPoint到CenterPoint”这两个线段间长度较大的那个为半径。
                        //以CenterPoint为顶点。总是顺时针方向绘制。

                        double rSToC, rEToC, r;
                        double sXOffsetToC = startPoint.X - centerCPPoint.X;
                        double sYOffsetToC = startPoint.Y - centerCPPoint.Y;
                        rSToC = Math.Sqrt(sXOffsetToC * sXOffsetToC + sYOffsetToC * sYOffsetToC);
                        double eXOffsetToC = endPoint.X - centerCPPoint.X;
                        double eYOffsetToC = endPoint.Y - centerCPPoint.Y;
                        rEToC = Math.Sqrt(eXOffsetToC * eXOffsetToC + eYOffsetToC * eYOffsetToC);

                        Point realStartPt; Point realEndPt;
                        if (rSToC > 0 && rEToC > 0)
                        {
                            this.mainPath.Visibility = System.Windows.Visibility.Visible;
                            if (rSToC > rEToC)
                            {
                                r = rSToC;
                                realStartPt = startPoint;

                                double k = rSToC / rEToC;
                                double x = centerCPPoint.X + (endPoint.X - centerCPPoint.X) * k;
                                double y = centerCPPoint.Y + (endPoint.Y - centerCPPoint.Y) * k;
                                realEndPt = new Point(x, y);

                                c = new Point(realStartPt.X + (realEndPt.X - realStartPt.X) / 2,
                                       realStartPt.Y + (realEndPt.Y - realStartPt.Y) / 2);
                            }
                            else
                            {
                                r = rEToC;
                                realEndPt = endPoint;

                                double k = rEToC / rSToC;
                                double x = centerCPPoint.X + (startPoint.X - centerCPPoint.X) * k;
                                double y = centerCPPoint.Y + (startPoint.Y - centerCPPoint.Y) * k;
                                realStartPt = new Point(x, y);

                                c = new Point(realStartPt.X + (realEndPt.X - realStartPt.X) / 2,
                                       realStartPt.Y + (realEndPt.Y - realStartPt.Y) / 2);
                            }
                            Size zeroSize = new Size(0, 0);
                            this.pathFigure.StartPoint = centerCPPoint;
                            this.arcSegmentAtInner.Size = zeroSize;
                            this.arcSegmentAtInner.Point = centerCPPoint;
                            this.startLineSegment.Point = realStartPt;
                            this.arcSegmentAtOuter.Size = new Size(r, r);
                            this.arcSegmentAtOuter.SweepDirection = SweepDirection.Clockwise;
                            this.arcSegmentAtOuter.IsLargeArc = LargeArc(startPoint, centerCPPoint, endPoint);
                            this.arcSegmentAtOuter.Point = realEndPt;
                            this.endLineSegment.Point = centerCPPoint;
                        }
                        else
                        {
                            this.mainPath.Visibility = System.Windows.Visibility.Hidden;
                        }

                        #endregion

                        if (c != null && c.HasValue)
                        {
                            Canvas.SetLeft(this.mainTextPanel, c.Value.X - this.mainTextPanel.ActualWidth / 2);
                            Canvas.SetTop(this.mainTextPanel, c.Value.Y - this.mainTextPanel.ActualHeight / 2);
                        }

                        break;
                    }
                case Enums.TriangleForm.Sector:
                    {
                        Point? c = null;

                        #region 环形绘制代码
                        this.pathFigure.Segments.Add(this.arcSegmentAtInner);
                        this.pathFigure.Segments.Add(this.startLineSegment);
                        this.pathFigure.Segments.Add(this.arcSegmentAtOuter);
                        this.pathFigure.Segments.Add(this.endLineSegment);
                        //环形，总是取“StartPoint到CenterPoint”与“EndPoint到CenterPoint”这两个线段间长度间距为环径。
                        //以CenterPoint为顶点。总是顺时针方向绘制。

                        double rSToC, rEToC, outerR, innerR;

                        double sXOffsetToC = startPoint.X - centerCPPoint.X;
                        double sYOffsetToC = startPoint.Y - centerCPPoint.Y;
                        rSToC = Math.Sqrt(sXOffsetToC * sXOffsetToC + sYOffsetToC * sYOffsetToC);

                        double eXOffsetToC = endPoint.X - centerCPPoint.X;
                        double eYOffsetToC = endPoint.Y - centerCPPoint.Y;
                        rEToC = Math.Sqrt(eXOffsetToC * eXOffsetToC + eYOffsetToC * eYOffsetToC);

                        Point outerStartPt, outerEndPt, innerStartPt, innerEndPt;
                        if (rSToC > 0 && rEToC > 0)
                        {
                            this.mainPath.Visibility = System.Windows.Visibility.Visible;
                            if (rSToC > rEToC)
                            {
                                outerR = rSToC;
                                innerR = rEToC;
                                outerStartPt = startPoint;

                                double ke = rSToC / rEToC;
                                double xe = centerCPPoint.X + (endPoint.X - centerCPPoint.X) * ke;
                                double ye = centerCPPoint.Y + (endPoint.Y - centerCPPoint.Y) * ke;
                                outerEndPt = new Point(xe, ye);

                                double ks = rEToC / rSToC;
                                double xs = centerCPPoint.X + (startPoint.X - centerCPPoint.X) * ks;
                                double ys = centerCPPoint.Y + (startPoint.Y - centerCPPoint.Y) * ks;
                                innerStartPt = new Point(xs, ys);

                                innerEndPt = endPoint;

                                c = new Point(innerStartPt.X + (innerEndPt.X - innerStartPt.X) / 2,
                                    innerStartPt.Y + (innerEndPt.Y - innerStartPt.Y) / 2);
                            }
                            else
                            {
                                outerR = rEToC;
                                innerR = rSToC;
                                outerEndPt = endPoint;

                                double ks = rEToC / rSToC;
                                double xs = centerCPPoint.X + (startPoint.X - centerCPPoint.X) * ks;
                                double ys = centerCPPoint.Y + (startPoint.Y - centerCPPoint.Y) * ks;
                                outerStartPt = new Point(xs, ys);

                                double ke = rSToC / rEToC;
                                double xe = centerCPPoint.X + (endPoint.X - centerCPPoint.X) * ke;
                                double ye = centerCPPoint.Y + (endPoint.Y - centerCPPoint.Y) * ke;
                                innerEndPt = new Point(xe, ye);

                                innerStartPt = startPoint;

                                c = new Point(innerStartPt.X + (innerEndPt.X - innerStartPt.X) / 2,
                                    innerStartPt.Y + (innerEndPt.Y - innerStartPt.Y) / 2);
                            }

                            bool isLarge = LargeArc(outerStartPt, centerCPPoint, outerEndPt);

                            this.pathFigure.StartPoint = innerEndPt;

                            this.arcSegmentAtInner.Size = new Size(innerR, innerR);
                            this.arcSegmentAtInner.SweepDirection = SweepDirection.Counterclockwise;
                            this.arcSegmentAtInner.IsLargeArc = isLarge;
                            this.arcSegmentAtInner.Point = innerStartPt;

                            this.startLineSegment.Point = outerStartPt;

                            this.arcSegmentAtOuter.Size = new Size(outerR, outerR);
                            this.arcSegmentAtOuter.SweepDirection = SweepDirection.Clockwise;
                            this.arcSegmentAtOuter.IsLargeArc = isLarge;
                            this.arcSegmentAtOuter.Point = outerEndPt;

                            this.endLineSegment.Point = innerEndPt;
                        }
                        else
                        {
                            this.mainPath.Visibility = System.Windows.Visibility.Hidden;
                        }

                        #endregion

                        if (c != null && c.HasValue)
                        {
                            Canvas.SetLeft(this.mainTextPanel, c.Value.X - this.mainTextPanel.ActualWidth / 2);
                            Canvas.SetTop(this.mainTextPanel, c.Value.Y - this.mainTextPanel.ActualHeight / 2);
                        }

                        break;
                    }
                case TriangleForm.Bracket:
                    {
                        #region 绘制可斜向的花括号

                        if (GetBezierControlPoints(startPoint, centerCPPoint, endPoint, out Point? point1, out Point? point2, out Point? point3))
                        {
                            this.pathFigure.Segments.Add(startBezierSegment);
                            this.pathFigure.Segments.Add(endBezierSegment);

                            this.pathFigure.StartPoint = startPoint;
                            startBezierSegment.Point1 = point1.Value;
                            startBezierSegment.Point2 = point2.Value;
                            startBezierSegment.Point3 = centerCPPoint;

                            endBezierSegment.Point1 = point2.Value;
                            endBezierSegment.Point2 = point3.Value;
                            endBezierSegment.Point3 = endPoint;

                            this.pathFigure.IsClosed = false;
                        }
                        #endregion
                        break;
                    }
                case TriangleForm.TLine:
                    {
                        //绘制首尾点连线和中控点向首尾点连线的垂线。
                        if (GetTPoint(startPoint, centerCPPoint, endPoint, out Point? tPoint))
                        {
                            polyLineSegment.Points.Clear();
                            this.pathFigure.Segments.Add(polyLineSegment);

                            this.pathFigure.StartPoint = startPoint;
                            this.polyLineSegment.Points.Add(tPoint.Value);
                            this.polyLineSegment.Points.Add(centerCPPoint);
                            this.polyLineSegment.Points.Add(tPoint.Value);
                            this.polyLineSegment.Points.Add(endPoint);

                            this.pathFigure.IsClosed = false;
                        }
                        break;
                    }
                case TriangleForm.OrgLine:
                    {
                        //绘制首尾点连线和中控点向首尾点连线的垂线。
                        if (GetOrgPoints(startPoint, centerCPPoint, endPoint, out Point? point1, out Point? point2, out Point? point3))
                        {
                            polyLineSegment.Points.Clear();
                            this.pathFigure.Segments.Add(polyLineSegment);

                            this.pathFigure.StartPoint = startPoint;
                            this.polyLineSegment.Points.Add(point1.Value);
                            this.polyLineSegment.Points.Add(point2.Value);
                            this.polyLineSegment.Points.Add(centerCPPoint);
                            this.polyLineSegment.Points.Add(point2.Value);
                            this.polyLineSegment.Points.Add(point3.Value);
                            this.polyLineSegment.Points.Add(endPoint);

                            this.pathFigure.IsClosed = false;
                        }
                        break;
                    }
                case TriangleForm.YLine:
                    {
                        //绘制首尾点连线和中控点向首尾点连线的垂线。
                        if (GetYPoint(startPoint, centerCPPoint, endPoint, out Point? yPoint))
                        {
                            polyLineSegment.Points.Clear();
                            this.pathFigure.Segments.Add(polyLineSegment);

                            this.pathFigure.StartPoint = startPoint;
                            this.polyLineSegment.Points.Add(yPoint.Value);
                            this.polyLineSegment.Points.Add(centerCPPoint);
                            this.polyLineSegment.Points.Add(yPoint.Value);
                            this.polyLineSegment.Points.Add(endPoint);

                            this.pathFigure.IsClosed = false;
                        }
                        break;
                    }
            }

            //移动部件控制点时，要刷新连接线的。
            this.movingRect = new Rect(startBasePt, endBasePt);
        }

        private bool GetOrgPoints(Point startPoint, Point centerCPPoint, Point endPoint, out Point? point1, out Point? point2, out Point? point3)
        {
            var x1 = startPoint.X;
            var y1 = startPoint.Y;
            var x2 = endPoint.X;
            var y2 = endPoint.Y;
            if (x2 != x1)
            {
                var k = (y2 - y1) / (x2 - x1);
                var x = (k * k * x1 + k * (centerCPPoint.Y - y1) + centerCPPoint.X) / (k * k + 1);
                var y = k * (x - x1) + y1;

                var h = (centerCPPoint.Y - y) / 2;
                var w = (centerCPPoint.X - x) / 2;

                point1 = new Point(startPoint.X + w, startPoint.Y + h);
                point2 = new Point(x + w, y + h);
                point3 = new Point(endPoint.X + w, endPoint.Y + h);
                return true;
            }
            else
            {
                point1 = new Point(startPoint.X + (centerCPPoint.X - startPoint.X) / 2, startPoint.Y);
                point2 = new Point(startPoint.X + (centerCPPoint.X - startPoint.X) / 2, centerCPPoint.Y);
                point3 = new Point(endPoint.X + (centerCPPoint.X - endPoint.X) / 2, endPoint.Y);
                return true;
            }
        }

        /// <summary>
        /// 返回垂足与中控点的中点。如果找不到，就返回中点。
        /// </summary>
        private bool GetYPoint(Point startPoint, Point centerCPPoint, Point endPoint, out Point? yPoint)
        {
            var x1 = startPoint.X;
            var y1 = startPoint.Y;
            var x2 = endPoint.X;
            var y2 = endPoint.Y;
            if (x2 != x1)
            {
                var k = (y2 - y1) / (x2 - x1);
                var x = (k * k * x1 + k * (centerCPPoint.Y - y1) + centerCPPoint.X) / (k * k + 1);
                var y = k * (x - x1) + y1;
                yPoint = new Point(x + (centerCPPoint.X - x) / 2, y + (centerCPPoint.Y - y) / 2);
                return true;
            }
            else
            {
                yPoint = new Point(startPoint.X + (endPoint.X - startPoint.X) / 2, startPoint.Y + (endPoint.Y - startPoint.Y) / 2);
                return true;
            }
        }

        /// <summary>
        /// 返回垂足。如果找不到，就返回中点。
        /// </summary>
        private bool GetTPoint(Point startPoint, Point centerCPPoint, Point endPoint, out Point? tPoint)
        {
            var x1 = startPoint.X;
            var y1 = startPoint.Y;
            var x2 = endPoint.X;
            var y2 = endPoint.Y;
            if (x2 != x1)
            {
                var k = (y2 - y1) / (x2 - x1);
                var x = (k * k * x1 + k * (centerCPPoint.Y - y1) + centerCPPoint.X) / (k * k + 1);
                var y = k * (x - x1) + y1;
                tPoint = new Point(x, y);
                return true;
            }
            else
            {
                tPoint = new Point(startPoint.X + (endPoint.X - startPoint.X) / 2, startPoint.Y + (endPoint.Y - startPoint.Y) / 2);
                return true;
            }
        }

        private bool GetBezierControlPoints(Point startPoint, Point centerCPPoint, Point endPoint, out Point? point1, out Point? point2, out Point? point3)
        {
            // 这三个点是指由 起点、终点、中控点构成的矩形的两个中点及矩形中心点。
            // 这两个边线中点是指除 SE 和 经过 C 点的两边之外的另两边的中点。
            // 先计算矩形。
            // 基本思路是先算出 C 到 SE 的垂足坐标，然后推导出 S1 和 E1。

            // 先计算出 Ax+By+C=0 这个方程，即先求出 ABC 

            var x1 = startPoint.X;
            var y1 = startPoint.Y;
            var x2 = endPoint.X;
            var y2 = endPoint.Y;
            if (x2 != x1)
            {
                var k = (y2 - y1) / (x2 - x1);
                var x = (k * k * x1 + k * (centerCPPoint.Y - y1) + centerCPPoint.X) / (k * k + 1);
                var y = k * (x - x1) + y1;

                var h = (centerCPPoint.Y - y);
                var w = (centerCPPoint.X - x);

                point1 = new Point(startPoint.X + w, startPoint.Y + h);
                point2 = new Point(x, y);
                point3 = new Point(endPoint.X + w, endPoint.Y + h);
                return true;
            }
            else
            {
                point1 = new Point(startPoint.X + (centerCPPoint.X - startPoint.X) / 2, startPoint.Y);
                point2 = new Point(startPoint.X + (centerCPPoint.X - startPoint.X) / 2, centerCPPoint.Y);
                point3 = new Point(endPoint.X + (centerCPPoint.X - endPoint.X) / 2, endPoint.Y);
                return true;
            }
        }

        private bool LargeArc(Point startPoint, Point centerCPPoint, Point endPoint)
        {
            if (startPoint.X == centerCPPoint.X)
            {
                if (startPoint.Y >= centerCPPoint.Y)
                {
                    if (endPoint.X >= startPoint.X) return true;
                    else return false;
                }
                else
                {
                    if (endPoint.X >= startPoint.X) return false;
                    else return true;
                }
            }
            else if (startPoint.Y == centerCPPoint.Y)
            {
                if (startPoint.X >= centerCPPoint.X)
                {
                    if (endPoint.Y > startPoint.Y) return false;
                    else return true;
                }
                else
                {
                    if (endPoint.Y > startPoint.Y) return true;
                    else return false;
                }
            }

            //分象限
            //一、四象限，E点在SC线上侧需要大弧，而下侧需要小弧
            //二、三象限，E点在SC线上侧需要小弧，而下侧需要大弧
            if (startPoint.X < centerCPPoint.X)//二、三象限
            {
                double k = (startPoint.Y - centerCPPoint.Y) / (startPoint.X - centerCPPoint.X);

                double y = endPoint.X * k + centerCPPoint.Y - k * centerCPPoint.X;
                if (endPoint.Y > y) return true;
                else return false;
            }
            else//一、四象限
            {
                double k = (startPoint.Y - centerCPPoint.Y) / (startPoint.X - centerCPPoint.X);

                double y = endPoint.X * k + centerCPPoint.Y - k * centerCPPoint.X;
                if (endPoint.Y > y) return false;
                else return true;
            }
        }

        /// <summary>
        /// 重定文本位置。
        /// </summary>
        /// <param name="startPoint">首端点。</param>
        /// <param name="centerCPPoint">中控制点。</param>
        /// <param name="endPoint">尾端点。</param>
        private void LocateTextPanel(Point startPoint, Point centerCPPoint, Point endPoint)
        {
            switch (this.triangleForm)
            {
                //case Enums.TriangleForm.Sector:
                //case Enums.TriangleForm.Pie:
                //这俩必须取消。因为扇形和饼形需要其它点的坐标，因此在DrawLine()方法中实现了。
                case Enums.TriangleForm.Triangle:
                    {
                        Point textStart;

                        double left, right, bottom, top;
                        left = Math.Min(Math.Min(startPoint.X, centerCPPoint.X), endPoint.X);
                        right = Math.Max(Math.Max(startPoint.X, centerCPPoint.X), endPoint.X);
                        top = Math.Min(Math.Min(startPoint.Y, centerCPPoint.Y), endPoint.Y);
                        bottom = Math.Max(Math.Max(startPoint.Y, centerCPPoint.Y), endPoint.Y);

                        textStart = new Point(left + (right - left) / 2 - (this.mainTextPanel.ActualWidth / 2) - this.mainTextPanel.Margin.Left,
                            top + (bottom - top) / 2 - (this.mainTextPanel.ActualHeight / 2) - this.mainTextPanel.Margin.Top);

                        Canvas.SetLeft(this.mainTextPanel, textStart.X);
                        Canvas.SetTop(this.mainTextPanel, textStart.Y);
                        break;
                    }
            }
        }

        public override void DropWidget(ModifingItem<Action, ModifingInfo> mi, Point mousePoint)
        {
            if (mi == null) return;

            Point globalLocation = masterEditor.MouseInfo.LeftButtonPreviewPoint;

            Point newStartPoint = new Point(
                startPoint.X - globalLocation.X + mousePoint.X, startPoint.Y - globalLocation.Y + mousePoint.Y);
            Point newCenterCPPoint = new Point(
                centerCPPoint.X - globalLocation.X + mousePoint.X, centerCPPoint.Y - globalLocation.Y + mousePoint.Y);
            Point newEndPoint = new Point(
                endPoint.X - globalLocation.X + mousePoint.X, endPoint.Y - globalLocation.Y + mousePoint.Y);

            #region 自动中心吸附
            bool autoAlignment = Globals.MainWindow.RtbtnAutoAlignment.IsChecked == true;

            if (autoAlignment)
            {
                double hOffset = double.MaxValue, vOffset = double.MaxValue;
                if (autoAlignment)
                {
                    //尝试自动对齐

                    //先尝试中心坐标对齐
                    //需要取出最接近的位置
                    Point center = new Point(newStartPoint.X + this.OuterRect.Width / 2, newStartPoint.Y + this.OuterRect.Height / 2);

                    foreach (UIElement ue in this.MasterEditor.MainCanvas.Children)
                    {
                        Widget widget = ue as ContentWidget;
                        if (widget == null)
                        {
                            widget = ue as ShapeWidget;
                            if (widget == null) continue;
                        }

                        if (widget == this) continue;
                        if (widget.MasterEditor.GetSelectedWidgetsList().Contains(widget)) continue;

                        Point widgetCenter = new Point(widget.OuterRect.Left + widget.OuterRect.Width / 2, widget.OuterRect.Top + widget.OuterRect.Height / 2);
                        double h = double.MaxValue, v = double.MaxValue, tmph = double.MaxValue, tmpv = double.MaxValue;

                        tmph = widgetCenter.X - center.X;
                        tmpv = widgetCenter.Y - center.Y;

                        if (Math.Abs(tmph) < Math.Abs(h)) h = tmph;
                        if (Math.Abs(tmpv) < Math.Abs(v)) v = tmpv;

                        if (Math.Abs(h) < Math.Abs(hOffset)) hOffset = h;
                        if (Math.Abs(v) < Math.Abs(vOffset)) vOffset = v;
                    }
                }

                //自动吸附对齐会造成很迷惑人的效果。取消之。2014年7月26日
                //if (hOffset < double.MaxValue && Math.Abs(hOffset) < 10)
                //{
                //    newStartPoint.X += hOffset;
                //    newCenterCPPoint.X += hOffset;
                //    newEndPoint.X += hOffset;
                //}

                //if (vOffset < double.MaxValue && Math.Abs(vOffset) < 10)
                //{
                //    newStartPoint.Y += vOffset;
                //    newCenterCPPoint.Y += vOffset;
                //    newEndPoint.Y += vOffset;
                //}
            }
            #endregion

            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag, startPoint.ToString(),
                newStartPoint.ToString());
            StartPoint = newStartPoint;

            Action actCenterCPPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag, centerCPPoint.ToString(),
                newCenterCPPoint.ToString());
            CenterCPPoint = newCenterCPPoint;

            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag, endPoint.ToString(),
                newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.ModifingInfo.NewMainSelectedWidgetID = this.id;
            mi.AddAction(actStartPoint);
            mi.AddAction(actCenterCPPoint);
            mi.AddAction(actEndPoint);
        }

        public override void DropWidgetControler(Point newMovingPoint, ModifingItem<Action, ModifingInfo> mi)
        {
            if (mi == null || mi.ModifingInfo == null) return;
            if (masterEditor == null || masterEditor.MasterManager == null) return;

            ModifingInfo info = mi.ModifingInfo;
            switch (draggingType)
            {
                case ControlDraggingType.Start:
                    {
                        info.ModifingDescription = "拖动三角形首端点";

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.StartPointTag, startPoint.ToString(), newMovingPoint.ToString());
                        StartPoint = newMovingPoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
                case ControlDraggingType.CenterCP:
                    {
                        info.ModifingDescription = "拖动三角形控制点";

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.CenterCPPointTag, centerCPPoint.ToString(), newMovingPoint.ToString());

                        CenterCPPoint = newMovingPoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
                case ControlDraggingType.End:
                    {
                        info.ModifingDescription = "拖动三角形尾端点";

                        Action actDraggingLineHeader = new Action(masterEditor.Id, id, this.GetType().Name,
                            XmlTags.EndPointTag, endPoint.ToString(), newMovingPoint.ToString());
                        EndPoint = newMovingPoint;

                        info.NewMainSelectedWidgetID = this.id;
                        mi.AddAction(actDraggingLineHeader);
                        break;
                    }
            }
        }

        public override void CollapseToTop(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.Y > endPoint.Y)
            {
                Point newStartPoint = new Point(startPoint.X, startPoint.Y - units);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.Y < endPoint.Y)
            {
                Point newEndPoint = new Point(endPoint.X, endPoint.Y - units);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果水平对齐，则不更改。
        }

        public override void ExpandToBottom(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.Y > endPoint.Y)
            {
                Point newStartPoint = new Point(startPoint.X, startPoint.Y + units);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.Y < endPoint.Y)
            {
                Point newEndPoint = new Point(endPoint.X, endPoint.Y + units);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果水平对齐，则不更改。
        }

        public override void CollapseToLeft(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.X > endPoint.X)
            {
                Point newStartPoint = new Point(startPoint.X - units, startPoint.Y);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.X < endPoint.X)
            {
                Point newEndPoint = new Point(endPoint.X - units, endPoint.Y);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果垂直对齐，则不更改
        }

        public override void ExpandToRight(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            if (startPoint.X > endPoint.X)
            {
                Point newStartPoint = new Point(startPoint.X + units, startPoint.Y);
                Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                    startPoint.ToString(), newStartPoint.ToString());
                StartPoint = newStartPoint;

                mi.AddAction(actStartPoint);
            }
            else if (startPoint.X < endPoint.X)
            {
                Point newEndPoint = new Point(endPoint.X + units, endPoint.Y);
                Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                    endPoint.ToString(), newEndPoint.ToString());
                EndPoint = newEndPoint;

                mi.AddAction(actEndPoint);
            }
            //如果垂直对齐，则不更改
        }

        private Point FormatStartMovingPoint(Point newInsideMovingPoint, Point endPoint)
        {
            double horizontalDistance = newInsideMovingPoint.X - endPoint.X;
            double verticalDistance = newInsideMovingPoint.Y - endPoint.Y;

            double absHD = Math.Abs(horizontalDistance);
            double absVD = Math.Abs(verticalDistance);

            if (absVD > absHD)
            {
                if (absVD > absHD * 2)
                {
                    newInsideMovingPoint = new Point(endPoint.X, newInsideMovingPoint.Y);
                }
                else
                {
                    int qua = Getquadrant(newInsideMovingPoint, endPoint);
                    switch (qua)
                    {
                        case 1:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                   endPoint.Y - (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                        case 2:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                   endPoint.Y + (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                        case 3:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                   endPoint.Y - (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                        case 4:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                    endPoint.Y + (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                    }
                }
            }
            else
            {
                if (absVD * 2 < absHD)
                {
                    newInsideMovingPoint = new Point(newInsideMovingPoint.X, endPoint.Y);
                }
                else
                {
                    int qua = Getquadrant(newInsideMovingPoint, endPoint);
                    switch (qua)
                    {
                        case 1:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                   endPoint.Y - (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                        case 2:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                   endPoint.Y + (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                        case 3:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                   endPoint.Y - (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                        case 4:
                            {
                                newInsideMovingPoint = new Point(newInsideMovingPoint.X,
                                    endPoint.Y + (newInsideMovingPoint.X - endPoint.X));
                                break;
                            }
                    }
                }
            }

            return newInsideMovingPoint;
        }

        /// <summary>
        /// 取与自身挂接的连接线。
        /// </summary>
        /// <returns>可能返回null。</returns>
        public List<ILinkableLine> GetLinkedLines()
        {
            if (this.masterEditor == null) return null;

            List<Widget> widgets = new List<Widget>();
            widgets.Add(this);

            return this.masterEditor.GetLinkedLines(widgets);
        }

        /// <summary>
        /// 以basePoint为基础坐标，画出四象限。
        /// 取出pt在哪个象限中。
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="basePoint"></param>
        /// <returns></returns>
        private int Getquadrant(Point pt, Point basePoint)
        {
            if (pt.X > basePoint.X)
            {
                if (pt.Y > basePoint.Y)
                {
                    return 4;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                if (pt.Y > basePoint.Y)
                {
                    return 3;
                }
                else
                {
                    return 2;
                }
            }
        }

        public override string GetRelativeOuterXml(Point baseCopyTopLeft)
        {
            if (this.xmlData == null) return string.Empty;

            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X - baseCopyTopLeft.X,
                oldStartPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.StartPointTag, newStartPoint.ToString());

            Point oldCenterCPPoint = centerCPPoint;
            Point newCenterCPPoint = new Point(oldCenterCPPoint.X - baseCopyTopLeft.X,
                oldCenterCPPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.CenterCPPointTag, newCenterCPPoint.ToString());

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X - baseCopyTopLeft.X,
                oldEndPoint.Y - baseCopyTopLeft.Y);
            this.xmlData.SetAttribute(XmlTags.EndPointTag, newEndPoint.ToString());

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(this.xmlData.OuterXml);

            this.xmlData.SetAttribute(XmlTags.StartPointTag, oldStartPoint.ToString());
            this.xmlData.SetAttribute(XmlTags.CenterCPPointTag, oldCenterCPPoint.ToString());
            this.xmlData.SetAttribute(XmlTags.EndPointTag, oldEndPoint.ToString());
            return sb.ToString();
        }

        public event EventHandler<MouseButtonEventArgs> ControlHandlerDoubleClicked;

        protected void OnControlHandlerDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (ControlHandlerDoubleClicked != null)
            {
                ControlHandlerDoubleClicked(sender, e);
            }
        }

        void endCtrl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = endPoint;
            draggingType = ControlDraggingType.End;
            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveLineWidgetControler;

            endCtrl.Visibility = System.Windows.Visibility.Hidden;
        }

        void startCtrl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                OnControlHandlerDoubleClicked(sender, e);
                e.Handled = true;
                return;
            }

            this.SelectOnlySelf();

            //开始拖动。
            e.Handled = true;
            masterEditor.MouseInfo.LeftButtonPreviewPoint = startPoint;
            draggingType = ControlDraggingType.Start;
            masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveLineWidgetControler;

            startCtrl.Visibility = System.Windows.Visibility.Hidden;
        }

        /// <summary>
        /// 这个虚方法是用以查看本部件是否在选定框的内部。
        /// 线型部件，各有各的计算办法。
        /// </summary>
        /// <param name="rect"></param>
        /// <returns></returns>
        public override bool IsInRect(Rect rect)
        {
            //return base.IsInRect(rect);//这个要屏蔽。
            bool isInRect = base.IsInRect(rect);
            if (isInRect)
            {
                return true;
            }
            else
            {
                Rect virtualRect = new Rect(TopLeft, BottomRight);
                return rect.IntersectsWith(virtualRect);
            }
        }

        void mainPath_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //双击编辑文本
            if (e.ClickCount == 2)
            {
                this.SelectOnlySelf();
                if (Globals.MainWindow.IsPresentatingByPath == false)
                {
                    this.Edit();
                }
                return;
            }

            ModifingItem<Action, ModifingInfo> mi = null;
            ModifingInfo info = null;
            if (FormatSelf(ref mi, ref info))
            {
                if (masterEditor != null)
                {
                    masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveWidgets;
                }
                return;//格式刷格式化，不更改选定状态。
            }

            KeyStates ksRightShift = Keyboard.GetKeyStates(Key.RightShift);
            KeyStates ksLeftShift = Keyboard.GetKeyStates(Key.LeftShift);

            bool isShift = false;

            if ((ksRightShift & KeyStates.Down) > 0 || (ksLeftShift & KeyStates.Down) > 0) { isShift = true; }

            bool isCtrl = false;
            KeyStates ksRightCtrl = Keyboard.GetKeyStates(Key.RightCtrl);
            KeyStates ksLeftCtrl = Keyboard.GetKeyStates(Key.LeftCtrl);

            if ((ksRightCtrl & KeyStates.Down) > 0 || (ksLeftCtrl & KeyStates.Down) > 0) isCtrl = true;

            if (isShift && isCtrl == false)
            {
                //基本规则：
                //    ①如果未选定，选定，并作为活动部件。
                //    ②如果已选定，看是否是活动部件，如果已经是活动部件，整个取消选定状态；
                //                                      如果不是活动部件，设为活动部件。
                if (this.IsSelected == false)
                {
                    this.IsMainSelected = true;
                }
                else
                {
                    if (this.IsMainSelected == false)
                    {
                        this.IsMainSelected = true;
                    }
                    else
                    {
                        this.IsSelected = false;
                    }
                }
            }
            else
            {
                if (IsSelected == false)
                {
                    this.SelectOnlySelf();
                }
                //如果是选定的部件，那么可能是在拖动。
            }

            //准备拖动。
            if (masterEditor != null)
            {
                masterEditor.MouseInfo.DraggingType = Enums.PageDraggingType.MoveWidgets;
                Point pt = e.GetPosition(this.masterEditor);
                pt = new Point(pt.X - this.masterEditor.BorderThickness.Left, pt.Y - this.masterEditor.BorderThickness.Top);
                masterEditor.MouseInfo.LeftButtonPreviewPoint = pt;
            }
        }

        void mainPath_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (this.IsSelected == false)
            {
                //如果未选定，就作为活动，
                this.SelectOnlySelf();
            }
            else
            {
                //若已选定，不更改选定范围，只更改“活动部件”为此部件
                if (this.IsMainSelected == false) this.IsMainSelected = true;
            }

            if (this.ContextMenu != null)
            {
                this.ContextMenu.IsOpen = true;
            }
        }

        public override void MoveUp(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - units);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X, centerCPPoint.Y - units);
            Action actStartCPPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - units);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actStartCPPoint);
            mi.AddAction(actEndPoint);
        }

        public override void MoveDown(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y + units);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X, centerCPPoint.Y + units);
            Action actStartCPPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y + units);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actStartCPPoint);
            mi.AddAction(actEndPoint);
        }

        public override void MoveLeft(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X - units, startPoint.Y);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X - units, centerCPPoint.Y);
            Action actStartCPPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X - units, endPoint.Y);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actStartCPPoint);
            mi.AddAction(actEndPoint);
        }

        public override void MoveRight(ModifingItem<Action, ModifingInfo> mi, double units = 1)
        {
            if (mi == null) return;

            Point newStartPoint = new Point(startPoint.X + units, startPoint.Y);
            Action actStartPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X + units, centerCPPoint.Y);
            Action actCenterCPPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X + units, endPoint.Y);
            Action actEndPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            EndPoint = newEndPoint;

            mi.AddAction(actStartPoint);
            mi.AddAction(actCenterCPPoint);
            mi.AddAction(actEndPoint);
        }

        /// <summary>
        /// 改变中心点横坐标。
        /// </summary>
        public override void MoveHorizontalCenterTo(ModifingItem<Action, ModifingInfo> mi, double center)
        {
            if (mi == null || double.IsNaN(center) || masterEditor == null) return;

            double offset = 0;
            double curCenter = TopLeft.X + (BottomRight.X - TopLeft.X) / 2;
            offset = curCenter - center;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X - offset, centerCPPoint.Y);
            Action actCenterCP = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            this.CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actCenterCP);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 改变中心点纵坐标。
        /// </summary>
        public override void MoveVerticalCenterTo(ModifingItem<Action, ModifingInfo> mi, double center)
        {
            if (mi == null || double.IsNaN(center) || masterEditor == null) return;

            double offset = 0;
            double curCenter = TopLeft.Y + (BottomRight.Y - TopLeft.Y) / 2;
            offset = curCenter - center;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X, centerCPPoint.Y - offset);
            Action actCenterCP = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            this.CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actCenterCP);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveDown不同，这里是指定向下移动到哪个位置（下边缘到哪个位置）。
        /// </summary>
        public override void MoveBottomSiderTo(ModifingItem<Action, ModifingInfo> mi, double bottom)
        {
            if (mi == null || double.IsNaN(bottom) || masterEditor == null) return;

            double offset = 0;
            double maxBottom = BottomRight.Y;
            offset = maxBottom - bottom;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X, centerCPPoint.Y - offset);
            Action actCenterCP = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            this.CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actCenterCP);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveLeft不同，这里是指定向左移动到哪个位置（左边缘到哪个位置）。
        /// </summary>
        public override void MoveLeftSiderTo(ModifingItem<Action, ModifingInfo> mi, double left)
        {
            if (mi == null || double.IsNaN(left) || masterEditor == null) return;

            double offset = 0;
            double minLeft = TopLeft.X;
            offset = minLeft - left;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X - offset, centerCPPoint.Y);
            Action actCenterCP = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            this.CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actCenterCP);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveRight不同，这里是指定向右移动到哪个位置（右边缘到哪个位置）。
        /// </summary>
        public override void MoveRightSiderTo(ModifingItem<Action, ModifingInfo> mi, double right)
        {
            if (mi == null || double.IsNaN(right) || masterEditor == null) return;

            double offset = 0;
            double maxRight = BottomRight.X;
            offset = maxRight - right;

            Point newStartPoint = new Point(startPoint.X - offset, startPoint.Y);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X, centerCPPoint.Y - offset);
            Action actCenterCP = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            this.CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X - offset, endPoint.Y);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actCenterCP);
            mi.AddAction(actEnd);
        }

        /// <summary>
        /// 与MoveUp不同，这里是指定向上移动到哪个位置（上边缘到哪个位置）。
        /// </summary>
        public override void MoveTopSiderTo(ModifingItem<Action, ModifingInfo> mi, double top)
        {
            if (mi == null || double.IsNaN(top) || masterEditor == null) return;

            double offset = 0;
            double minTop = TopLeft.Y;
            offset = minTop - top;

            Point newStartPoint = new Point(startPoint.X, startPoint.Y - offset);
            Action actStart = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.StartPointTag,
                startPoint.ToString(), newStartPoint.ToString());
            this.StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X, centerCPPoint.Y - offset);
            Action actCenterCP = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());
            this.CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X, endPoint.Y - offset);
            Action actEnd = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.EndPointTag,
                endPoint.ToString(), newEndPoint.ToString());
            this.EndPoint = newEndPoint;

            mi.AddAction(actStart);
            mi.AddAction(actCenterCP);
            mi.AddAction(actEnd);
        }

        public override void MoveWhenDraggingControler(Point newOutSidePoint)
        {
            switch (draggingType)
            {
                case ControlDraggingType.Start:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(newMovingPoint, this.centerCPPoint, this.endPoint);
                        break;
                    }
                case ControlDraggingType.CenterCP:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(this.startPoint, newMovingPoint, this.endPoint);
                        break;
                    }
                case ControlDraggingType.End:
                    {
                        Point newMovingPoint = newOutSidePoint;
                        DrawLine(this.startPoint, this.centerCPPoint, newMovingPoint);
                        break;
                    }
            }

            this.commentAdorner.Visibility =
                this.hyperLinkAdorner.Visibility =
                this.centerAdorner.Visibility = Visibility.Collapsed;//与椭圆、矩形不同
        }

        public override void MoveWhenDraggingWidget(Point mousePoint)
        {
            if (masterEditor == null) return;

            Point newMovingPoint = mousePoint;
            Point globalLocation = masterEditor.MouseInfo.LeftButtonPreviewPoint;

            Point tmpStartPoint = new Point(startPoint.X - globalLocation.X + newMovingPoint.X,
                startPoint.Y - globalLocation.Y + newMovingPoint.Y);
            Point tmpEndPoint = new Point(endPoint.X - globalLocation.X + newMovingPoint.X,
                endPoint.Y - globalLocation.Y + newMovingPoint.Y);
            Point tmpCenterCPPoint = new Point(centerCPPoint.X - globalLocation.X + newMovingPoint.X,
                centerCPPoint.Y - globalLocation.Y + newMovingPoint.Y);

            startCtrl.CenterPoint = tmpStartPoint;
            centerCPCtrl.CenterPoint = tmpCenterCPPoint;
            endCtrl.CenterPoint = tmpEndPoint;

            DrawLine(tmpStartPoint, tmpCenterCPPoint, tmpEndPoint);

            this.commentAdorner.Visibility =
                this.hyperLinkAdorner.Visibility =
                this.centerAdorner.Visibility = Visibility.Collapsed;//与椭圆、矩形不同
        }

        /// <summary>
        /// 刷新所有控制点。
        /// </summary>
        public override void RefreshControlers()
        {
            if (isSelected)
            {
                //刷新控制点色彩
                if (masterEditor != null && masterEditor.MasterManager != null)
                {
                    if (startCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetStartControlerBrush)
                        startCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetStartControlerBrush;

                    if (centerCPCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetCenterControlerBrush)
                        centerCPCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetCenterControlerBrush;

                    if (endCtrl.MainSelectedBrush != masterEditor.MasterManager.WidgetEndControlerBrush)
                        endCtrl.MainSelectedBrush = masterEditor.MasterManager.WidgetEndControlerBrush;
                }

                startCtrl.CenterPoint = startPoint;
                centerCPCtrl.CenterPoint = centerCPPoint;
                endCtrl.CenterPoint = endPoint;

                startCtrl.Visibility = centerCPCtrl.Visibility =
                    endCtrl.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                startCtrl.Visibility = centerCPCtrl.Visibility =
                    endCtrl.Visibility = System.Windows.Visibility.Hidden;
            }

            this.commentAdorner.InvalidateVisual();
        }

        public override void RefreshLineDash()
        {
            switch (lineDash)
            {
                case LineDashType.DashType.Dash:
                    {
                        //this.mainPolygon.StrokeDashArray = LineDashType.dashCollection; break;
                        this.mainPath.StrokeDashArray = LineDashType.dashCollection; break;
                    }
                case LineDashType.DashType.DashDotDot:
                    {
                        //this.mainPolygon.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                        this.mainPath.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                    }
                case LineDashType.DashType.Dot:
                    {
                        //this.mainPolygon.StrokeDashArray = LineDashType.dotCollection; break;
                        this.mainPath.StrokeDashArray = LineDashType.dotCollection; break;
                    }
                case LineDashType.DashType.Solid:
                    {
                        //this.mainPolygon.StrokeDashArray = LineDashType.solidCollection; break;
                        this.mainPath.StrokeDashArray = LineDashType.solidCollection; break;
                    }
                default:
                    {
                        //this.mainPolygon.StrokeDashArray = LineDashType.dashDotCollection; break;
                        this.mainPath.StrokeDashArray = LineDashType.dashDotCollection; break;
                    }
            }
        }

        public override void RefreshIsSelected()
        {
            base.RefreshIsSelected();
        }

        public override void RefreshWidgetLineWidth()
        {
            //mainPolygon.StrokeThickness = widgetLineWidth;
            mainPath.StrokeThickness = widgetLineWidth;
        }

        public override void RefreshLocation()
        {
            base.RefreshLocation();

            //mainPolygon.Points = new PointCollection(){
            //    startPoint,centerCPPoint,endPoint,startPoint,
            //};
            DrawLine();

            this.movingRect = new Rect(this.TopLeft, this.BottomRight);

            RefreshWidgetLineWidth();
            RefreshControlers();
            RefreshTextRotateAngle();

            this.centerAdorner.Visibility = Visibility.Visible;
            this.centerAdorner.InvalidateVisual();

            this.RefreshCommentText();
            this.RefreshHyperLinkText();
        }

        public override void RefreshPointWhenGroupIn(Point baseTopLeft)
        {
            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X - baseTopLeft.X,
                oldStartPoint.Y - baseTopLeft.Y);
            StartPoint = newStartPoint;

            Point oldCenterCPPoint = centerCPPoint;
            Point newCenterCPPoint = new Point(oldCenterCPPoint.X - baseTopLeft.X,
                oldCenterCPPoint.Y - baseTopLeft.Y);
            CenterCPPoint = newCenterCPPoint;

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X - baseTopLeft.X,
                oldEndPoint.Y - baseTopLeft.Y);
            EndPoint = newEndPoint;
        }

        public override void RefreshPointWhenGroupOut(Point baseTopLeft)
        {
            Point oldStartPoint = startPoint;
            Point newStartPoint = new Point(oldStartPoint.X + baseTopLeft.X,
                oldStartPoint.Y + baseTopLeft.Y);
            StartPoint = newStartPoint;

            Point oldCenterCPPoint = centerCPPoint;
            Point newCenterCPPoint = new Point(oldCenterCPPoint.X + baseTopLeft.X,
                oldCenterCPPoint.Y + baseTopLeft.Y);
            CenterCPPoint = newCenterCPPoint;

            Point oldEndPoint = endPoint;
            Point newEndPoint = new Point(oldEndPoint.X + baseTopLeft.X,
                oldEndPoint.Y + baseTopLeft.Y);
            EndPoint = newEndPoint;
        }

        public override void RefreshWidgetBackColor()
        {
            base.RefreshWidgetBackColor();
            if (widgetBackColor == Brushes.Transparent)
            {
                //mainPolygon.Fill = null;
                mainPath.Fill = null;
            }
            else
            {
                //mainPolygon.Fill = widgetBackColor;
                mainPath.Fill = widgetBackColor;
            }
        }

        public override void RefreshWidgetLineColor()
        {
            //this.mainPolygon.Stroke = widgetLineColor;
            this.mainPath.Stroke = widgetLineColor;
        }

        protected void RefreshSelectedStatus()
        {
            //base.RefreshSelectedStatus();
            RefreshControlers();
        }

        public override void RefreshIsShadowVisible()
        {
            base.RefreshIsShadowVisible();

            if (isShadowVisible)
            {
                //this.mainPolygon.Effect = Widget.ShadowEffect;
                this.mainPath.Effect = Widget.ShadowEffect;
            }
            else
            {
                //this.mainPolygon.Effect = null;
                this.mainPath.Effect = null;
            }
        }

        public override void RefreshTextPanelLocatin()
        {
            switch (this.triangleForm)
            {
                case Enums.TriangleForm.Triangle:
                    {
                        LocateTextPanel(this.startPoint, this.centerCPPoint, this.endPoint);
                        break;
                    }
                default:
                    {
                        DrawLine(this.startPoint, this.centerCPPoint, this.endPoint);
                        break;
                    }
            }
        }

        public void ResetControlers(ModifingItem<Action, ModifingInfo> mi)
        {
            if (mi == null || mi.ModifingInfo == null)
            {
                MessageBox.Show("　　PolyLineWidget的ResetControlers()方法的mi参数或mi.ModifingInfo参数不能为null。",
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (masterEditor == null || masterEditor.MasterManager == null) return;

            mi.ModifingInfo.ModifingDescription = "复位控制点";

            Point newCenterCPPoint = new Point(startPoint.X + (endPoint.X - startPoint.X) / 2,
                startPoint.Y + (endPoint.Y - startPoint.Y) / 2);

            Action actCenterCPPoint = new Action(masterEditor.Id, id, this.GetType().Name, XmlTags.CenterCPPointTag,
                centerCPPoint.ToString(), newCenterCPPoint.ToString());

            CenterCPPoint = newCenterCPPoint;

            mi.AddAction(actCenterCPPoint);

            masterEditor.MasterManager.RegisterModifingItem(mi);
        }

        /// <summary>
        /// 刷新文本区旋转角度。
        /// </summary>
        public void RefreshTextRotateAngle()
        {
            if (this.mainTextPanel.RenderTransformOrigin != DefaultRenderCenter)
            {
                this.mainTextPanel.RenderTransformOrigin = DefaultRenderCenter;
            }

            if (this.textRotateAngle == 0)
            {
                this.mainTextPanel.RenderTransform = DefaultRotateTransform;
            }
            else
            {
                this.mainTextPanel.RenderTransform = new RotateTransform(textRotateAngle);
            }
        }

        public override void UpdatePointsWhenPasting(Point basePasteTopLeft)
        {
            Point newStartPoint = new Point(startPoint.X + basePasteTopLeft.X,
               startPoint.Y + basePasteTopLeft.Y);

            StartPoint = newStartPoint;

            Point newCenterCPPoint = new Point(centerCPPoint.X + basePasteTopLeft.X,
                centerCPPoint.Y + basePasteTopLeft.Y);

            CenterCPPoint = newCenterCPPoint;

            Point newEndPoint = new Point(endPoint.X + basePasteTopLeft.X,
                endPoint.Y + basePasteTopLeft.Y);

            EndPoint = newEndPoint;
        }

        #endregion


        #region 其它=========================================================================================================

        //private enum ControlDraggingType { Start, End, None, CenterCP }
        //2012年5月30日已移植并合并至Enums.LineCtrlDraggingType文件中。

        private static DoubleCollection dashArray;

        #endregion
    }
}
