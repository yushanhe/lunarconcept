﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    class TitleManager
    {
        private static FontFamily font_SimSun = new FontFamily("SimSun");

        private static FontFamily font_MicrosoftYaHei = new FontFamily("Microsoft YaHei");

        /// <summary>
        /// 小字号应该用宋体。雅黑的破折号会导致最后一个字出问题。而且，字号小时，雅黑末尾破折号显示不出来。
        /// </summary>
        public static string GetFontName(Enums.TitleStyle titleStyle = Enums.TitleStyle.Normal)
        {
            //暂时不考虑支持其它字体2012年4月12日。
            //return "SimSun";

            //2012年6月23日，主标题以及１－３级雅黑或黑体，４－５级以及正文用宋体。

            double fontSize = GetFontSize(titleStyle);
            if (fontSize > 15) return "Microsoft YaHei";
            else return "SimSun";
        }

        public static FontFamily GetFontFamily(Enums.TitleStyle titleStyle = Enums.TitleStyle.Normal)
        {
            string fontName = GetFontName(titleStyle);
            switch (fontName.ToLower())
            {
                case "microsoft yahei":
                    {
                        return font_MicrosoftYaHei;
                    }
                default:
                    {
                        return font_SimSun;
                    }
            }
        }

        public static double GetFontSize(Enums.TitleStyle titleStyle = Enums.TitleStyle.Normal)
        {
            double defFontSize;
            if (Globals.MainWindow == null || Globals.MainWindow.EditorManager == null)
            {
                defFontSize = 12;
            }
            else
            {
                defFontSize = Globals.MainWindow.EditorManager.DefaultFontSize;
            }

            switch (titleStyle)
            {
                case Enums.TitleStyle.MainTitle: return defFontSize + 10;
                case Enums.TitleStyle.T1: return defFontSize + 8;
                case Enums.TitleStyle.T2: return defFontSize + 6;
                case Enums.TitleStyle.T3: return defFontSize + 4;
                case Enums.TitleStyle.T4: return defFontSize + 2;
                case Enums.TitleStyle.T5: return defFontSize;
                default: return defFontSize;//默认１２。正文。
            }
        }

        public static FontWeight GetFontWeight(Enums.TitleStyle titleStyle = Enums.TitleStyle.Normal)
        {
            switch (titleStyle)
            {
                case Enums.TitleStyle.MainTitle: return FontWeights.Bold;
                case Enums.TitleStyle.T1: return FontWeights.Bold;
                case Enums.TitleStyle.T2: return FontWeights.Bold;
                case Enums.TitleStyle.T3: return FontWeights.Bold;
                case Enums.TitleStyle.T4: return FontWeights.Bold;
                case Enums.TitleStyle.T5:           //T5一般是当作“带缩进”的正文来用。
                default: return FontWeights.Normal; //默认。正文。
            }
        }

        /// <summary>
        /// 取某级标题对应的左上角点应在的位置。
        /// </summary>
        /// <param name="basePoint">只需要纵坐标即可。</param>
        public static Point? GetTitleStartPoint(EditorManager manager, PageEditor mainPe, Point basePoint, Enums.TitleStyle titleStyle)
        {
            if (manager == null || mainPe == null) return null;

            switch (titleStyle)
            {
                case Enums.TitleStyle.MainTitle:
                    {
                        return new System.Windows.Point(mainPe.EditArea.Left + 4, basePoint.Y);
                    }
                case Enums.TitleStyle.T1:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Left + 4 + offset, basePoint.Y);
                    }
                case Enums.TitleStyle.T2:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Left + 4 + offset * 2, basePoint.Y);
                    }
                case Enums.TitleStyle.T3:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Left + 4 + offset * 3, basePoint.Y);
                    }
                case Enums.TitleStyle.T4:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Left + 4 + offset * 4, basePoint.Y);
                    }
                case Enums.TitleStyle.T5:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Left + 4 + offset * 5, basePoint.Y);
                    }
                default://Normal
                    {
                        return new System.Windows.Point(mainPe.EditArea.Left + 4, basePoint.Y);//无缩进
                    }
            }
        }

        /// <summary>
        /// 取某级标题对应的右下角点应在的位置。
        /// </summary>
        /// <param name="basePoint">只需要纵坐标即可。</param>
        public static Point? GetTitleEndPoint(EditorManager manager, PageEditor mainPe, Point basePoint, Enums.TitleStyle titleStyle)
        {
            if (manager == null || mainPe == null) return null;

            switch (titleStyle)
            {
                case Enums.TitleStyle.MainTitle:
                    {
                        return new System.Windows.Point(mainPe.EditArea.Right - 4,
                            basePoint.Y + manager.DefaultFontSize + 20);
                    }
                case Enums.TitleStyle.T1:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Right - 4 - offset,
                            basePoint.Y + manager.DefaultFontSize + 18);
                    }
                case Enums.TitleStyle.T2:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Right - 4 - offset,
                            basePoint.Y + manager.DefaultFontSize + 16);
                    }
                case Enums.TitleStyle.T3:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Right - 4 - offset,
                            basePoint.Y + manager.DefaultFontSize + 14);
                    }
                case Enums.TitleStyle.T4:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Right - 4 - offset,
                            basePoint.Y + manager.DefaultFontSize + 12);
                    }
                case Enums.TitleStyle.T5:
                    {
                        double offset = manager.DefaultFontSize * 2;
                        return new System.Windows.Point(mainPe.EditArea.Right - 4 - offset,
                            basePoint.Y + manager.DefaultFontSize + 10);
                    }
                default://Normal
                    {
                        return new System.Windows.Point(mainPe.EditArea.Right - 4,
                            basePoint.Y + manager.DefaultFontSize + 10);
                    }
            }
        }


        /// <summary>
        /// 生成编号文本。只有T1-T4有效。T5、MainTitle、Normal直接返回string.Empty。
        /// </summary>
        /// <param name="titleLevel">标题级别。</param>
        /// <param name="number">编号数值。</param>
        public static string BuildAutoNumberString(Enums.TitleStyle titleLevel, int number)
        {
            switch (titleLevel)
            {
                case Enums.TitleStyle.T1:
                    {
                        string srcText = number.ToString();
                        srcText = srcText.Replace("1", "一");
                        srcText = srcText.Replace("2", "二");
                        srcText = srcText.Replace("3", "三");
                        srcText = srcText.Replace("4", "四");
                        srcText = srcText.Replace("5", "五");
                        srcText = srcText.Replace("6", "六");
                        srcText = srcText.Replace("7", "七");
                        srcText = srcText.Replace("8", "八");
                        srcText = srcText.Replace("9", "九");
                        srcText = srcText.Replace("0", "〇");//五笔编码常为llll或nnll极点五笔中是llll

                        return srcText + "、";//一级标题。一、XXX
                    }
                case Enums.TitleStyle.T2:
                    {
                        string srcText = number.ToString();
                        srcText = srcText.Replace("1", "一");
                        srcText = srcText.Replace("2", "二");
                        srcText = srcText.Replace("3", "三");
                        srcText = srcText.Replace("4", "四");
                        srcText = srcText.Replace("5", "五");
                        srcText = srcText.Replace("6", "六");
                        srcText = srcText.Replace("7", "七");
                        srcText = srcText.Replace("8", "八");
                        srcText = srcText.Replace("9", "九");
                        srcText = srcText.Replace("0", "○");

                        return "（" + srcText + "）";//二级标题。（二）XXX
                    }
                case Enums.TitleStyle.T3:
                    {
                        string srcText = number.ToString();
                        return srcText + "．";//三级标题。3．XXX
                    }
                case Enums.TitleStyle.T4:
                    {
                        string srcText = number.ToString();
                        return "（" + srcText + "）";//四级标题。（4）XXX
                    }
                case Enums.TitleStyle.T5:
                    {
                        //2012年6月27日：暂时T5都直接返回“◆ ”，做成项目列表的样子。不问数字。
                        return "◆ ";
                    }
                default: return string.Empty;
            }
        }

    }
}
