﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.ModifingManager;
using System.Windows;
using System.Xml;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    /// <summary>
    /// 创建时间：2012年1月21日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：将LineWidget重构一下。现在，LineWidget只分两种：ArrowLineWidget/ShapeWidget。
    /// </summary>
    public abstract class ShapeWidget : LineWidget
    {
        public ShapeWidget(PageEditor pageEditor)
            : base(pageEditor)
        {
            this.layerIndex = WidgetLayerIndex.ShapeWidget;
            RefreshLayerIndex();
        }

        public override void Build()
        {
            base.Build();

            //此类不是任何Style相关属性的“最终实现类”，因此，这里不调用任何Refreshxxx()方法。
        }

        protected override void BuildStyleProperties()
        {
            base.BuildStyleProperties();

            //此类不是任何Style相关属性的“最终实现类”，
            //也没有必须由此类读取的Xml特性值。
        }

        public override void BuildWidgetStylePropertiesAndRefresh()
        {
            base.BuildWidgetStylePropertiesAndRefresh();

            //此类不是任何Style相关属性的“最终实现类”，因此，这里不调用任何Refreshxxx()方法。
        }

        public override System.Windows.Rect OuterRect
        {
            get { return new Rect(TopLeft, BottomRight); }
        }

        /// <summary>
        /// 使用EditorManager.FormatBrush来格式化此部件。
        /// 
        /// </summary>
        /// <returns>如果被格式化，说明不是要进行选定状态的改变。</returns>
        public override bool FormatSelf(ref ModifingItem<Action, ModifingInfo> mi, ref ModifingInfo info)
        {
            if (this.masterEditor == null || this.masterEditor.MasterManager == null) return false;

            EditorManager manager = this.masterEditor.MasterManager;
            if (manager.FormatBrush == null) return false;//不在“格式刷”状态

            PageEditor mainPageEditor = this.masterEditor;

            List<Widgets.Widget> destWidgets = new List<Widget>();
            destWidgets.Add(this);//格式化功能只需要自身一个即可。不更改兄弟部件的选定状态。

            if (info == null)
            {
                info = new ModifingInfo();
                info.ModifingDescription = "格式化部件";
                manager.GetSelectedPageEditorStatus(info);
                manager.GetSelectedWidgetStatus_Old(info);
                manager.GetSelectedWidgetStatus_New(info);
            }

            bool shouldReg = false;
            if (mi == null)
            {
                shouldReg = true;
                mi = new ModifingItem<Action, ModifingInfo>(info);
            }

            //设置格式
            Action actOpacity = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetOpacityTag,
                this.WidgetOpacity.ToString(), manager.FormatBrush.DefaultWidgetStyle.WidgetOpacity.ToString());
            this.WidgetOpacity = manager.FormatBrush.DefaultWidgetStyle.WidgetOpacity;

            Action actBackColor = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetBackColor,
                BrushManager.GetName(this.WidgetBackColor),
                BrushManager.GetName(manager.FormatBrush.DefaultWidgetStyle.WidgetBackColor));
            this.WidgetBackColor = manager.FormatBrush.DefaultWidgetStyle.WidgetBackColor;

            Action actForeColor = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetForeColor,
                BrushManager.GetName(this.WidgetForeColor),
                BrushManager.GetName(manager.FormatBrush.DefaultWidgetStyle.WidgetForeColor));
            this.WidgetForeColor = manager.FormatBrush.DefaultWidgetStyle.WidgetForeColor;

            Action actLineColor = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetLineColorTag,
                BrushManager.GetName(this.WidgetLineColor),
                BrushManager.GetName(manager.FormatBrush.DefaultWidgetStyle.WidgetLineColor));
            this.WidgetLineColor = manager.FormatBrush.DefaultWidgetStyle.WidgetLineColor;

            Action actLineWidth = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetLineWidthTag,
                this.WidgetLineWidth.ToString(), manager.FormatBrush.DefaultWidgetStyle.WidgetLineWidth.ToString());
            this.WidgetLineWidth = manager.FormatBrush.DefaultWidgetStyle.WidgetLineWidth;

            Action actLineDash = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.LineDashTag,
                this.LineDash.ToString(), manager.FormatBrush.DefaultWidgetStyle.LineDash.ToString());
            this.LineDash = manager.FormatBrush.DefaultWidgetStyle.LineDash;

            Action actIsShadowVisible = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.IsShadowVisibleTag,
                this.IsShadowVisible.ToString(), manager.FormatBrush.DefaultWidgetStyle.IsShadowVisible.ToString());
            this.IsShadowVisible = manager.FormatBrush.DefaultWidgetStyle.IsShadowVisible;

            mi.AddAction(actOpacity); mi.AddAction(actBackColor); mi.AddAction(actForeColor); mi.AddAction(actLineColor);
            mi.AddAction(actLineDash); mi.AddAction(actLineWidth); mi.AddAction(actIsShadowVisible);

            var r = this as RhombWidget;
            if (r != null)
            {
                Action actWidgetForm = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.WidgetFormTag,
                    r.WidgetForm.ToString(), manager.FormatBrush.DefaultWidgetStyle.WidgetForm.ToString());
                r.WidgetForm = manager.FormatBrush.DefaultWidgetStyle.WidgetForm;
                mi.AddAction(actWidgetForm);
            }

            var t = this as TriangleWidget;
            if (t != null)
            {
                Action actTriangleForm = new Action(mainPageEditor.Id, this.id, this.GetType().Name, XmlTags.TriangleFormTag,
                    t.TriangleForm.ToString(), manager.FormatBrush.DefaultWidgetStyle.TriangleForm.ToString());
                t.TriangleForm = manager.FormatBrush.DefaultWidgetStyle.TriangleForm;
                mi.AddAction(actTriangleForm);
            }

            if (shouldReg)
            {
                manager.RegisterModifingItem(mi);

                if (manager.FormatBrush.OnlyFormatOnceTime)
                {
                    masterEditor.RaiseWidgetFormated(new List<Widgets.Widget>() { this });
                }
            }

            return true;
        }

        /// <summary>
        /// 实际上不需要此方法。因为现有的几个派生类中一个也没有调用此方法。
        /// </summary>
        public override void RefreshIsShadowVisible()
        {
            if (isShadowVisible)
            {
                this.mainTextPanel.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.mainTextPanel.Effect = null;
            }
        }

        public override void RefreshWidgetBackColor()
        {
            //this.mainTextPanel.Background = this.widgetBackColor;
            //ShapeWidget的文本区域，背景色直接用透明色。

            //此类不是WidgetBackColor的“最终实现类”，此方法也不需要进行什么额外操作。
        }

    }
}
