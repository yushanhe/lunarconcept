﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicRhombWidget
    {

        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.RhombWidget).Name));
        }

        private void MiSetBackgroundImage_Clicked(object sender, RoutedEventArgs e)
        {
            var mainSelWidget = Globals.MainWindow.EditorManager.GetMainSelectedWidget();
            if (mainSelWidget != null)
            {
                mainSelWidget.LoadImageFromFile();
            }
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        private void CMRhombWidget_Opend(object sender, RoutedEventArgs e)
        {
            if (Globals.MainWindow == null) return;
            EditorManager manager = Globals.MainWindow.EditorManager;
            if (manager == null) return;
            PageEditor pe = manager.GetMainSelectedPageEditor();
            if (pe == null) return;

            MiRhomb.IsChecked = MiCross.IsChecked = MiEightSidedShape.IsChecked =
                MiLeftParallelogram.IsChecked = MiRightParallelogram.IsChecked = false;

            Widget mainSelWidget = pe.GetMainSelectedWidget();
            if (mainSelWidget != null)
            {
                RhombWidget rw = mainSelWidget as RhombWidget;
                if (rw != null)
                {
                    switch (rw.WidgetForm)
                    {
                        case Enums.WidgetForm.Cross:
                            {
                                MiRhomb.IsChecked = false;
                                MiCross.IsChecked = true;
                                MiEightSidedShape.IsChecked = false;
                                MiLeftParallelogram.IsChecked = false;
                                MiRightParallelogram.IsChecked = false;
                                break;
                            }
                        case Enums.WidgetForm.EightSidedShape:
                            {
                                MiRhomb.IsChecked = false;
                                MiCross.IsChecked = false;
                                MiEightSidedShape.IsChecked = true;
                                MiLeftParallelogram.IsChecked = false;
                                MiRightParallelogram.IsChecked = false;
                                break;
                            }
                        case Enums.WidgetForm.LeftParallelogram:
                            {
                                MiRhomb.IsChecked = false;
                                MiCross.IsChecked = false;
                                MiEightSidedShape.IsChecked = false;
                                MiLeftParallelogram.IsChecked = true;
                                MiRightParallelogram.IsChecked = false;
                                break;
                            }
                        case Enums.WidgetForm.RightParallelogram:
                            {
                                MiRhomb.IsChecked = false;
                                MiCross.IsChecked = false;
                                MiEightSidedShape.IsChecked = false;
                                MiLeftParallelogram.IsChecked = false;
                                MiRightParallelogram.IsChecked = true;
                                break;
                            }
                        default:
                            {
                                MiRhomb.IsChecked = true;
                                MiCross.IsChecked = false;
                                MiEightSidedShape.IsChecked = false;
                                MiLeftParallelogram.IsChecked = false;
                                MiRightParallelogram.IsChecked = false;
                                break;
                            }
                    }
                }
            }
        }

        private void MiRhomb_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRhombWidgetFormCommand.Execute(Enums.WidgetForm.Rhomb));
        }

        private void MiCross_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRhombWidgetFormCommand.Execute(Enums.WidgetForm.Cross));
        }

        private void MiEightSidedShape_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRhombWidgetFormCommand.Execute(Enums.WidgetForm.EightSidedShape));
        }

        private void MiLeftParallelogram_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRhombWidgetFormCommand.Execute(Enums.WidgetForm.LeftParallelogram));
        }

        private void MiRightParallelogram_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetRhombWidgetFormCommand.Execute(Enums.WidgetForm.RightParallelogram));
        }

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
