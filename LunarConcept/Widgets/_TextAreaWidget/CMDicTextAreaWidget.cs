﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace SHomeWorkshop.LunarConcept.Widgets.ContextMenus.EventHandlers
{
    partial class CMDicTextAreaWidget
    {

        private void MiHelp_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.ShowHelpCommand.ShowHelpPage(typeof(Widgets.TextArea).Name));
        }

        private void MiSetCommentText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetCommentTextCommand.Execute());
        }

        private void MiSetHyperLinkText_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetHyperLinkTextCommand.Execute());
        }

        #region 下面这几个是内容部件公共菜单项目
        private void MiRect_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Rect));
        }

        private void MiRhomb_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Rhomb));
        }

        private void MiEllipse_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Ellipse));
        }

        private void MiRoundRect_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.RoundRect));
        }

        private void MiSixSideShape_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.SixSideShape));
        }

        private void MiEightSideShape_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.EightSidedShape));
        }

        private void MiCross_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.Cross));
        }

        private void MiLeftParallelogram_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.LeftParallelogram));
        }

        private void MiRightParallelogram_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.RightParallelogram));
        }

        private void MiFillArrowLeft_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowLeft));
        }

        private void MiFillArrowRight_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowRight));
        }

        private void MiFillArrowTop_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowTop));
        }

        private void MiFillArrowBottom_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.FillArrowBottom));
        }

        private void MiArrowLeft_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowLeft));
        }

        private void MiArrowRight_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowRight));
        }

        private void MiArrowTop_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowTop));
        }

        private void MiArrowBottom_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.SetContentWidgetsOuterBorderTypeCommand.Execute(Enums.OuterBorderType.ArrowBottom));
        }
        #endregion

        #region 下面这几个是公共菜单项目

        private void MiBoldOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc k"));
        }

        private void MiBoldOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("jc g"));
        }

        private void MiItalicOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx k"));
        }

        private void MiItalicOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("qx g"));
        }

        private void MiUnderLineOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx k"));
        }

        private void MiUnderLineOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("xhx g"));
        }

        private void MiStriketThroughOn_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx k"));
        }

        private void MiStriketThroughOff_Clicked(object sender, RoutedEventArgs e)
        {
            LunarMessage.Warning(Commands.TextCommands.RunTextCommand.Run("scx g"));
        }

        #endregion
    }
}
