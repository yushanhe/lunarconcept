﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Xml;
using SHomeWorkshop.LunarConcept.Tools;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using System.Windows.Documents;
using System.Windows.Data;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    /// <summary>
    /// 创建时间：2012年1月2日
    /// 创建者：  杨震宇
    /// 修改时间：2012年1月19日
    /// 修改内容：将TextArea的主要功能全部移植到Widget类中，这样，所有Widget都支持文本了。
    /// 
    /// 主要用途：在主页面编辑区中提供一个文本显示区（支持简单地富文本内容）。
    /// </summary>
    public class TextArea : ContentWidget
    {
        #region 构造方法=====================================================================================================

        /// <summary>
        /// [静态构造方法]
        /// </summary>
        static TextArea()
        {
            contextMenu = (ContextMenu)Globals.MainWindow.MainGrid.FindResource("CMDicTextAreaWidget");
        }

        /// <summary>
        /// [构造方法]生成一个TextArea部件。
        /// </summary>
        /// <param name="masterPageEditor">所属的PageEditor。</param>
        public TextArea(Controls.PageEditor masterPageEditor)
            : base(masterPageEditor)
        {
            this.ContextMenu = contextMenu;

            widgetType = Enums.WidgetTypes.TextArea;
            widgetClassLocalName = Widget.GetWidgetClassLocalName(this.GetType().Name);

            this.mainBorder.BorderBrush = this.widgetLineColor = Brushes.Black;
            this.widgetLineWidth = 2;
            this.mainBorder.BorderThickness = defLineWidth;
            this.mainBorder.Padding = defBorderPadding;

            //部件内容由派生部件类自行实现。
            this.mainBorder.Child = this.mainTextPanel;
        }

        #endregion

        /// <summary>
        /// 本类通用上下文菜单。
        /// </summary>
        private static ContextMenu contextMenu;

        public static Thickness defLineWidth = new Thickness(2);

        public static Thickness defBorderPadding = new Thickness(10);

        public override void RefreshTextPanelLocatin()
        {
            //不需要内容。此类的文本面板自动定位。
        }
    }
}
