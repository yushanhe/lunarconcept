﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SHomeWorkshop.LunarConcept.Adorners;
using System.Windows;
using System.Xml;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.ModifingManager;

namespace SHomeWorkshop.LunarConcept.Widgets.Interfaces
{
    /// <summary>
    /// 创建时间：二○一二年五月三十日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：令ContentWidget、RectangleWidget、RhombWidget(菱形)、EllipseWidget均支持“拖出自动连接线”。
    /// ★备注：　此前只有ContentWidget类支持自动连接线，且自动连接线只能是直线。
    /// </summary>
    public interface ICanBeLinkedWidget
    {
        double ActualHeight { get; }

        double ActualWidth { get; }

        Point BottomRight { get; }

        /// <summary>
        /// 必须有中心装饰器。用于“拖出自动连接线”。
        /// </summary>
        WidgetCenterAdorner CenterAdorner { get; }

        string Id { get; }

        bool IsLinked { get; }

        bool IsMainSelected { get; set; }

        bool IsSelected { get; set; }

        bool IsShadowVisible { get; set; }

        Point Location { get; set; }

        Controls.PageEditor MasterEditor { get; }

        /// <summary>
        /// 部件被拖动时的外边。
        /// </summary>
        Rect MovingRect { get; }

        Rect OuterRect { get; }

        Point TopLeft { get; }

        XmlNode XmlData { get; set; }

        double WidgetOpacity { get; set; }

        Brush WidgetBackColor { get; set; }

        Brush WidgetLineColor { get; set; }

        Brush WidgetForeColor { get; set; }

        double WidgetLineWidth { get; set; }

        /// <summary>
        /// 应返回部件类定义的中文名称。
        /// </summary>
        string WidgetClassName { get; }

        string ImageBase64 { get; set; }

        bool IsWidgetPropertySetted(string property);

        /// <summary>
        /// [只读]部件类型的枚举名称。为便于switch而提供，免去使用反射的麻烦（尽管不够灵活）。
        /// </summary>
        Enums.WidgetTypes WidgetType { get; }

        Thickness WidgetPadding { get; set; }

        Point StartPoint { get; set; }

        Point EndPoint { get; set; }

        Enums.TitleStyle TitleLevel { get; set; }

        //方法===================================================================================================

        List<ILinkableLine> GetLinkedLines();

        void InvalidateArrange();

        void NewID();

        void SelectOnlySelf();

        void UpdateLayout();

        void MoveLeftSiderTo(ModifingItem<Action, ModifingInfo> mi, double left);

        void MoveRightSiderTo(ModifingItem<Action, ModifingInfo> mi, double right);

        void MoveTopSiderTo(ModifingItem<Action, ModifingInfo> mi, double top);

        void MoveBottomSiderTo(ModifingItem<Action, ModifingInfo> mi, double bottom);
    }
}
