﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Controls;

namespace SHomeWorkshop.LunarConcept.Widgets.Interfaces
{
    /// <summary>
    /// 创建时间：2012年1月22日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：让Rectangle/Ellipse/Rhomb/StraitLine支持等尺寸功能。
    ///           ——它们的共同特征是：都只有“StartPoint”和“EndPoint”这两个控制点。
    /// </summary>
    public interface ICanSameSize
    {
        /// <summary>
        /// [只读]提供此属性是因为修改操作记录项（Action）需要部件的Id。
        /// </summary>
        string Id { get; }

        bool IsSelected { get; set; }

        bool IsMainSelected { get; set; }

        /// <summary>
        /// [只读]提供此属性是因为修改操作记录项（Action）还需要部件所在页面编辑器的Id。
        /// </summary>
        PageEditor MasterEditor { get; }

        /// <summary>
        /// [读写]首控制点坐标。
        /// </summary>
        [Tools.LunarProperty("StartPoint", Enums.PropertyDateType.Point)]
        Point StartPoint { get; set; }

        /// <summary>
        /// [读写]首控制点坐标。
        /// </summary>
        [Tools.LunarProperty("EndPoint", Enums.PropertyDateType.Point)]
        Point EndPoint { get; set; }

        /// <summary>
        /// [只读]左上角坐标。
        /// </summary>
        Point TopLeft { get; }

        /// <summary>
        /// [只读]右下角坐标。
        /// </summary>
        Point BottomRight { get; }
    }
}
