﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace SHomeWorkshop.LunarConcept.Widgets.WidgetsStyleSamples
{
    /// <summary>
    /// 创建时间：2012年6月15日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：创建一个代表对应部件的样式的CheckBox。以便设置部件默认样式。
    /// 
    /// ★注意★：类名必须以“_WSS”开头。
    /// </summary>
    class _WSSRhombWidget : WidgetsStyleSamples.WidgetStyleSample
    {
        public _WSSRhombWidget(WidgetStyle srcStyle, string localName) :
            base(srcStyle, localName)
        {
            this.mainStyle = WidgetStyle.FromWidgetClassName(typeof(Widgets.RhombWidget).Name);

            this.sampleTextBlock.Text =
                (this.mainStyle != null && string.IsNullOrEmpty(this.mainStyle.Text) == false) ?
                    this.mainStyle.Text :
                    this.WidgetClassLocalName;

            this.Children.Add(this.sampleTextBlock);

            Canvas.SetZIndex(sampleTextBlock, 100);

            Canvas.SetLeft(this.sampleTextBlock, (this.Width - this.sampleTextBlock.Width) / 2);
            Canvas.SetTop(this.sampleTextBlock, (this.Height - this.sampleTextBlock.Height) / 2);

            mainPolygon.Points = new PointCollection()
            {
                new Point(this.Width/2,10),
                new Point(10,this.Height/2),
                new Point(this.Width/2,this.Height-10),
                new Point(this.Width-10,this.Height/2),
            };

            this.Children.Add(this.mainPolygon);

            //=========挂接需要的事件。
            this.MainStyle = WidgetStyle.FromWidgetClassName(this.WidgetClassName);

            //this.mainStyle.ArrowsChanged += new EventHandler<ArrowsChangedEventArgs>(mainStyle_ArrowsChanged);//无意义
            this.MainStyle.IsShadowVisibleChanged += new EventHandler<IsShadowVisibleChangedEventArgs>(mainStyle_IsShadowVisibleChanged);
            this.MainStyle.LineDashChanged += new EventHandler<LineDashChangedEventArgs>(mainStyle_LineDashChanged);

            this.MainStyle.WidgetBackColorChanged += new EventHandler<WidgetBackColorChangedEventArgs>(mainStyle_WidgetBackColorChanged);
            this.MainStyle.WidgetLineColorChanged += new EventHandler<WidgetLineColorChangedEventArgs>(mainStyle_WidgetLineColorChanged);
            this.MainStyle.WidgetLineWidthChanged += new EventHandler<WidgetLineWidthChangedEventArgs>(mainStyle_WidgetLineWidthChanged);

            this.MainStyle.WidgetForeColorChanged += new EventHandler<WidgetForeColorChangedEventArgs>(mainStyle_WidgetForeColorChanged);
            this.MainStyle.WidgetOpacityChanged += new EventHandler<WidgetOpacityChangedEventArgs>(mainStyle_WidgetOpacityChanged);
            //this.mainStyle.WidgetPaddingChanged += new EventHandler<WidgetPaddingChangedEventArgs>(mainStyle_WidgetPaddingChanged);//无意义
        }

        //void mainStyle_WidgetPaddingChanged(object sender, WidgetPaddingChangedEventArgs e)
        //{
        //
        //}

        void mainStyle_WidgetOpacityChanged(object sender, WidgetOpacityChangedEventArgs e)
        {
            this.mainPolygon.Opacity = this.sampleTextBlock.Opacity = e.NewValue;
        }

        void mainStyle_WidgetForeColorChanged(object sender, WidgetForeColorChangedEventArgs e)
        {
            this.sampleTextBlock.Foreground = e.NewValue;
        }

        void mainStyle_WidgetLineWidthChanged(object sender, WidgetLineWidthChangedEventArgs e)
        {
            this.mainPolygon.StrokeThickness = (e.NewValue < 1) ? 1 : e.NewValue;
        }

        void mainStyle_WidgetLineColorChanged(object sender, WidgetLineColorChangedEventArgs e)
        {
            this.mainPolygon.Stroke = e.NewValue;
        }

        void mainStyle_WidgetBackColorChanged(object sender, WidgetBackColorChangedEventArgs e)
        {
            this.mainPolygon.Fill = e.NewValue;
        }

        void mainStyle_LineDashChanged(object sender, LineDashChangedEventArgs e)
        {
            switch (e.NewValue)
            {
                case LineDashType.DashType.Dash:
                    {
                        this.mainPolygon.StrokeDashArray = LineDashType.dashCollection; break;
                    }
                case LineDashType.DashType.DashDotDot:
                    {
                        this.mainPolygon.StrokeDashArray = LineDashType.dashDotDotCollection; break;
                    }
                case LineDashType.DashType.Dot:
                    {
                        this.mainPolygon.StrokeDashArray = LineDashType.dotCollection; break;
                    }
                case LineDashType.DashType.DashDot:
                    {
                        this.mainPolygon.StrokeDashArray = LineDashType.dashDotCollection; break;
                    }
                default:
                    {
                        this.mainPolygon.StrokeDashArray = LineDashType.solidCollection; break;
                    }
            }
        }

        void mainStyle_IsShadowVisibleChanged(object sender, IsShadowVisibleChangedEventArgs e)
        {
            if (e.NewValue)
            {
                this.Effect = Widget.ShadowEffect;
            }
            else
            {
                this.Effect = null;
            }
        }

        //void mainStyle_ArrowsChanged(object sender, ArrowsChangedEventArgs e)
        //{
        //}

        private Polygon mainPolygon = new Polygon()
        {
            Stroke = Brushes.Black,
            StrokeThickness = 2,
        };
    }
}