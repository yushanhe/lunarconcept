﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Reflection;
using System.Windows;
using System.Windows.Media;

namespace SHomeWorkshop.LunarConcept.Widgets.WidgetsStyleSamples
{
    /// <summary>
    /// 创建时间：2012年6月14日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：用于表示各部件样式的示例类。部件本身与编辑器耦合过密，不能直接使用。
    ///           只有部分属性才是各部件均支持的属性。也只有这些公有属性才应支持样式。
    ///           
    /// ★注意★：所有自本类派生的类，类名均需要以“_WSS”开头。因为本类中使用了反射来生成相关类的实例。
    /// </summary>
    class WidgetStyleSample : Canvas
    {
        public WidgetStyleSample(WidgetStyle srcStyle, string localName)
        {
            this.Width = 64;
            this.Height = 48;
            this.Margin = DefaultMargin;
            this.Background = Brushes.Transparent;

            if (srcStyle != null)
            {
                this.widgetClassName = srcStyle.WidgetClassName;
            }
            this.widgetClassLocalName = localName;

            //各部件示例自行控制文本显示。

        }

        /// <summary>
        /// 此值必须由派生类自行实例化。
        /// </summary>
        protected WidgetStyle mainStyle = null;

        public WidgetStyle MainStyle
        {
            get { return mainStyle; }
            set { mainStyle = value; }
        }

        private static readonly Thickness defaultMargin = new Thickness(10);

        public static Thickness DefaultMargin
        {
            get { return WidgetStyleSample.defaultMargin; }
        }

        private string widgetClassLocalName = "未知部件";
        /// <summary>
        /// 本地化的[部件类名称]。
        /// </summary>
        public string WidgetClassLocalName
        {
            get { return widgetClassLocalName; }
        }

        private string widgetClassName = "Widget";
        /// <summary>
        /// [只读]对应的部件类的名称。此样式服务于指定类型的部件。
        /// </summary>
        public string WidgetClassName
        {
            get { return widgetClassName; }
        }

        protected TextBlock sampleTextBlock = new TextBlock()
        {
            Width = 24,
            Height = 16,
            TextAlignment = TextAlignment.Center
        };

        /// <summary>
        /// 根据提供的部件类的名称来生成对应的“部件样式示例”的实例。
        /// </summary>
        /// <param name="widgetClassName"></param>
        /// <returns></returns>
        public static WidgetStyleSample FromWidgetClassName(string widgetClassName)
        {
            //利用反射来自动生成相应的实例。

            //定义参数类型数组   
            Type[] argrmentTypes = new Type[2];
            argrmentTypes[0] = typeof(WidgetStyle);
            argrmentTypes[1] = typeof(string);

            //定义参数数组   
            object[] arguments = new object[2];
            arguments[0] = WidgetStyle.FromWidgetClassName(widgetClassName);
            arguments[1] = Widget.GetWidgetClassLocalName(widgetClassName);

            Type[] _wssClasses = Globals.MainWindow.GetType().Module.FindTypes(Module.FilterTypeNameIgnoreCase, "_wss*");

            foreach (Type type in _wssClasses)
            {
                if (type.Name.Contains(widgetClassName))
                {
                    return type.GetConstructor(argrmentTypes).Invoke(arguments) as WidgetStyleSample;
                }
            }

            return null;
        }
    }
}
