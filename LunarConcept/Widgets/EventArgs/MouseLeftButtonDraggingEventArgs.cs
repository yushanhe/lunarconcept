﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using SHomeWorkshop.LunarConcept.Enums;
using SHomeWorkshop.LunarConcept.Controls;
using System.Windows.Input;

namespace SHomeWorkshop.LunarConcept.Widgets
{
    public class MouseLeftButtonDraggingEventArgs : EventArgs
    {
        private ModifierKeys modifiers = ModifierKeys.None;
        /// <summary>
        /// 修饰键。
        /// </summary>
        public ModifierKeys Modifiers
        {
            get { return modifiers; }
            set { modifiers = value; }
        }

        public PageEditor MasterPageEditor { get; set; }

        public Widget MainSelectedWidget { get; set; }

        public string ControlerName { get; set; }

        public Point StartPoint { get; set; }

        public Point EndPoint { get; set; }

        private PageDraggingType draggingType = PageDraggingType.SelectWidgets;
        /// <summary>
        /// 正在使用鼠标进行何种操作。
        /// </summary>
        public PageDraggingType DraggingType
        {
            get { return draggingType; }
            set { draggingType = value; }
        }
    }
}
