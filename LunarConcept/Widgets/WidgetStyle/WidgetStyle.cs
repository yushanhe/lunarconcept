﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using SHomeWorkshop.LunarConcept.Enums;
using System.Windows;
using SHomeWorkshop.LunarConcept.Tools;
using SHomeWorkshop.LunarConcept.Widgets;

namespace SHomeWorkshop.LunarConcept
{
    /// <summary>
    /// 创建时间：2012年1月31日
    /// 创建者：  杨震宇
    /// 
    /// 主要用途：给每种部件提供一种默认属性组合。
    /// 
    ///           这些属性包括：前景、背景、边框（或线）宽、有无箭头等。
    /// </summary>
    public class WidgetStyle
    {
        /// <summary>
        /// [构造方法]直接调用此构造方法，必须提供使用此Style的部件类的类名。
        /// </summary>
        /// <param name="widgetClassName">部件类类名。</param>
        public WidgetStyle(string widgetClassName)
        {
            this.widgetClassName = (widgetClassName != null) ? widgetClassName : string.Empty;
        }

        private string widgetClassName = string.Empty;
        /// <summary>
        /// [读写]样式所服务的部件类的名称。
        /// </summary>
        public string WidgetClassName
        {
            get { return widgetClassName; }
            set { widgetClassName = value; }
        }

        private double radius = 0;
        /// <summary>
        /// [读写]矩形部件支持“圆角率”。
        /// </summary>
        public double Radius
        {
            get { return radius; }
            set
            {
                double oldValue = this.radius;
                this.radius = value;
                this.OnWidgetRadiusChanged(this, new WidgetRadiusChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private double widgetOpacity = 1.0;
        /// <summary>
        /// 表示透明度。
        /// </summary>
        public double WidgetOpacity
        {
            get { return widgetOpacity; }
            set
            {
                double oldValue = this.widgetOpacity;
                this.widgetOpacity = value;
                this.OnWidgetOpacityChanged(this, new WidgetOpacityChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private Thickness widgetPadding = new Thickness(0);
        /// <summary>
        /// 只有对ContentWidget才有意义。
        /// </summary>
        public Thickness WidgetPadding
        {
            get { return widgetPadding; }
            set
            {
                Thickness oldValue = this.widgetPadding;
                this.widgetPadding = value;
                this.OnWidgetPaddingChanged(this, new WidgetPaddingChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private Brush widgetForeColor = Brushes.Black;

        public Brush WidgetForeColor
        {
            get { return widgetForeColor; }
            set
            {
                Brush oldValue = this.widgetForeColor;
                this.widgetForeColor = value;
                this.OnWidgetForeColorChanged(this, new WidgetForeColorChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private Brush widgetBackColor = Brushes.Transparent;

        public Brush WidgetBackColor
        {
            get { return widgetBackColor; }
            set
            {
                Brush oldValue = this.widgetBackColor;
                this.widgetBackColor = value;
                this.OnWidgetBackColorChanged(this, new WidgetBackColorChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private Brush widgetLineColor = Brushes.Black;

        public Brush WidgetLineColor
        {
            get { return widgetLineColor; }
            set
            {
                Brush oldValue = this.widgetLineColor;
                widgetLineColor = value;
                this.OnWidgetLineColorChanged(this, new WidgetLineColorChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private double widgetLineWidth = 2;

        public double WidgetLineWidth
        {
            get { return widgetLineWidth; }
            set
            {
                double oldValue = this.widgetLineWidth;
                this.widgetLineWidth = value;
                this.OnWidgetLineWidthChanged(this, new WidgetLineWidthChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private OuterBorderType widgetOutBorderType = OuterBorderType.Rect;

        public OuterBorderType WidgetOutBorderType
        {
            get { return this.widgetOutBorderType; }
            set { this.widgetOutBorderType = value; }
        }

        private bool isShadowVisible = false;

        public bool IsShadowVisible
        {
            get { return isShadowVisible; }
            set
            {
                bool oldValue = this.isShadowVisible;
                this.isShadowVisible = value;
                this.OnIsShadowVisibleChanged(this, new IsShadowVisibleChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        /// <summary>
        /// 部件形态，目前只用于菱形部件。
        /// </summary>
        public WidgetForm WidgetForm { get; set; } = WidgetForm.UnKnown;

        /// <summary>
        /// 三角形部件的形态。
        /// </summary>
        public TriangleForm TriangleForm { get; set; } = TriangleForm.Triangle;

        private ArrowType arrows = ArrowType.End;//默认为End。
        /// <summary>
        /// 只有对ArrowLineWidget才有意义。
        /// </summary>
        public ArrowType Arrows
        {
            get { return arrows; }
            set
            {
                ArrowType oldValue = this.arrows;
                this.arrows = value;
                this.OnArrowsChanged(this, new ArrowsChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        /// <summary>
        /// 只有对LineWidget才有意义。
        /// </summary>
        private LineDashType.DashType lineDash = LineDashType.DashType.Solid;

        public LineDashType.DashType LineDash
        {
            get { return lineDash; }
            set
            {
                LineDashType.DashType oldValue = this.lineDash;
                this.lineDash = value;
                this.OnLineDashChanged(this, new LineDashChangedEventArgs()
                {
                    OldValue = oldValue,
                    NewValue = value,
                });
            }
        }

        private string text = string.Empty;

        /// <summary>
        /// 一般用作示例文本。
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }


        #region 方法================================================================================================

        /// <summary>
        /// 从另一个WidgetStyle复制值到此实例。
        /// </summary>
        /// <param name="anotherWidgetStyle">要复制值的源实例。</param>
        /// <returns>如果源为null，返回false。否则返回真。</returns>
        public bool CopyValue(WidgetStyle anotherWidgetStyle)
        {
            if (anotherWidgetStyle == null) return false;

            this.Arrows = anotherWidgetStyle.Arrows;
            this.IsShadowVisible = anotherWidgetStyle.IsShadowVisible;
            this.LineDash = anotherWidgetStyle.LineDash;
            this.WidgetBackColor = anotherWidgetStyle.WidgetBackColor;
            this.WidgetLineColor = anotherWidgetStyle.WidgetLineColor;
            this.WidgetLineWidth = anotherWidgetStyle.WidgetLineWidth;
            this.WidgetForeColor = anotherWidgetStyle.WidgetForeColor;
            this.WidgetOpacity = anotherWidgetStyle.WidgetOpacity;
            return true;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[StyleName]Text=" + this.text + "\r\n");
            sb.Append("[WidgetName]WidgetClassName=" + this.widgetClassName + "\r\n");

            sb.Append("[01]Arrows=" + this.Arrows.ToString() + "\r\n");
            sb.Append("[02]IsShadowVisible=" + this.IsShadowVisible.ToString() + "\r\n");
            sb.Append("[03]LineDash=" + this.LineDash.ToString() + "\r\n");

            sb.Append("[04]WidgetBackColor=" + BrushManager.GetName(this.WidgetBackColor) + "\r\n");
            sb.Append("[05]WidgetLineColor=" + BrushManager.GetName(this.WidgetLineColor) + "\r\n");
            sb.Append("[06]WidgetLineWidth=" + this.WidgetLineWidth.ToString() + "\r\n");

            sb.Append("[07]WidgetForeColor=" + BrushManager.GetName(this.WidgetForeColor) + "\r\n");
            sb.Append("[08]WidgetOpacity=" + this.WidgetOpacity.ToString() + "\r\n");
            sb.Append("[09]WidgetPadding=" + this.WidgetPadding.ToString() + "\r\n");

            sb.Append("[10]WidgetRadius=" + this.Radius.ToString() + "\r\n");
            sb.Append("[11]WidgetOutBorderType=" + this.WidgetOutBorderType.ToString() + "\r\n");
            return sb.ToString();
        }

        public string ToXML()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<WidgetStyle Text=\"" + this.text + "\" ");

            sb.Append("WidgetClassName=\"" + this.widgetClassName + "\" ");
            sb.Append("Arrows=\"" + this.Arrows.ToString() + "\" ");
            sb.Append("IsShadowVisible=\"" + this.IsShadowVisible.ToString() + "\" ");
            sb.Append("LineDash=\"" + this.LineDash.ToString() + "\" ");

            sb.Append("WidgetBackColor=\"" + BrushManager.GetName(this.WidgetBackColor) + "\" ");
            sb.Append("WidgetLineColor=\"" + BrushManager.GetName(this.WidgetLineColor) + "\" ");
            sb.Append("WidgetLineWidth=\"" + this.WidgetLineWidth.ToString() + "\" ");

            sb.Append("WidgetForeColor=\"" + BrushManager.GetName(this.WidgetForeColor) + "\" ");
            sb.Append("WidgetOpacity=\"" + this.WidgetOpacity.ToString() + "\" ");
            sb.Append("WidgetPadding=\"" + this.WidgetPadding.ToString() + "\" ");

            sb.Append("WidgetRadius=\"" + this.Radius.ToString() + "\" ");
            sb.Append("WidgetOutBorderType=\"" + this.WidgetOutBorderType.ToString() + "\" ");
            sb.Append("WidgetForm=\"" + this.WidgetForm.ToString() + "\" ");
            sb.Append("TriangleForm=\"" + this.TriangleForm.ToString() + "\" ");
            sb.Append("/>");

            return sb.ToString();
        }

        /// <summary>
        /// 从WidgetStyle.ToXML()实例方法导出的Xml文本重新生成一个WidgetStyle实例。
        /// </summary>
        /// <param name="xml">XML文本。注意：不要添加Xml头。</param>
        /// <returns>如果Xml文本不合法，会返回一个默认值（没有ClassName，其实没啥用处）。</returns>
        public static WidgetStyle FromXML(string xml)
        {
            if (string.IsNullOrEmpty(xml)) return new WidgetStyle(string.Empty);

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            try
            {
                doc.LoadXml(XmlTools.XmlHeaderText + xml);

                WidgetStyle newStyle = new WidgetStyle(string.Empty);

                if (doc.DocumentElement.Name != "WidgetStyle") return new WidgetStyle(string.Empty);

                System.Xml.XmlAttribute attrText = doc.DocumentElement.GetAttributeByName("Text");
                if (attrText != null)
                {
                    newStyle.Text = attrText.Value;
                }

                System.Xml.XmlAttribute attrWidgetClassName = doc.DocumentElement.GetAttributeByName("WidgetClassName");
                if (attrWidgetClassName != null)
                {
                    newStyle.WidgetClassName = attrWidgetClassName.Value;
                }

                System.Xml.XmlAttribute attrArrows = doc.DocumentElement.GetAttributeByName("Arrows");
                if (attrArrows != null)
                {
                    newStyle.Arrows = (ArrowType)Enum.Parse(typeof(ArrowType), attrArrows.Value);
                }

                System.Xml.XmlAttribute attrIsShadowVisible = doc.DocumentElement.GetAttributeByName("IsShadowVisible");
                if (attrIsShadowVisible != null)
                {
                    newStyle.IsShadowVisible = bool.Parse(attrIsShadowVisible.Value);
                }

                System.Xml.XmlAttribute attrLineDash = doc.DocumentElement.GetAttributeByName("LineDash");
                if (attrLineDash != null)
                {
                    newStyle.LineDash = (LineDashType.DashType)Enum.Parse(typeof(LineDashType.DashType), attrLineDash.Value);
                }

                System.Xml.XmlAttribute attrWidgetBackColor = doc.DocumentElement.GetAttributeByName("WidgetBackColor");
                if (attrWidgetBackColor != null)
                {
                    newStyle.WidgetBackColor = BrushManager.GetBrush(attrWidgetBackColor.Value);
                }

                System.Xml.XmlAttribute attrWidgetLineColor = doc.DocumentElement.GetAttributeByName("WidgetLineColor");
                if (attrWidgetLineColor != null)
                {
                    newStyle.WidgetLineColor = BrushManager.GetBrush(attrWidgetLineColor.Value);
                }

                System.Xml.XmlAttribute attrWidgetLineWidth = doc.DocumentElement.GetAttributeByName("WidgetLineWidth");
                if (attrWidgetLineWidth != null)
                {
                    newStyle.WidgetLineWidth = double.Parse(attrWidgetLineWidth.Value);
                }

                System.Xml.XmlAttribute attrWidgetForeColor = doc.DocumentElement.GetAttributeByName("WidgetForeColor");
                if (attrWidgetForeColor != null)
                {
                    newStyle.WidgetForeColor = BrushManager.GetBrush(attrWidgetForeColor.Value);
                }

                System.Xml.XmlAttribute attrWidgetOpacity = doc.DocumentElement.GetAttributeByName("WidgetOpacity");
                if (attrWidgetOpacity != null)
                {
                    newStyle.WidgetOpacity = double.Parse(attrWidgetOpacity.Value);
                }

                System.Xml.XmlAttribute attrWidgetPadding = doc.DocumentElement.GetAttributeByName("WidgetPadding");
                if (attrWidgetPadding != null)
                {
                    newStyle.WidgetPadding = (Thickness)Globals.ThickConverter.ConvertFrom(attrWidgetPadding.Value);
                }

                System.Xml.XmlAttribute attrRadius = doc.DocumentElement.GetAttributeByName("Radius");
                if (attrRadius != null)
                {
                    newStyle.Radius = double.Parse(attrRadius.Value);
                }

                System.Xml.XmlAttribute attrWidgetOutBorderType = doc.DocumentElement.GetAttributeByName("WidgetOutBorderType");
                if (attrRadius != null)
                {
                    newStyle.widgetOutBorderType = (OuterBorderType)Enum.Parse(typeof(OuterBorderType), attrWidgetOutBorderType.Value);
                }

                System.Xml.XmlAttribute attrWidgetForm = doc.DocumentElement.GetAttributeByName("WidgetForm");
                if (attrWidgetForm != null)
                {
                    if (Enum.TryParse<WidgetForm>(attrWidgetForm.Value, out WidgetForm widgetForm))
                    {
                        newStyle.WidgetForm = widgetForm;
                    }
                }

                System.Xml.XmlAttribute attrTriangleForm = doc.DocumentElement.GetAttributeByName("TriangleForm");
                if (attrTriangleForm != null)
                {
                    if (Enum.TryParse<TriangleForm>(attrTriangleForm.Value, out TriangleForm triangleForm))
                    {
                        newStyle.TriangleForm = triangleForm;
                    }
                }

                return newStyle;
            }
            catch (Exception ex)
            {
                MessageBox.Show("　　WidgetStyle.FromXML()方法转换出错。异常信息如下：\r\n　　" + ex.Message,
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
        }

        /// <summary>
        /// 根据类名生成一个默认的Style。★注意：可以不提供类名。因此生成的样式的WidgetClassName属性值可能不合需要。
        /// </summary>
        /// <param name="widgetClassName">部件类名称。</param>
        /// <returns>不会返回null。</returns>
        public static WidgetStyle FromWidgetClassName(string widgetClassName)
        {
            if (string.IsNullOrEmpty(widgetClassName)) return new WidgetStyle(string.Empty);

            return new WidgetStyle(widgetClassName);
        }

        /// <summary>
        /// 从此类某个实例调用ToString()方法生成的字符串后，利用这个字符串重新生成一个此类的实例。
        /// ★注意：若源文本中没有相关信息，会返回一个默认的实例。不会返回null。
        ///         若源文本有缺陷（不符合规则），才会返回null。
        /// </summary>
        /// <param name="srcText">源文本。</param>
        /// <returns>此类的新实例。</returns>
        public static WidgetStyle FromString(string srcText)
        {
            if (string.IsNullOrEmpty(srcText)) return new WidgetStyle(string.Empty);
            string[] textArray;

            if (srcText.Contains("\r\n"))
            {
                char[] separator = new char[2];
                separator[0] = '\r'; separator[1] = '\n';
                textArray = srcText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                string[] separator = new string[1];
                separator[0] = "&#xD;&#xA";
                textArray = srcText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
            }


            if (textArray.Length <= 0) return new WidgetStyle(string.Empty);

            WidgetStyle newStyle = new WidgetStyle(string.Empty);

            try
            {
                foreach (string text in textArray)
                {
                    int index = text.IndexOf("=");
                    if (index < 0 || index >= text.Length) continue;
                    string value = text.Substring(index + 1);

                    if (text.StartsWith("[StyleName]"))
                    {
                        newStyle.Text = value;
                    }
                    else if (text.StartsWith("[WidgetName]"))
                    {
                        newStyle.WidgetClassName = value;
                    }
                    else if (text.StartsWith("[01]"))
                    {
                        newStyle.Arrows = (ArrowType)Enum.Parse(typeof(ArrowType), value);
                    }
                    else if (text.StartsWith("[02]"))
                    {
                        newStyle.IsShadowVisible = bool.Parse(value);
                    }
                    else if (text.StartsWith("[03]"))
                    {
                        newStyle.LineDash = (LineDashType.DashType)Enum.Parse(typeof(LineDashType.DashType), value);
                    }
                    else if (text.StartsWith("[04]"))
                    {
                        newStyle.WidgetBackColor = BrushManager.GetBrush(value);
                    }
                    else if (text.StartsWith("[05]"))
                    {
                        newStyle.WidgetLineColor = BrushManager.GetBrush(value);
                    }
                    else if (text.StartsWith("[06]"))
                    {
                        newStyle.WidgetLineWidth = double.Parse(value);
                    }
                    else if (text.StartsWith("[07]"))
                    {
                        newStyle.WidgetForeColor = BrushManager.GetBrush(value);
                    }
                    else if (text.StartsWith("[08]"))
                    {
                        newStyle.WidgetOpacity = double.Parse(value);
                    }
                    else if (text.StartsWith("[09]"))
                    {
                        newStyle.WidgetPadding = (Thickness)Globals.ThickConverter.ConvertFrom(value);
                    }
                    else if (text.StartsWith("[10]"))
                    {
                        newStyle.Radius = double.Parse(value);
                    }
                    else if (text.StartsWith("[11]"))
                    {
                        newStyle.WidgetOutBorderType = (OuterBorderType)Enum.Parse(typeof(OuterBorderType), value);
                    }
                }

                return newStyle;
            }
            catch (Exception ex)
            {
                MessageBox.Show("　　WidgetStyle.FromString()方法转换出错。异常信息如下：\r\n　　" + ex.Message,
                    Globals.AppName, MessageBoxButton.OK, MessageBoxImage.Warning);
                return null;
            }
        }

        /// <summary>
        /// 从部件取WidgetStyle。如参数为null，返回null。
        /// </summary>
        /// <param name="srcWidget">要取样式的部件。</param>
        /// <returns>顺利的话，返回WidgetStyle新实例。否则返回null。</returns>
        public static WidgetStyle FromWidget(Widgets.Widget srcWidget)
        {
            if (srcWidget == null) return null;

            WidgetStyle newStyle = new WidgetStyle(string.Empty);
            string widgetClassName = srcWidget.GetType().Name;

            newStyle.WidgetClassName = widgetClassName;

            switch (widgetClassName)
            {
                case "TextArea":
                    {
                        TextArea w = srcWidget as TextArea;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetOutBorderType = OuterBorderType.Rect;
                        break;
                    }
                case "PictureBox":
                    {
                        PictureBox w = srcWidget as PictureBox;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetOutBorderType = OuterBorderType.Rect;
                        break;
                    }
                case "BezierLineWidget":
                    {
                        BezierLineWidget w = srcWidget as BezierLineWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.Arrows = w.Arrows;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = srcWidget.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.Radius = w.Radius;
                        break;
                    }
                case "BracketWidget":
                    {
                        BracketWidget w = srcWidget as BracketWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.Arrows = w.Arrows;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = srcWidget.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.Radius = w.Radius;
                        break;
                    }
                case "EllipseWidget":
                    {
                        EllipseWidget w = srcWidget as EllipseWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        break;
                    }
                case "GroupWidget":
                    {
                        GroupWidget w = srcWidget as GroupWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetOutBorderType = OuterBorderType.Rect;
                        break;
                    }
                case "PolyLineWidget":
                    {
                        PolyLineWidget w = srcWidget as PolyLineWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.Arrows = w.Arrows;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = srcWidget.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.Radius = w.Radius;
                        break;
                    }
                case "RectangleWidget":
                    {
                        RectangleWidget w = srcWidget as RectangleWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.Radius = w.Radius;
                        break;
                    }
                case "RhombWidget":
                    {
                        RhombWidget w = srcWidget as RhombWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetForm = w.WidgetForm;
                        break;
                    }
                case "StraitLineWidget":
                    {
                        StraitLineWidget w = srcWidget as StraitLineWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.Arrows = w.Arrows;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineColor = srcWidget.WidgetLineColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.WidgetPadding = w.WidgetPadding;
                        newStyle.Radius = w.Radius;
                        break;
                    }
                case "TriangleWidget":
                    {
                        TriangleWidget w = srcWidget as TriangleWidget;
                        newStyle.WidgetOpacity = srcWidget.WidgetOpacity;
                        newStyle.WidgetBackColor = srcWidget.WidgetBackColor;
                        newStyle.WidgetLineWidth = w.WidgetLineWidth;
                        newStyle.WidgetLineColor = w.WidgetLineColor;
                        newStyle.LineDash = w.LineDash;
                        newStyle.WidgetForeColor = srcWidget.WidgetForeColor;
                        newStyle.IsShadowVisible = srcWidget.IsShadowVisible;
                        newStyle.TriangleForm = (srcWidget as TriangleWidget).TriangleForm;
                        break;
                    }
                default:
                    {
                        return null;
                    }
            }

            return newStyle;
        }

        #endregion


        #region 事件================================================================================================


        public event EventHandler<ArrowsChangedEventArgs> ArrowsChanged;

        protected void OnArrowsChanged(object sender, ArrowsChangedEventArgs e)
        {
            if (this.ArrowsChanged != null)
            {
                this.ArrowsChanged(sender, e);
            }
        }

        public event EventHandler<IsShadowVisibleChangedEventArgs> IsShadowVisibleChanged;

        protected void OnIsShadowVisibleChanged(object sender, IsShadowVisibleChangedEventArgs e)
        {
            if (this.IsShadowVisibleChanged != null)
            {
                this.IsShadowVisibleChanged(sender, e);
            }
        }

        public event EventHandler<LineDashChangedEventArgs> LineDashChanged;

        protected void OnLineDashChanged(object sender, LineDashChangedEventArgs e)
        {
            if (this.LineDashChanged != null)
            {
                this.LineDashChanged(sender, e);
            }
        }

        public event EventHandler<WidgetBackColorChangedEventArgs> WidgetBackColorChanged;

        protected void OnWidgetBackColorChanged(object sender, WidgetBackColorChangedEventArgs e)
        {
            if (this.WidgetBackColorChanged != null)
            {
                this.WidgetBackColorChanged(sender, e);
            }
        }

        public event EventHandler<WidgetLineColorChangedEventArgs> WidgetLineColorChanged;

        protected void OnWidgetLineColorChanged(object sender, WidgetLineColorChangedEventArgs e)
        {
            if (this.WidgetLineColorChanged != null)
            {
                this.WidgetLineColorChanged(sender, e);
            }
        }

        public event EventHandler<WidgetLineWidthChangedEventArgs> WidgetLineWidthChanged;

        protected void OnWidgetLineWidthChanged(object sender, WidgetLineWidthChangedEventArgs e)
        {
            if (this.WidgetLineWidthChanged != null)
            {
                this.WidgetLineWidthChanged(sender, e);
            }
        }

        public event EventHandler<WidgetForeColorChangedEventArgs> WidgetForeColorChanged;

        protected void OnWidgetForeColorChanged(object sender, WidgetForeColorChangedEventArgs e)
        {
            if (this.WidgetForeColorChanged != null)
            {
                this.WidgetForeColorChanged(sender, e);
            }
        }

        public event EventHandler<WidgetOpacityChangedEventArgs> WidgetOpacityChanged;

        protected void OnWidgetOpacityChanged(object sender, WidgetOpacityChangedEventArgs e)
        {
            if (this.WidgetOpacityChanged != null)
            {
                this.WidgetOpacityChanged(sender, e);
            }
        }

        public event EventHandler<WidgetRadiusChangedEventArgs> WidgetRadiusChanged;

        protected void OnWidgetRadiusChanged(object sender, WidgetRadiusChangedEventArgs e)
        {
            if (this.WidgetRadiusChanged != null)
            {
                this.WidgetRadiusChanged(sender, e);
            }
        }

        public event EventHandler<WidgetPaddingChangedEventArgs> WidgetPaddingChanged;

        protected void OnWidgetPaddingChanged(object sender, WidgetPaddingChangedEventArgs e)
        {
            if (this.WidgetPaddingChanged != null)
            {
                this.WidgetPaddingChanged(sender, e);
            }
        }

        #endregion


    }


    #region 事件参数类================================================================================================

    //Arrows
    //IsShadowVisible
    //WidgetBackColor 
    //WidgetLineColor 
    //WidgetLineWidth 
    //WidgetForeColor 
    //WidgetOpacity 
    //WidgetPadding

    public class ArrowsChangedEventArgs : EventArgs
    {
        public ArrowType OldValue { get; set; }
        public ArrowType NewValue { get; set; }
    }

    public class IsShadowVisibleChangedEventArgs : EventArgs
    {
        public bool OldValue { get; set; }
        public bool NewValue { get; set; }
    }

    public class LineDashChangedEventArgs : EventArgs
    {
        public LineDashType.DashType OldValue { get; set; }
        public LineDashType.DashType NewValue { get; set; }
    }

    public class WidgetBackColorChangedEventArgs : EventArgs
    {
        public Brush OldValue { get; set; }
        public Brush NewValue { get; set; }
    }

    public class WidgetLineColorChangedEventArgs : EventArgs
    {
        public Brush OldValue { get; set; }
        public Brush NewValue { get; set; }
    }

    public class WidgetLineWidthChangedEventArgs : EventArgs
    {
        public double OldValue { get; set; }
        public double NewValue { get; set; }
    }

    public class WidgetForeColorChangedEventArgs : EventArgs
    {
        public Brush OldValue { get; set; }
        public Brush NewValue { get; set; }
    }

    public class WidgetOpacityChangedEventArgs : EventArgs
    {
        public double OldValue { get; set; }
        public double NewValue { get; set; }
    }

    public class WidgetRadiusChangedEventArgs : EventArgs
    {
        public double OldValue { get; set; }
        public double NewValue { get; set; }
    }

    public class WidgetPaddingChangedEventArgs : EventArgs
    {
        public Thickness OldValue { get; set; }
        public Thickness NewValue { get; set; }
    }

    #endregion
}
